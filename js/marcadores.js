function jsonestados(){

        var veracruz = document.createElement('script');
        veracruz.setAttribute(
        'src',
        'estados/veracruz.json');
        
        var list = document.getElementsByTagName('head')[0].appendChild(veracruz);

        var queretaro = document.createElement('script');
        queretaro.setAttribute(
        'src',
        'estados/queretaro.json');

		document.getElementsByTagName('head')[0].appendChild(queretaro);

		var tlaxcala = document.createElement('script');
        tlaxcala.setAttribute(
        'src',
        'estados/tlaxcala.json');

		document.getElementsByTagName('head')[0].appendChild(tlaxcala);

		var hidalgo = document.createElement('script');
        hidalgo.setAttribute(
        'src',
        'estados/hidalgo.json');

		document.getElementsByTagName('head')[0].appendChild(hidalgo);

		var nuevo_leon = document.createElement('script');
        nuevo_leon.setAttribute(
        'src',
        'estados/nuevo_leon.json');

		document.getElementsByTagName('head')[0].appendChild(nuevo_leon);

		var puebla = document.createElement('script');
        puebla.setAttribute(
        'src',
        'estados/puebla.json');

		document.getElementsByTagName('head')[0].appendChild(puebla);
		/*Aqui termina el codigo de la carga de los estados*/

}
//funciones para agregar el color a cada una de las caps json
//el color se obtiene de cada json de los estados llamado color
function color(){
	// Añadir algún estilo.
	  map.data.setStyle (function (feature) {
	    return ({
	      fillColor: feature.getProperty ('color'),
	      strokeWeight: 1
	    });
	  });

}

function marker_int(oms,markers){
	//console.log(marcadores);
	  for (var i = 0; i < marcador.length; i++) {
	  //var datum = window.mapData[i];	
      var marker = new google.maps.Marker({
      position: new google.maps.LatLng(marcador[i][7], marcador[i][8]),
      map: map,
      draggable: true,
      animation: google.maps.Animation.DROP,
      title: marcador[i][1],
      	icon: {
		  path: google.maps.SymbolPath.CIRCLE,
		  scale: 7, //tamaño
		  strokeColor: '#F9FDFD', //color del borde
		  strokeWeight: 2, //grosor del borde
		  fillColor: '#09B5F5', //color de relleno
		  fillOpacity:5// opacidad del relleno
		},
    });
      //marker.desc = datum.d;
      intang.push(marker);
      (function(i, marker) {
	  google.maps.event.addListener(marker,'click',function() {
	  if (!infowindow) {
	  infowindow = new google.maps.InfoWindow();
	  }
	  infowindow.setContent("<strong>Nombre: </strong>"+marcador[i][1]+
		  "<br>"+"<strong>Nombre del evento: </strong>"+marcador[i][2]+
		  "<br>"+"<strong>Localidad: </strong>"+marcador[i][3]+
		  "<br>"+"<strong>Municipio: </strong>"+marcador[i][4]+
		  "<br>"+"<strong>Estado: </strong>"+marcador[i][5]+
		  "<br>"+"<strong>Mes: </strong>"+marcador[i][6]+
		  "<br>"+"<strong>Colaborador de Gobierno: </strong>"+marcador[i][10]+
		  "<br>"+"<strong>Colaboración externa: </strong>"+marcador[i][11]

	  );
	  infowindow.open(map, marker);
	  //map.setZoom(10);
      map.setCenter(marker.getPosition());
      /*if (marker.getAnimation() !== null) {
          marker.setAnimation(null);
        } else {
          marker.setAnimation(google.maps.Animation.BOUNCE);
        }*/
         
	  });
	  })(i, marker);
	  oms.addMarker(marker);
	  markers.push(marker);
	  //clusterMarker.push(marker);
	   //new MarkerClusterer(map, clusterMarker, {imagePath: 'img/m', maxZoom: 14});	
    }


}
 
function marker_nat(oms,markers){
	 for (var i = 0; i < markernat.length; i++) {
	  //var datum = window.mapData[i];	
      var marker = new google.maps.Marker({
      position: new google.maps.LatLng(markernat[i][7], markernat[i][8]),
      map: map,
      draggable: true,
      animation: google.maps.Animation.DROP,
      title: markernat[i][1],
     	icon: {
		  path: google.maps.SymbolPath.CIRCLE,
		  scale: 7, //tamaño
		  strokeColor: '#12F6D8', //color del borde
		  strokeWeight: 2, //grosor del borde
		  fillColor: '#CB2ECD', //color de relleno
		  fillOpacity:1// opacidad del relleno
		},
    });
      //marker.desc = datum.d;
      mar.push(marker);
      (function(i, marker) {
	  google.maps.event.addListener(marker,'click',function() {
	  if (!infowindow) {
	  infowindow = new google.maps.InfoWindow();
	  }
	  infowindow.setContent("<strong>Nombre: </strong>"+markernat[i][1]+
		  "<br>"+"<strong>Nombre del evento: </strong>"+markernat[i][2]+
		  "<br>"+"<strong>Localidad: </strong>"+markernat[i][3]+
		  "<br>"+"<strong>Municipio: </strong>"+markernat[i][4]+
		  "<br>"+"<strong>Estado: </strong>"+markernat[i][5]+
		  "<br>"+"<strong>Mes :</strong>"+markernat[i][6]+
		  "<br>"+"<strong>Colaborador de Gobierno: </strong>"+markernat[i][10]+
		  "<br>"+"<strong>Colaboracion externas: </strong>"+markernat[i][11]

	  );
	  infowindow.open(map, marker);
	  //map.setZoom(10);
      map.setCenter(marker.getPosition());
      /*if (marker.getAnimation() !== null) {
          marker.setAnimation(null);
        } else {
          marker.setAnimation(google.maps.Animation.BOUNCE);
        }*/	
	  });
	  })(i, marker);
	  oms.addMarker(marker);
	  markers.push(marker);
	  //clusterMarker.push(marker);
    }

}

function marker_edu(oms,markers){
	for (var i = 0; i < markeredu.length; i++) {
		  //var datum = window.mapData[i];	
	      var marker = new google.maps.Marker({
	      position: new google.maps.LatLng(markeredu[i][7], markeredu[i][8]),
	      map: map,
        title: markeredu[i][1],
	      icon: {
		  path: google.maps.SymbolPath.CIRCLE,
		  scale: 7, //tamaño
		  strokeColor: '#2D06F1', //color del borde
		  strokeWeight: 2, //grosor del borde
		  fillColor: '#2EBB08', //color de relleno
		  fillOpacity:1// opacidad del relleno
		},
	    });
	     // marker.desc = datum.d;
	      edu.push(marker);
	      (function(i, marker) {
		  google.maps.event.addListener(marker,'click',function() {
		  if (!infowindow) {
		  infowindow = new google.maps.InfoWindow();
		  }
		  infowindow.setContent("<strong>Nombre: </strong>"+markeredu[i][1]+
			  "<br>"+"<strong>Nombre del evento: </strong>"+markeredu[i][2]+
			  "<br>"+"<strong>Localidad: </strong>"+markeredu[i][3]+
			  "<br>"+"<strong>Municipio: </strong>"+markeredu[i][4]+
			  "<br>"+"<strong>Estado: </strong>"+markeredu[i][5]+
			  "<br>"+"<strong>Mes: </strong>"+markeredu[i][6]+
			  "<br>"+"<strong>Colaborador de Gobierno: </strong>"+markeredu[i][10]+
			  "<br>"+"<strong>Colaboracion externas: </strong>"+markeredu[i][11]

		  );
		  infowindow.open(map, marker);
		  //map.setZoom(10);
      	  map.setCenter(marker.getPosition());
		  });
		  })(i, marker);
		  oms.addMarker(marker);
		  markers.push(marker);
	    }


}

function marker_artes(oms,markers){
	for (var i = 0; i < markerarte.length; i++) {
		  //var datum = window.mapData[i];	
	      var marker = new google.maps.Marker({
	      position: new google.maps.LatLng(markerarte[i][7], markerarte[i][8]),
	      map: map,
        title: markerarte[i][1],
	      icon: {
		  path: google.maps.SymbolPath.CIRCLE,
		  scale: 7, //tamaño
		  strokeColor: '#C4F308', //color del borde
		  strokeWeight: 2, //grosor del borde
		  fillColor: '#86216D', //color de relleno
		  fillOpacity:1// opacidad del relleno
		},
	    });
	      art.push(marker);
	      (function(i, marker) {
		  google.maps.event.addListener(marker,'click',function() {
		  if (!infowindow) {
		  infowindow = new google.maps.InfoWindow();
		  }
		  infowindow.setContent("<strong>Nombre: </strong>"+markerarte[i][1]+
			  "<br>"+"<strong>Nombre del evento: </strong>"+markerarte[i][2]+
			  "<br>"+"<strong>Localidad: </strong>"+markerarte[i][3]+
			  "<br>"+"<strong>Municipio: </strong>"+markerarte[i][4]+
			  "<br>"+"<strong>Estado: </strong>"+markerarte[i][5]+
			  "<br>"+"<strong>Mes: </strong>"+markerarte[i][6]+
			  "<br>"+"<strong>Colaborador de Gobierno: </strong>"+markerarte[i][10]+
			  "<br>"+"<strong>Colaboracion externas: </strong>"+markerarte[i][11]

		  );
		  infowindow.open(map, marker);
		  //map.setZoom(10);
          map.setCenter(marker.getPosition());
		  });
		  })(i, marker);
		  oms.addMarker(marker);
		  markers.push(marker);
	    }

}

function marker_event1(oms,markers){
	for (var i = 0; i < event1.length; i++) {
		  //var datum = window.mapData[i];	
	      var marker = new google.maps.Marker({
	      position: new google.maps.LatLng(event1[i][7], event1[i][8]),
	      map: map,
        title: event1[i][1],
	      icon: {
		  path: google.maps.SymbolPath.CIRCLE,
		  scale: 7, //tamaño
		  strokeColor: '#f00', //color del border
		  strokeWeight: 2, //grosor del borde
		  fillColor: '#FC9B02', //color de relleno
		  fillOpacity:1// opacidad del relleno
		},
	    });
	     even1.push(marker);
	     (function(i, marker) {
		  google.maps.event.addListener(marker,'click',function() {
		  if (!infowindow) {
		  infowindow = new google.maps.InfoWindow();
		  }
		  infowindow.setContent("<strong>Nombre: </strong>"+event1[i][1]+
			  "<br>"+"<strong>Nombre del evento: </strong>"+event1[i][2]+
			  "<br>"+"<strong>Localidad: </strong>"+event1[i][3]+
			  "<br>"+"<strong>Municipio: </strong>"+event1[i][4]+
			  "<br>"+"<strong>Estado: </strong>"+event1[i][5]+
			  "<br>"+"<strong>Mes: </strong>"+event1[i][6]+
			  "<br>"+"<strong>Colaborador de Gobierno: </strong>"+event1[i][10]+
			  "<br>"+"<strong>Colaboracion externas: </strong>"+event1[i][11]
		  );
		  infowindow.open(map, marker);
		   //map.setZoom(10);
           map.setCenter(marker.getPosition());
		  });
		  })(i, marker);
		  oms.addMarker(marker);
		  markers.push(marker);
	    }
}

function marker_event2(oms,markers){
	for (var i = 0; i < event2.length; i++) {
		  //var datum = window.mapData[i];	
	      var marker = new google.maps.Marker({
	      position: new google.maps.LatLng(event2[i][7], event2[i][8]),
	      map: map,
          title: event2[i][1],
	      icon: {
		  path: google.maps.SymbolPath.CIRCLE,
		  scale: 7, //tamaño
		  strokeColor: '#0BCFD6', //color del border
		  strokeWeight: 2, //grosor del borde
		  fillColor: '#D9D9D9', //color de relleno
		  fillOpacity:1// opacidad del relleno
		},
	    });
	     even2.push(marker);
	     (function(i, marker) {
		  google.maps.event.addListener(marker,'click',function() {
		  if (!infowindow) {
		  infowindow = new google.maps.InfoWindow();
		  }
		  infowindow.setContent("<strong>Nombre: </strong>"+event2[i][1]+
			  "<br>"+"<strong>Nombre del evento: </strong>"+event2[i][2]+
			  "<br>"+"<strong>Localidad: </strong>"+event2[i][3]+
			  "<br>"+"<strong>Municipio: </strong>"+event2[i][4]+
			  "<br>"+"<strong>Estado: </strong>"+event2[i][5]+
			  "<br>"+"<strong>Mes: </strong>"+event2[i][6]+
			  "<br>"+"<strong>Colaborador de Gobierno: </strong>"+event2[i][10]+
			  "<br>"+"<strong>Colaboracion externas: </strong>"+event2[i][11]
		  );
		  infowindow.open(map, marker);
		  //map.setZoom(10);
      	  map.setCenter(marker.getPosition());
		  });
		  })(i, marker);
		  oms.addMarker(marker);
		  markers.push(marker);
	    }
}

function marker_event3(oms,markers){
	for (var i = 0; i < event3.length; i++) {
		  //var datum = window.mapData[i];	
	      var marker = new google.maps.Marker({
	      position: new google.maps.LatLng(event3[i][7], event3[i][8]),
	      map: map,
        title: event3[i][1],
	      icon: {
		  path: google.maps.SymbolPath.CIRCLE,
		  scale: 7, //tamaño
		  strokeColor: '#FBFBFB', //color del border
		  strokeWeight: 2, //grosor del borde
		  fillColor: '#0D6541', //color de relleno
		  fillOpacity:1// opacidad del relleno
		},
	    });
	    even3.push(marker);
	     (function(i, marker) {
		  google.maps.event.addListener(marker,'click',function() {
		  if (!infowindow) {
		  infowindow = new google.maps.InfoWindow();
		  }
		  infowindow.setContent("<strong>Nombre: </strong>"+event3[i][1]+
			  "<br>"+"<strong>Nombre del evento: </strong>"+event3[i][2]+
			  "<br>"+"<strong>Localidad: </strong>"+event3[i][3]+
			  "<br>"+"<strong>Municipio: </strong>"+event3[i][4]+
			  "<br>"+"<strong>Estado: </strong>"+event3[i][5]+
			  "<br>"+"<strong>Mes: </strong>"+event3[i][6]+
			  "<br>"+"<strong>Colaborador de Gobierno: </strong>"+event3[i][10]+
			  "<br>"+"<strong>Colaboracion externas: </strong>"+event3[i][11]
		  );
		  infowindow.open(map, marker);
		  //map.setZoom(10);
      	  map.setCenter(marker.getPosition());
		  });
		  })(i, marker);
		  oms.addMarker(marker);
		  markers.push(marker);
	    }

}


function marker_event4(oms,markers) {
	for (var i = 0; i < event4.length; i++) {
		  //var datum = window.mapData[i];
	      var marker = new google.maps.Marker({
	      position: new google.maps.LatLng(event4[i][7], event4[i][8]),
	      map: map,
          title: event4[i][1],
	      icon: {
		  path: google.maps.SymbolPath.CIRCLE,
		  scale: 7, //tamaño
		  strokeColor: '#229F0D', //color del border
		  strokeWeight: 2, //grosor del borde
		  fillColor: '#ED308F', //color de relleno
		  fillOpacity:1// opacidad del relleno
		},
	    });
	    even4.push(marker);
	    (function(i, marker) {
		  google.maps.event.addListener(marker,'click',function() {
		  if (!infowindow) {
		  infowindow = new google.maps.InfoWindow();
		  }
		  infowindow.setContent("<strong>Nombre: </strong>"+event4[i][1]+
			  "<br>"+"<strong>Nombre del evento: </strong>"+event4[i][2]+
			  "<br>"+"<strong>Localidad: </strong>"+event4[i][3]+
			  "<br>"+"<strong>Municipio: </strong>"+event4[i][4]+
			  "<br>"+"<strong>Estado: </strong>"+event4[i][5]+
			  "<br>"+"<strong>Mes: </strong>"+event4[i][6]+
			  "<br>"+"<strong>Colaborador de Gobierno: </strong>"+event4[i][10]+
			  "<br>"+"<strong>Colaboracion externas: </strong>"+event4[i][11]
		  );
		  infowindow.open(map, marker);
		  //map.setZoom(10);
      	  map.setCenter(marker.getPosition());
		  });
		  })(i, marker);
		  oms.addMarker(marker);
		  markers.push(marker);
	    }	

}

function marker_event5(oms,markers){
	for (var i = 0; i < event5.length; i++) {
		  //var datum = window.mapData[i];	
	      var marker = new google.maps.Marker({
	      position: new google.maps.LatLng(event5[i][7], event5[i][8]),
	      map: map,
        title: event5[i][1],
	      icon: {
		  path: google.maps.SymbolPath.CIRCLE,
		  scale: 7, //tamaño
		  strokeColor: '#0C0808', //color del border
		  strokeWeight: 2, //grosor del borde
		  fillColor: '#EB9001', //color de relleno
		  fillOpacity:1// opacidad del relleno
		},
	    });
	    even5.push(marker);
	    (function(i, marker) {
		  google.maps.event.addListener(marker,'click',function() {
		  if (!infowindow) {
		  infowindow = new google.maps.InfoWindow();
		  }
		  infowindow.setContent("<strong>Nombre: </strong>"+event5[i][1]+
			  "<br>"+"<strong>Nombre del evento: </strong>"+event5[i][2]+
			  "<br>"+"<strong>Localidad: </strong>"+event5[i][3]+
			  "<br>"+"<strong>Municipio: </strong>"+event5[i][4]+
			  "<br>"+"<strong>Estado: </strong>"+event5[i][5]+
			  "<br>"+"<strong>Mes: </strong>"+event5[i][6]+
			  "<br>"+"<strong>Colaborador de Gobierno: </strong>"+event5[i][10]+
			  "<br>"+"<strong>Colaboracion externas: </strong>"+event5[i][11]

		  );
		  infowindow.open(map, marker);
		  //map.setZoom(10);
      	  map.setCenter(marker.getPosition());
		  });
		  })(i, marker);
		  oms.addMarker(marker);
		  markers.push(marker);
	}

}

function marker_event6(oms,markers){
	for (var i = 0; i < event6.length; i++) {
		  //var datum = window.mapData[i];
	      var marker = new google.maps.Marker({
	      position: new google.maps.LatLng(event6[i][7], event6[i][8]),
	      map: map,
          title: event6[i][1],
	      icon: {
		  path: google.maps.SymbolPath.CIRCLE,
		  scale: 7, //tamaño
		  strokeColor: '#B50CFB', //color del border
		  strokeWeight: 2, //grosor del borde
		  fillColor: '#f00', //color de relleno
		  fillOpacity:1// opacidad del relleno
		},
	    });
	    even6.push(marker);
	    (function(i, marker) {
		  google.maps.event.addListener(marker,'click',function() {
		  if (!infowindow) {
		  infowindow = new google.maps.InfoWindow();
		  }
		  infowindow.setContent("<strong>Nombre: </strong>"+event6[i][1]+
			  "<br>"+"<strong>Nombre del evento: </strong>"+event6[i][2]+
			  "<br>"+"<strong>Localidad: </strong>"+event6[i][3]+
			  "<br>"+"<strong>Municipio: </strong>"+event6[i][4]+
			  "<br>"+"<strong>Estado: </strong>"+event6[i][5]+
			  "<br>"+"<strong>Mes: </strong>"+event6[i][6]+
			  "<br>"+"<strong>Colaborador de Gobierno: </strong>"+event6[i][10]+
			  "<br>"+"<strong>Colaboracion externas: </strong>"+event6[i][11]
		  );
		  infowindow.open(map, marker);
		  //map.setZoom(10);
          map.setCenter(marker.getPosition());
		  });
		  })(i, marker);
		  oms.addMarker(marker);
		  markers.push(marker);
	}

}

function marker_cursos(oms,markers){
	for (var i = 0; i < cursos.length; i++) {
		  //var datum = window.mapData[i];	
	      var marker = new google.maps.Marker({
	      position: new google.maps.LatLng(cursos[i][7], cursos[i][8]),
	      map: map,
          title: cursos[i][1],
	      icon: {
		  path: google.maps.SymbolPath.CIRCLE,
		  scale: 7, //tamaño
		  strokeColor: '#f00', //color del border
		  strokeWeight: 2, //grosor del borde
		  fillColor: '#119D7E', //color de relleno
		  fillOpacity:1// opacidad del relleno
		},
	    });
	    curs.push(marker);
	    (function(i, marker) {
		  google.maps.event.addListener(marker,'click',function() {
		  if (!infowindow) {
		  infowindow = new google.maps.InfoWindow();
		  }
		  infowindow.setContent("<strong>Nombre: </strong>"+cursos[i][1]+
			  "<br>"+"<strong>Nombre del evento: </strong>"+cursos[i][2]+
			  "<br>"+"<strong>Localidad: </strong>"+cursos[i][3]+
			  "<br>"+"<strong>Municipio: </strong>"+cursos[i][4]+
			  "<br>"+"<strong>Estado: </strong>"+cursos[i][5]+
			  "<br>"+"<strong>Mes: </strong>"+cursos[i][6]+
			  "<br>"+"<strong>Colaborador de Gobierno: </strong>"+cursos[i][10]+
			  "<br>"+"<strong>Colaboracion externas: </strong>"+cursos[i][11]
		  );
		  infowindow.open(map, marker);
		  //map.setZoom(10);
      	  map.setCenter(marker.getPosition());
		  });
		  })(i, marker);
		  oms.addMarker(marker);
		  markers.push(marker);
	}

}

function marker_festival(oms,markers){
	for (var i = 0; i < festival.length; i++) {
		  //var datum = window.mapData[i];	
	      var marker = new google.maps.Marker({
	      position: new google.maps.LatLng(festival[i][7], festival[i][8]),
	      map: map,
        title: festival[i][1],
	      icon: {
		  path: google.maps.SymbolPath.CIRCLE,
		  scale: 7, //tamaño
		  strokeColor: '#A49530', //color del border
		  strokeWeight: 2, //grosor del borde
		  fillColor: '#0AEFC9',//color de relleno
		  fillOpacity:1// opacidad del relleno
		},
	    });
	    fest.push(marker);
	    (function(i, marker) {
		  google.maps.event.addListener(marker,'click',function() {
		  if (!infowindow) {
		  infowindow = new google.maps.InfoWindow();
		  }
		  infowindow.setContent("<strong>Nombre: </strong>"+festival[i][1]+
			  "<br>"+"<strong>Nombre del evento: </strong>"+festival[i][2]+
			  "<br>"+"<strong>Localidad: </strong>"+festival[i][3]+
			  "<br>"+"<strong>Municipio: </strong>"+festival[i][4]+
			  "<br>"+"<strong>Estado: </strong>"+festival[i][5]+
			  "<br>"+"<strong>Mes: </strong>"+festival[i][6]+
			  "<br>"+"<strong>Colaborador de Gobierno: </strong>"+festival[i][10]+
			  "<br>"+"<strong>Colaboracion externas: </strong>"+festival[i][11]
		  );
		  infowindow.open(map, marker);
		  //map.setZoom(10);
      	  map.setCenter(marker.getPosition());
		  });
		  })(i, marker);
		  oms.addMarker(marker);
		  markers.push(marker);
	}

}

function marker_foro(oms,markers){
	for (var i = 0; i < foro.length; i++) {
		  //var datum = window.mapData[i];
	      var marker = new google.maps.Marker({
	      position: new google.maps.LatLng(foro[i][7], foro[i][8]),
	      map: map,
        title: foro[i][1],
	      icon: {
		  path: google.maps.SymbolPath.CIRCLE,
		  scale: 7, //tamaño
		  strokeColor: '#B408FC', //color del border
		  strokeWeight: 2, //grosor del borde
		  fillColor: '#84BF12',//color de relleno
		  fillOpacity:1// opacidad del relleno
		},
	    });
	    conf.push(marker);
	    (function(i, marker) {
		  google.maps.event.addListener(marker,'click',function() {
		  if (!infowindow) {
		  infowindow = new google.maps.InfoWindow();
		  }
		  infowindow.setContent("<strong>Nombre: </strong>"+foro[i][1]+
			  "<br>"+"<strong>Nombre del evento: </strong>"+foro[i][2]+
			  "<br>"+"<strong>Localidad: </strong>"+foro[i][3]+
			  "<br>"+"<strong>Municipio: </strong>"+foro[i][4]+
			  "<br>"+"<strong>Estado: </strong>"+foro[i][5]+
			  "<br>"+"<strong>Mes: </strong>"+foro[i][6]+
			  "<br>"+"<strong>Colaborador de Gobierno: </strong>"+foro[i][10]+
			  "<br>"+"<strong>Colaboracion externas: </strong>"+foro[i][11]

		  );
		  infowindow.open(map, marker);
		  //map.setZoom(10);
      	  map.setCenter(marker.getPosition());
		  });
		  })(i, marker);
		  oms.addMarker(marker);
		  markers.push(marker);
	}

}

function marker_presentaciones(oms,markers){
	for (var i = 0; i < presentaciones.length; i++) {
		  //var datum = window.mapData[i];
	      var marker = new google.maps.Marker({
	      position: new google.maps.LatLng(presentaciones[i][7], presentaciones[i][8]),
	      map: map,
        title: presentaciones[i][1],
	      icon: {
		  path: google.maps.SymbolPath.CIRCLE,
		  scale: 7, //tamaño
		  strokeColor: '#14996C', //color del border
		  strokeWeight: 2, //grosor del borde
		  fillColor: '#FF26E2',//color de relleno
		  fillOpacity:1// opacidad del relleno
		},
	    });
	    pres.push(marker);
	    (function(i, marker) {
		  google.maps.event.addListener(marker,'click',function() {
		  if (!infowindow) {
		  infowindow = new google.maps.InfoWindow();
		  }
		  infowindow.setContent("<strong>Nombre: </strong>"+presentaciones[i][1]+
			  "<br>"+"<strong>Nombre del evento: </strong>"+presentaciones[i][2]+
			  "<br>"+"<strong>Localidad: </strong>"+presentaciones[i][3]+"<br>"+
			  "<strong>Municipio: </strong>"+presentaciones[i][4]+
			  "<br>"+"<strong>Estado: </strong>"+presentaciones[i][5]+
			  "<br>"+"<strong>Mes: </strong>"+presentaciones[i][6]+
			  "<br>"+"<strong>Colaborador de Gobierno: </strong>"+presentaciones[i][10]+
			  "<br>"+"<strong>Colaboracion externas: </strong>"+presentaciones[i][11]
		  );
		  infowindow.open(map, marker);
		  //map.setZoom(10);
      	  map.setCenter(marker.getPosition());
		  });
		  })(i, marker);
		  oms.addMarker(marker);
		  markers.push(marker);
	}

}

function marker_artistica(oms,markers){
	for (var i = 0; i < artistica.length; i++) {
		  //var datum = window.mapData[i];
	      var marker = new google.maps.Marker({
	      position: new google.maps.LatLng(artistica[i][7], artistica[i][8]),
	      map: map,
        title: artistica[i][1],
	      icon: {
		  path: google.maps.SymbolPath.CIRCLE,
		  scale: 7, //tamaño
		  strokeColor: '#f00', //color del border
		  strokeWeight: 2, //grosor del borde
		  fillColor: '#F5ED08',//color de relleno
		  fillOpacity:1// opacidad del relleno
		},
	    });
	    arti.push(marker);
	    (function(i, marker) {
		  google.maps.event.addListener(marker,'click',function() {
		  if (!infowindow) {
		  infowindow = new google.maps.InfoWindow();
		  }
		  infowindow.setContent("<strong>Nombre: </strong>"+artistica[i][1]+
			  "<br>"+"<strong>Nombre del evento: </strong>"+artistica[i][2]+
			  "<br>"+"<strong>Localidad: </strong>"+artistica[i][3]+
			  "<br>"+"<strong>Municipio: </strong>"+artistica[i][4]+
			  "<br>"+"<strong>Estado: </strong>"+artistica[i][5]+
			  "<br>"+"<strong>Mes: </strong>"+artistica[i][6]+
			  "<br>"+"<strong>Colaborador de Gobierno: </strong>"+artistica[i][10]+
			  "<br>"+"<strong>Colaboracion externas: </strong>"+artistica[i][11]
		  );
		  infowindow.open(map, marker);
		  //map.setZoom(10);
      	  map.setCenter(marker.getPosition());
		  });
		  })(i, marker);
		  oms.addMarker(marker);
		  markers.push(marker);

	}

}

function marker_restauracion(oms,markers){
	for (var i = 0; i < restauracion.length; i++) {
		  //var datum = window.mapData[i];
	      var marker = new google.maps.Marker({
	      position: new google.maps.LatLng(restauracion[i][7], restauracion[i][8]),
	      map: map,
        title: restauracion[i][1],
	      icon: {
		  path: google.maps.SymbolPath.CIRCLE,
		  scale: 7, //tamaño
		  strokeColor: '#f00', //color del border
		  strokeWeight: 2, //grosor del borde
		  fillColor: '#0F0F0F',//color de relleno
		  fillOpacity:1// opacidad del relleno
		},
	    });
	    rest.push(marker);
	    (function(i, marker) {
		  google.maps.event.addListener(marker,'click',function() {
		  if (!infowindow) {
		  infowindow = new google.maps.InfoWindow();
		  }
		  infowindow.setContent("<strong>Nombre: </strong>"+restauracion[i][1]+
			  "<br>"+"<strong>Nombre del evento: </strong>"+restauracion[i][2]+
			  "<br>"+"<strong>Localidad: </strong>"+restauracion[i][3]+
			  "<br>"+"<strong>Municipio: </strong>"+restauracion[i][4]+
			  "<br>"+"<strong>Estado: </strong>"+restauracion[i][5]+
			  "<br>"+"<strong>Mes: </strong>"+restauracion[i][6]+
			  "<br>"+"<strong>Colaborador de Gobierno: </strong>"+restauracion[i][10]+
			  "<br>"+"<strong>Colaboracion externas: </strong>"+restauracion[i][11]

		  );
		  infowindow.open(map, marker);
		  //map.setZoom(10);
      	  map.setCenter(marker.getPosition());
		  });
		  })(i, marker);
		  oms.addMarker(marker);
		  markers.push(marker);
	}

}

function marker_otrob(oms,markers){
	for (var i = 0; i < otrofestival.length; i++) {
		  //var datum = window.mapData[i];	
	      var marker = new google.maps.Marker({
	      position: new google.maps.LatLng(otrofestival[i][7], otrofestival[i][8]),
	      map: map,
          title: otrofestival[i][1],
	      icon: {
		  path: google.maps.SymbolPath.CIRCLE,
		  scale: 7, //tamaño
		  strokeColor: '#AF0909', //color del border
		  strokeWeight: 2, //grosor del borde
		  fillColor: '#FFA4A7',//color de relleno
		  fillOpacity:1// opacidad del relleno
		},
	    });
	    otrofest.push(marker);
	    (function(i, marker) {
		  google.maps.event.addListener(marker,'click',function() {
		  if (!infowindow) {
		  infowindow = new google.maps.InfoWindow();
		  }
		  infowindow.setContent("<strong>Nombre: </strong>"+otrofestival[i][1]+
			  "<br>"+"<strong>Nombre del evento: </strong>"+otrofestival[i][2]+
			  "<br>"+"<strong>Localidad: </strong>"+otrofestival[i][3]+
			  "<br>"+"<strong>Municipio: </strong>"+otrofestival[i][4]+
			  "<br>"+"<strong>Estado: </strong>"+otrofestival[i][5]+
			  "<br>"+"<strong>Mes: </strong>"+otrofestival[i][6]+
			  "<br>"+"<strong>Colaborador de Gobierno: </strong>"+otrofestival[i][10]+
			  "<br>"+"<strong>Colaboracion externas: </strong>"+otrofestival[i][11]
		  );
		  infowindow.open(map, marker);
		  //map.setZoom(10);
      	  map.setCenter(marker.getPosition());
		  });
		  })(i, marker);
		  oms.addMarker(marker);
		  markers.push(marker);
	}

}

function mmarker_infantes(oms,markers) {
	 for (var i = 0; i < markerinfantes.length; i++) {
	  //var datum = window.mapData[i];	
      var marker = new google.maps.Marker({
      position: new google.maps.LatLng(markerinfantes[i][7], markerinfantes[i][8]),
      map: map,
      title: markerinfantes[i][1],
      	icon: {
		  path: google.maps.SymbolPath.CIRCLE,
		  scale: 7, //tamaño
		  strokeColor: '#FFC21F', //color del borde
		  strokeWeight: 2, //grosor del borde
		  fillColor: '#f00', //color de relleno
		  fillOpacity:1// opacidad del relleno
		},
    });
      infante.push(marker);
      (function(i, marker) {
		  google.maps.event.addListener(marker,'click',function() {
		  if (!infowindow) {
		  infowindow = new google.maps.InfoWindow();
		  }
		  infowindow.setContent("<strong>Nombre: </strong>"+markerinfantes[i][1]+
			  "<br>"+"<strong>Nombre del evento: </strong>"+markerinfantes[i][2]+
			  "<br>"+"<strong>Localidad: </strong>"+markerinfantes[i][3]+
			  "<br>"+"<strong>Municipio: </strong>"+markerinfantes[i][4]+
			  "<br>"+"<strong>Estado: </strong>"+markerinfantes[i][5]+
			  "<br>"+"<strong>Mes: </strong>"+markerinfantes[i][6]+
			  "<br>"+"<strong>Colaborador de Gobierno: </strong>"+markerinfantes[i][10]+
			  "<br>"+"<strong>Colaboracion externas: </strong>"+markerinfantes[i][11]
		  );
		  infowindow.open(map, marker);
		  //map.setZoom(10);
          map.setCenter(marker.getPosition());
		  });
		  })(i, marker);
		  oms.addMarker(marker);
		  markers.push(marker);
    }

}

function marker_jovenes(oms,markers){
	 for (var i = 0; i < markerjovenes.length; i++) {
	  //var datum = window.mapData[i];	
      var marker = new google.maps.Marker({
      position: new google.maps.LatLng(markerjovenes[i][7], markerjovenes[i][8]),
      map: map,
      title: markerjovenes[i][1],
      	icon: {
		  path: google.maps.SymbolPath.CIRCLE,
		  scale: 7, //tamaño
		  strokeColor: '#B82D2D', //color del borde
		  strokeWeight: 2, //grosor del borde
		  fillColor: '#CBCACA', //color de relleno
		  fillOpacity:1// opacidad del relleno
		},
    });
     joven.push(marker);
     (function(i, marker) {
	  google.maps.event.addListener(marker,'click',function() {
	  if (!infowindow) {
	  infowindow = new google.maps.InfoWindow();
	  }
	  infowindow.setContent("<strong>Nombre: </strong>"+markerjovenes[i][1]+
		  "<br>"+"<strong>Nombre del evento: </strong>"+markerjovenes[i][2]+
		  "<br>"+"<strong>Localidad: </strong>"+markerjovenes[i][3]+
		  "<br>"+"<strong>Municipio: </strong>"+markerjovenes[i][4]+
		  "<br>"+"<strong>Estado: </strong>"+markerjovenes[i][5]+
		  "<br>"+"<strong>Mes: </strong>"+markerjovenes[i][6]+
		  "<br>"+"<strong>Colaborador de Gobierno: </strong>"+markerjovenes[i][10]+
		  "<br>"+"<strong>Colaboracion externas: </strong>"+markerjovenes[i][11]
	  );
	  infowindow.open(map, marker);
	  //map.setZoom(10);
      map.setCenter(marker.getPosition());
	  });
	  })(i, marker);
	  oms.addMarker(marker);
	  markers.push(marker);
    }
}

function marker_mujeres(oms,markers){
	for (var i = 0; i < markermujeres.length; i++) {
	  //var datum = window.mapData[i];
      var marker = new google.maps.Marker({
      position: new google.maps.LatLng(markermujeres[i][7], markermujeres[i][8]),
      map: map,
      title: markermujeres[i][1],
      	icon: {
		  path: google.maps.SymbolPath.CIRCLE,
		  scale: 7, //tamaño
		  strokeColor: 'white', //color del borde
		  strokeWeight: 2, //grosor del borde
		  fillColor: '#E69413', //color de relleno
		  fillOpacity:1// opacidad del relleno
		},
     });
     muj.push(marker);
     (function(i, marker) {
	   google.maps.event.addListener(marker,'click',function() {
	   if (!infowindow) {
	   infowindow = new google.maps.InfoWindow();
	   }
	   infowindow.setContent("<strong>Nombre: </strong>"+markermujeres[i][1]+
		   "<br>"+"<strong>Nombre del evento: </strong>"+markermujeres[i][2]+
		   "<br>"+"<strong>Localidad: </strong>"+markermujeres[i][3]+
		   "<br>"+"<strong>Municipio: </strong>"+markermujeres[i][4]+
		   "<br>"+"<strong>Estado: </strong>"+markermujeres[i][5]+
		   "<br>"+"<strong>Mes: </strong>"+markermujeres[i][6]+
		   "<br>"+"<strong>Colaborador de Gobierno: </strong>"+markermujeres[i][10]+
		   "<br>"+"<strong>Colaboracion externas: </strong>"+markermujeres[i][11]
	   );
	   infowindow.open(map, marker);
	   //map.setZoom(10);
       map.setCenter(marker.getPosition());
	   });
	   })(i, marker);
	   oms.addMarker(marker);
	   markers.push(marker);
    }
}

function marker_mayores(oms,markers){
	for (var i = 0; i < markermayores.length; i++) {
	  //var datum = window.mapData[i];	
      var marker = new google.maps.Marker({
      position: new google.maps.LatLng(markermayores[i][7], markermayores[i][8]),
      map: map,
      title: markermayores[i][1],
      	icon: {
		  path: google.maps.SymbolPath.CIRCLE,
		  scale: 7, //tamaño
		  strokeColor: '#f00', //color del borde
		  strokeWeight: 2, //grosor del borde
		  fillColor: '#9412A0', //color de relleno
		  fillOpacity:1// opacidad del relleno
		},
     });
      mayores.push(marker);
       (function(i, marker) {
	   google.maps.event.addListener(marker,'click',function() {
	   if (!infowindow) {
	   infowindow = new google.maps.InfoWindow();
	   }
	   infowindow.setContent("<strong>Nombre: </strong>"+markermayores[i][1]+
		   "<br>"+"<strong>Nombre del evento: </strong>"+markermayores[i][2]+
		   "<br>"+"<strong>Localidad: </strong>"+markermayores[i][3]+
		   "<br>"+"<strong>Municipio: </strong>"+markermayores[i][4]+
		   "<br>"+"<strong>Estado: </strong>"+markermayores[i][5]+
		   "<br>"+"<strong>Mes: </strong>"+markermayores[i][6]+
		   "<br>"+"<strong>Colaborador de Gobierno: </strong>"+markermayores[i][10]+
		   "<br>"+"<strong>Colaboracion externas: </strong>"+markermayores[i][11]
	   );
	   infowindow.open(map, marker);
	   //map.setZoom(10);
       map.setCenter(marker.getPosition());
	   });
	   })(i, marker);
	   oms.addMarker(marker);
	   markers.push(marker);
    }
}

function marker_general(oms,markers){
	for (var i = 0; i < markergeneral.length; i++) {
	  //var datum = window.mapData[i];	
      var marker = new google.maps.Marker({
      position: new google.maps.LatLng(markergeneral[i][7], markergeneral[i][8]),
      map: map,
      title: markergeneral[i][1],
      	icon: {
		  path: google.maps.SymbolPath.CIRCLE,
		  scale: 7, //tamaño
		  //strokeColor: '#f00', //color del borde
		  strokeWeight: 2, //grosor del borde
		  fillColor: '#15CBBC', //color de relleno
		  fillOpacity:1// opacidad del relleno
		},
     });
      pgeneral.push(marker);
       (function(i, marker) {
	   google.maps.event.addListener(marker,'click',function() {
	   if (!infowindow) {
	   infowindow = new google.maps.InfoWindow();
	   }
	   infowindow.setContent("<strong>Nombre: </strong>"+markergeneral[i][1]+
		   "<br>"+"<strong>Nombre del evento: </strong>"+markergeneral[i][2]+
		   "<br>"+"<strong>Localidad: </strong>"+markergeneral[i][3]+
		   "<br>"+"<strong>Municipio: </strong>"+markergeneral[i][4]+
		   "<br>"+"<strong>Estado: </strong>"+markergeneral[i][5]+
		   "<br>"+"<strong>Mes: </strong>"+markergeneral[i][6]+
		   "<br>"+"<strong>Colaborador de Gobierno: </strong>"+markergeneral[i][10]+
		   "<br>"+"<strong>Colaboracion externas: </strong>"+markergeneral[i][11]
	   );
	   infowindow.open(map, marker);
	   /// map.setZoom(10);
       map.setCenter(marker.getPosition());
	   });
	   })(i, marker);
	   oms.addMarker(marker);
	   markers.push(marker);
    }	
}

function marker_otra(oms,markers) {
	for (var i = 0; i < markerotra.length; i++) {
	  // var datum = window.mapData[i];	
      var marker = new google.maps.Marker({
      position: new google.maps.LatLng(markerotra[i][7], markerotra[i][8]),
      map: map,
      title: markerotra[i][1],
      	icon: {
		  path: google.maps.SymbolPath.CIRCLE,
		  scale: 7, //tamaño
		  strokeColor: '#402BC0', //color del borde
		  strokeWeight: 2, //grosor del borde
		  fillColor: '#C5B3FF', //color de relleno
		  fillOpacity:1// opacidad del relleno
		},
     });
     otrobj.push(marker);
      (function(i, marker) {
	   google.maps.event.addListener(marker,'click',function() {
	   if (!infowindow) {
	   infowindow = new google.maps.InfoWindow();
	   }
	   infowindow.setContent("<strong>Nombre: </strong>"+markerotra[i][1]+
		   "<br>"+"<strong>Nombre del evento: </strong>"+markerotra[i][2]+
		   "<br>"+"<strong>Localidad: </strong>"+markerotra[i][3]+
		   "<br>"+"<strong>Municipio: </strong>"+markerotra[i][4]+
		   "<br>"+"<strong>Estado: </strong>"+markerotra[i][5]+
		   "<br>"+"<strong>Mes: </strong>"+markerotra[i][6]+
		   "<br>"+"<strong>Colaborador de Gobierno: </strong>"+markerotra[i][10]+
		   "<br>"+"<strong>Colaboracion externas: </strong>"+markerotra[i][11]
	   );
	   infowindow.open(map, marker);
	   //map.setZoom(10);
       map.setCenter(marker.getPosition());
	   });
	   })(i, marker);
	   oms.addMarker(marker);
	   markers.push(marker);
    }	
}
//funcion para mostrar las graficas
//La dat se obtiene de los json de cada uno de los estados la letter de cada uno 
function mostrargraficas(){
	map.data.addListener('click', function(event) {
          var dat = document.getElementById('info-box').textContent =
              event.feature.getProperty('letter');
              console.log(dat);
              if (dat == "G") {
                setTimeout(clickbutton,2000);
                function clickbutton()
                 {
                  // simulamos el click del mouse en el boton del formulario
                  $("#ver").click();
                  $("#ventveracruz").click();
                  }
                }else if (dat == "q") {
                   setTimeout(clickbutton,2000);
                   function clickbutton()
                   {
                   // simulamos el click del mouse en el boton del formulario
                    $("#que").click();
                    $("#ventqueretaro").click();
                   }
                }else if (dat == "t") {
                   setTimeout(clickbutton,2000);
                   function clickbutton()
                   {
                   // simulamos el click del mouse en el boton del formulario
                    $("#tlax").click();
                    $("#ventlax").click();
                   }
                }else if (dat == "h") {
                   setTimeout(clickbutton,2000);
                   function clickbutton()
                   {
                   // simulamos el click del mouse en el boton del formulario
                    $("#hidal").click();
                    $("#venhidal").click();
                   }
                }else if (dat == "l") {
                  setTimeout(clickbutton,2000);
                  function clickbutton()
                  {
                    // simulamos el click del mouse en el boton del formulario
                    $("#leon").click();
                    $("#venleon").click();
                  }
                }else if (dat == "p") {
                  setTimeout(clickbutton,2000);
                  function clickbutton()
                  {
                    // simulamos el click del mouse en el boton del formulario
                    $("#pueb").click();
                    $("#ventpuebla").click();
                  } 
                }

                
        });


	
}

//aqui empiezan todas las funciones para mostrar y borrar los markers para cada categoria  

// funcion natural 
function natAll(map) {
    for (var i = 0; i < mar.length; i++) {
    mar[i].setMap(map);
    }
}
function clearnat() {
        natAll(null);
}

function shownat() {
        natAll(map);
}      

//funciones para intangible
function All(map) {
    for (var i = 0; i <  intang.length; i++) {
    intang[i].setMap(map);
    }
}
function Markers() {
        All(null);

    



}
function showMarker() {
        All(map);
}
//funciones para la educacion
function educa(map) {
    for (var i = 0; i <  edu.length; i++) {
    edu[i].setMap(map);
    }
}
function educaMarkers() {
        educa(null);
}
function showeduca() {
        educa(map);
}
//funciones para las artes
function arte(map) {
    for (var i = 0; i <  art.length; i++) {
    art[i].setMap(map);
    }
}
function arteMarkers() {
        arte(null);
}
function showarte() {
        arte(map);
}

//funciones para publico objetivo
function public_ob(map) {
    for (var i = 0; i < pub.length; i++) {
    pub[i].setMap(map);
    }
}
function clearpub() {
        public_ob(null);
}

function showpub() {
        public_ob(map);
} 

//funciones para publico objetivo

function event_1(map) {
    for (var i = 0; i < even1.length; i++) {
    even1[i].setMap(map);
    }
}
function clearevent1() {
        event_1(null);
}

function showevent1() {
        event_1(map);
}

//funciones para el evento 2
function event_2(map) {
    for (var i = 0; i < even2.length; i++) {
    even2[i].setMap(map);
    }
}
function clearevent2() {
        event_2(null);
}

function showevent2() {
        event_2(map);
}

//funciones para el evento 3
function event_3(map) {
    for (var i = 0; i < even3.length; i++) {
    even3[i].setMap(map);
    }
}
function clearevent3() {
        event_3(null);
}

function showevent3() {
        event_3(map);
}

//funciones para el evento 4
function event_4(map) {
    for (var i = 0; i < even4.length; i++) {
    even4[i].setMap(map);
    }
}
function clearevent4() {
        event_4(null);
}

function showevent4() {
        event_4(map);
}

//funciones para el evento 5
function event_5(map) {
    for (var i = 0; i < even5.length; i++) {
    even5[i].setMap(map);
    }
}
function clearevent5() {
        event_5(null);
}

function showevent5() {
        event_5(map);
}
//funciones para el evento 6
function event_6(map) {
    for (var i = 0; i < even6.length; i++) {
    even6[i].setMap(map);
    }
}
function clearevent6() {
        event_6(null);
}

function showevent6() {
        event_6(map);
}

//funciones para los cursos
function curso(map) {
    for (var i = 0; i < curs.length; i++) {
    curs[i].setMap(map);
    }
}
function clearcurso() {
        curso(null);
}

function showcurso() {
        curso(map);
}

//funciones para los festivales
function festi(map) {
    for (var i = 0; i < fest.length; i++) {
    fest[i].setMap(map);
    }
}
function clearfesti() {
        festi(null);
}

function showfesti() {
        festi(map);
}
//funciones para los foros 
function foros(map) {
    for (var i = 0; i < conf.length; i++) {
    conf[i].setMap(map);
    }
}
function clearforo() {
        foros(null);
}

function showforo() {
        foros(map);
}

//funciones para las prestaciones
function prestacion(map) {
    for (var i = 0; i < pres.length; i++) {
    pres[i].setMap(map);
    }
}
function clearprest() {
        prestacion(null);
}

function showprest() {
        prestacion(map);
}
//funciones para la artistica
function artist(map) {
    for (var i = 0; i < arti.length; i++) {
    arti[i].setMap(map);
    }
}
function clearartist() {
        artist(null);
}

function showartist() {
        artist(map);
}
//funciones para la restauracion
function restaura(map) {
    for (var i = 0; i < rest.length; i++) {
    rest[i].setMap(map);
    }
}
function clearestauracion() {
        restaura(null);
}

function showrestauracion() {
        restaura(map);
}
//funciones para el otro festival
function otrofestiva(map) {
    for (var i = 0; i < otrofest.length; i++) {
    	otrofest[i].setMap(map);
    }
}
function clearfestival() {
        otrofestiva(null);
}

function showfestival() {
        otrofestiva(map);
}

// funcion natural 
function setMapOnAll(map) {
    for (var i = 0; i < mar.length; i++) {
    mar[i].setMap(map);
    }
}
function clearMarkers() {
        setMapOnAll(null);
}

function showMarkers() {
        setMapOnAll(map);
}      

//funciones para intangible
function All(map) {
    for (var i = 0; i <  intang.length; i++) {
    intang[i].setMap(map);
    }
}
function Markers() {
        All(null);
}
function showMarker() {
        All(map);
}
//funciones para la educacion
function educa(map) {
    for (var i = 0; i <  edu.length; i++) {
    edu[i].setMap(map);
    }
}
function educaMarkers() {
        educa(null);
}
function showeduca() {
        educa(map);
}
//funciones para las artes
function arte(map) {
    for (var i = 0; i <  art.length; i++) {
    art[i].setMap(map);
    }
}
function arteMarkers() {
        arte(null);
}
function showarte() {
        arte(map);
}

//funciones para publico objetivo
function public_ob(map) {
    for (var i = 0; i < pub.length; i++) {
    pub[i].setMap(map);
    }
}
function clearpub() {
        public_ob(null);
}

function showpub() {
        public_ob(map);
} 

//funciones para publico objetivo

function event_1(map) {
    for (var i = 0; i < even1.length; i++) {
    even1[i].setMap(map);
    }
}
function clearevent1() {
        event_1(null);
}

function showevent1() {
        event_1(map);
}

//funciones para el evento 2
function event_2(map) {
    for (var i = 0; i < even2.length; i++) {
    even2[i].setMap(map);
    }
}
function clearevent2() {
        event_2(null);
}

function showevent2() {
        event_2(map);
}

//funciones para el evento 3
function event_3(map) {
    for (var i = 0; i < even3.length; i++) {
    even3[i].setMap(map);
    }
}
function clearevent3() {
        event_3(null);
}

function showevent3() {
        event_3(map);
}

//funciones para el evento 4
function event_4(map) {
    for (var i = 0; i < even4.length; i++) {
    even4[i].setMap(map);
    }
}
function clearevent4() {
        event_4(null);
}

function showevent4() {
        event_4(map);
}

//funciones para el evento 5
function event_5(map) {
    for (var i = 0; i < even5.length; i++) {
    even5[i].setMap(map);
    }
}
function clearevent5() {
        event_5(null);
}

function showevent5() {
        event_5(map);
}
//funciones para el evento 6
function event_6(map) {
    for (var i = 0; i < even6.length; i++) {
    even6[i].setMap(map);
    }
}
function clearevent6() {
        event_6(null);
}

function showevent6() {
        event_6(map);
}

//funciones para los cursos
function curso(map) {
    for (var i = 0; i < curs.length; i++) {
    curs[i].setMap(map);
    }
}
function clearcurso() {
        curso(null);
}

function showcurso() {
        curso(map);
}

//funciones para los festivales
function festi(map) {
    for (var i = 0; i < fest.length; i++) {
    fest[i].setMap(map);
    }
}
function clearfesti() {
        festi(null);
}

function showfesti() {
        festi(map);
}
//funciones para los foros 
function foros(map) {
    for (var i = 0; i < conf.length; i++) {
    conf[i].setMap(map);
    }
}
function clearforo() {
        foros(null);
}

function showforo() {
        foros(map);
}

//funciones para las prestaciones
function prestacion(map) {
    for (var i = 0; i < pres.length; i++) {
    pres[i].setMap(map);
    }
}
function clearprest() {
        prestacion(null);
}

function showprest() {
        prestacion(map);
}
//funciones para la artistica
function artist(map) {
    for (var i = 0; i < arti.length; i++) {
    arti[i].setMap(map);
    }
}
function clearartist() {
        artist(null);
}

function showartist() {
        artist(map);
}
//funciones para la restauracion
function restaura(map) {
    for (var i = 0; i < rest.length; i++) {
    rest[i].setMap(map);
    }
}
function clearestauracion() {
        restaura(null);
}

function showrestauracion() {
        restaura(map);
}
//funciones para el otro festival
function otrofestiva(map) {
    for (var i = 0; i < otrofest.length; i++) {
    	otrofest[i].setMap(map);
    }
}
function clearfestival() {
        otrofestiva(null);
}

function showfestival() {
        otrofestiva(map);
}
// funcion infantes 
function setMapOnAll(map) {
    for (var i = 0; i < infante.length; i++) {
    infante[i].setMap(map);
    }
}
function clearinfantes() {
        setMapOnAll(null);
}

function showinfantes() {
        setMapOnAll(map);
}
//funciones para los jovenes
function jovall(map) {
    for (var i = 0; i < joven.length; i++) {
    	joven[i].setMap(map);
    }
}
function clearjoven() {
        jovall(null);
}

function showjoven() {
        jovall(map);
}
//funciones para las mujeres
function mujall(map) {
    for (var i = 0; i < muj.length; i++) {
    	muj[i].setMap(map);
    }
}
function clearmujer() {
        mujall(null);
}

function showmujer() {
       mujall(map);
} 
//funciones para los mayores
function mayall(map) {
    for (var i = 0; i < mayores.length; i++) {
    	mayores[i].setMap(map);
    }
}
function clearmayores() {
        mayall(null);
}

function showmayores() {
       mayall(map);
}
//funciones para el publico en general
function generalall(map) {
    for (var i = 0; i < pgeneral.length; i++) {
    	pgeneral[i].setMap(map);
    }
}
function cleargeneral() {
        generalall(null);
}

function showgen() {
       generalall(map);
}
//funciones para la otra
function oball(map) {
    for (var i = 0; i < otrobj.length; i++) {
    	otrobj[i].setMap(map);
    }
}
function clearobj() {
        oball(null);
}

function showobj() {
       oball(map);
}
