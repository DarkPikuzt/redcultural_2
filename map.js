$(document).ready(function(){
  //mapoa de mexico
  //cuompruebas@gmail.com / DRIVE / redOPC / estados.kml
  //layer0 = "https://drive.google.com/file/d/1k0a-vVIximUXji1gdd2ecYIxWFU2URIE/view?usp=sharing"; 

 //initCheckLayer();
 initMap();

$('[data-toggle="tooltip"]').tooltip();   

});

function initMap() {

	 var map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: 23.0000000, lng: -102.0000000},
    zoom: 5
  });


	var layer = new google.maps.FusionTablesLayer({
    suppressInfoWindows: true, 
    query:{
      select: 'geometry',
      //Estados asociados a la red OPC con conteo por estado de la poblacion objetivo
      ////cuompruebas@gmail.com / DRIVE / redOPC / mexico (fusionTables) -- cuom2013
      from : '1zeYergZMutKoTHxC-BS2aEOmZtcyzfsDULL6W2IE'
    },
    styles:[{
      where: " 'Estado' = 'Querétaro'",
      polygonOptions: {
        fillColor: '#ae8a5e'
      }
    },{
      where: " 'Estado' = 'Tlaxcala'",
      polygonOptions: {
        fillColor: '#EF820D'
      }
    } ,{
      where: " 'Estado' = 'Veracruz'",
      polygonOptions: {
        fillColor: '#FFBF00'
      }
    },  {
      where: " 'Estado' = 'Hidalgo' ",
      polygonOptions: {
        fillColor: '#ffb708A'
      }
    }, {
      where: " 'Estado' = 'Nuevo León'",
      polygonOptions: {
        fillColor: '#5587ff'
      }
    },{
      where: " 'Estado' = 'Puebla'",
      polygonOptions: {
        fillColor: '#ff7855'
      }
    },{
      where: " 'Estado' = 'Colima'",
      polygonOptions: {
        fillColor: '#ff7855'
      } 
    }]
  });

	layer.setMap(map);
	//esta(map);

}

function esta(map){
		$(document).ready(function(){ 
			$("select[name=buscar]").change(function(){
				var mun = $('select[name=buscar]').val();

				//console.log(mun);
					var Querétaro = "Querétaro";
					var Tlaxcala = "Tlaxcala";
					var Veracruz = "Veracruz";
					var Hidalgo = "Hidalgo";
					var Nuevo_Leon = "Nuevo Leon";
					var Puebla = "Puebla";

					if (mun == Querétaro) {

							 /*var map = new google.maps.Map(document.getElementById('map'), {
		    				 center: {lat: 23.0000000, lng: -102.0000000},
		   					 zoom: 5
		 					 });*/
		 					 map = map

							var layer = new google.maps.FusionTablesLayer({
						    suppressInfoWindows: true, 
						    query:{
						      select: 'geometry',
						      from : '1zeYergZMutKoTHxC-BS2aEOmZtcyzfsDULL6W2IE'
						    },
						    styles:[{
						      where: " 'Estado' = 'Querétaro'",
						      polygonOptions: {
						      fillColor: '#ae8a5e'
						      }
						    
						    }]

			  			});

						layer.setMap(map);	
						
						return false;
					}

			});

		});

}




function reg(){

	$("select[name=buscar]").change(function(){
			var mun = $('select[name=buscar]').val();
			//alert(mun);
			console.log(mun);
			var Querétaro = "Querétaro";
			var Tlaxcala = "Tlaxcala";
			var Veracruz = "Veracruz";
			var Hidalgo = "Hidalgo";
			var Nuevo_Leon = "Nuevo LeÓn";
			var Puebla = "Puebla";

			if (mun == Querétaro) {

					 var map = new google.maps.Map(document.getElementById('map'), {
    				 center: {lat: 23.0000000, lng: -102.0000000},
   					 zoom: 5
 					 });

					var layer = new google.maps.FusionTablesLayer({
				    suppressInfoWindows: true, 
				    query:{
				      
				      from : '1zeYergZMutKoTHxC-BS2aEOmZtcyzfsDULL6W2IE'
				    },
				    styles:[{
				      where: " 'Estado' = 'Querétaro'",
				      polygonOptions: {
				      fillColor: '#ae8a5e'
				      }
				    
				    }]
	  			});

				//layer.setMap(map);
				//return false;
			}
		});
}

//comentarios 
$(document).ready(function(){
	$("select[name=buscar]").change(function(){
			var mun = $('select[name=buscar]').val();
			//alert(mun);
			var Querétaro = "Querétaro";
			var Tlaxcala = "Tlaxcala";
			var Veracruz = "Veracruz";
			var Hidalgo = "Hidalgo";
			var NuevoLeon = "Nuevo León";
			var Puebla = "Puebla";

			if (mun == Querétaro) {

					var map = new google.maps.Map(document.getElementById('map'), {
	    			center: {lat: 20.8542575, lng: -99.84756},
	   				zoom: 6
	  				});
					

					var layer = new google.maps.FusionTablesLayer({
				    suppressInfoWindows: true, 
				    query:{
				      
				      from : '1zeYergZMutKoTHxC-BS2aEOmZtcyzfsDULL6W2IE'
				    },
				    styles:[{
				      where: " 'Estado' = 'Querétaro'",
				      polygonOptions: {
				      fillColor: '#ae8a5e'
				      }
				    
				    }]
	  			});

				layer.setMap(map);
				return false;
			}else if (mun == Tlaxcala) {
					var map = new google.maps.Map(document.getElementById('map'), {
	    			center: {lat: 19.416667, lng: -98.166667},
	   				zoom: 6
	  				});
					

					var layer = new google.maps.FusionTablesLayer({
				    suppressInfoWindows: true, 
				    query:{
				     
				      from : '1zeYergZMutKoTHxC-BS2aEOmZtcyzfsDULL6W2IE'
				    },
				    styles:[{
				      where: " 'Estado' = 'Tlaxcala'",
				      polygonOptions: {
				      fillColor: '##E0BAD8'
				      }
				    
				    }]
	  			});

				layer.setMap(map);

			}else if (mun == Veracruz) {
					var map = new google.maps.Map(document.getElementById('map'), {
	    			center: {lat: 19.516237, lng: -96.918747},
	   				zoom: 6
	  				});
					

					var layer = new google.maps.FusionTablesLayer({
				    suppressInfoWindows: true, 
				    query:{
				      
				      
				      from : '1zeYergZMutKoTHxC-BS2aEOmZtcyzfsDULL6W2IE'
				    },
				    styles:[{
				      where: " 'Estado' = 'Veracruz'",
				      polygonOptions: {
				      fillColor: '#FFBF00'
				      }
				    
				    }]
	  			});

				layer.setMap(map);

			}else if (mun == Hidalgo) {
					var map = new google.maps.Map(document.getElementById('map'), {
	    			center: {lat: 20.5, lng: -99},
	   				zoom: 6
	  				});

					var layer = new google.maps.FusionTablesLayer({
				    suppressInfoWindows: true, 
				    query:{
				     
				      from : '1zeYergZMutKoTHxC-BS2aEOmZtcyzfsDULL6W2IE'
				    },
				    styles:[{
				      where: " 'Estado' = 'Hidalgo'",
				      polygonOptions: {
				      fillColor: '#ffb708A'
				      }
				    
				    }]
	  			});

				layer.setMap(map);
			}else if (mun == NuevoLeon) {
					console.log(mun)
					var map = new google.maps.Map(document.getElementById('map'), {
	    			center: {lat: 25.6714, lng: -100.3184700},
	   				zoom: 6
	  				});
					
					var layer = new google.maps.FusionTablesLayer({
				    suppressInfoWindows: true, 
				    query:{
				      
				      from : '1zeYergZMutKoTHxC-BS2aEOmZtcyzfsDULL6W2IE'
				    },
				    styles:[{
				      where: " 'Estado' = 'Nuevo León'",
				      polygonOptions: {
				      fillColor: '#D9F3AB'
				      }
				    
				    }]
	  			});

				layer.setMap(map);	

					


			}else if (mun == Puebla) {
					var map = new google.maps.Map(document.getElementById('map'), {
	    			center: {lat: 18.833333, lng: -98},
	   				zoom: 6
	  				});
					

					var layer = new google.maps.FusionTablesLayer({
				    suppressInfoWindows: true, 
				    query:{
				      
				      from : '1zeYergZMutKoTHxC-BS2aEOmZtcyzfsDULL6W2IE'
				    },
				    styles:[{
				      where: " 'Estado' = 'Puebla'",
				      polygonOptions: {
				      fillColor: '#06AAFF'
				      }
				    
				    }]
	  			});

				layer.setMap(map);
			}
			
			
		});
});
