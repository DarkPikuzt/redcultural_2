-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-05-2019 a las 19:35:53
-- Versión del servidor: 10.1.10-MariaDB
-- Versión de PHP: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `boletines_recultivar_mexico`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `boletin1`
--

CREATE TABLE `boletin1` (
  `id` int(11) NOT NULL,
  `NOM` varchar(100) DEFAULT NULL,
  `TIPO_ORG` varchar(100) DEFAULT NULL,
  `COLABS_RM` varchar(200) DEFAULT NULL,
  `COLAB_EXT` varchar(300) DEFAULT NULL,
  `COLAB_GOB` varchar(200) DEFAULT NULL,
  `NOM_EVENT` varchar(200) DEFAULT NULL,
  `FECHA` varchar(100) DEFAULT NULL,
  `NUM_DIAS` varchar(50) DEFAULT NULL,
  `MES` int(11) DEFAULT NULL,
  `ANIO` int(11) DEFAULT NULL,
  `LOC` varchar(100) DEFAULT NULL,
  `MPIO` varchar(100) DEFAULT NULL,
  `EDO` varchar(100) DEFAULT NULL,
  `CVE_GEO` varchar(100) DEFAULT NULL,
  `LATITUD` varchar(100) DEFAULT NULL,
  `LONGITUD` varchar(100) DEFAULT NULL,
  `PUBLIC_OBJ` int(11) DEFAULT NULL,
  `PAT_INT` int(11) DEFAULT NULL,
  `PAT_NAT` int(11) DEFAULT NULL,
  `PAT_TAN` int(11) DEFAULT NULL,
  `ED` int(11) DEFAULT NULL,
  `AR` int(11) DEFAULT NULL,
  `OBJ_EVNT1` int(11) DEFAULT NULL,
  `OBJ_EVNT2` int(11) DEFAULT NULL,
  `OBJ_EVNT3` int(11) DEFAULT NULL,
  `OBJ_EVNT4` int(11) DEFAULT NULL,
  `OBJ_EVNT5` int(11) DEFAULT NULL,
  `OBJ_EVNT6` int(11) DEFAULT NULL,
  `PRINS_ACT` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `boletin1`
--

INSERT INTO `boletin1` (`id`, `NOM`, `TIPO_ORG`, `COLABS_RM`, `COLAB_EXT`, `COLAB_GOB`, `NOM_EVENT`, `FECHA`, `NUM_DIAS`, `MES`, `ANIO`, `LOC`, `MPIO`, `EDO`, `CVE_GEO`, `LATITUD`, `LONGITUD`, `PUBLIC_OBJ`, `PAT_INT`, `PAT_NAT`, `PAT_TAN`, `ED`, `AR`, `OBJ_EVNT1`, `OBJ_EVNT2`, `OBJ_EVNT3`, `OBJ_EVNT4`, `OBJ_EVNT5`, `OBJ_EVNT6`, `PRINS_ACT`) VALUES
(1, 'Laboratorio Escenico A.C.', '1', 'Casa 243, ACCESA Cultura y espect?culos, Colectivo Cultural N?huatl Nikan Tipowi', 'Universidad Veracruzana Intercultural', 'S/D', 'Foro Ciudadano Orizaba: Hacia un Plan de Desarrollo Cultural', '10/01/2018', '1', 1, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 7, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 5),
(2, 'Betania Benitez Rodriguez', '3', 'Vulcanizadora Producciones', 'Radio Universidad Veracruzana, Facultad de Teatro de la Universidad Veracruzana', 'Secretar?a de Cultura, FONCA', 'Taller de Producci?n Radiof?nica', '02/02/2018', '12', 2, 2018, 'Xalapa-Enr?quez', 'Xalapa', 'Veracruz', '300870001', '19.5408338', '-96.9146374', 7, 0, 0, 0, 2, 0, 0, 2, 0, 0, 0, 0, 5),
(3, 'Betania Benitez Rodriguez', '3', 'Compa??a Cabaret', 'Teatro Bar La Culpa', 'S/D', 'Secuestrando el Amor', '14/02/2018', '2', 2, 2018, 'Xalapa-Enr?quez', 'Xalapa', 'Veracruz', '300870001', '19.5408338', '-96.9146374', 2, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 4),
(4, 'Joel Merino', '3', 'S/D', 'S/D', 'Secretar?a de Educaci?n del Estado de Quer?taro, Centro Educativo y Cultural del Estado de Quer?taro', 'Exposici?n de Pintura Cax?n/C?nit', '22/02/2018', '5', 2, 2018, 'Amealco de Bonfil', 'Amealco de Bonfil', 'Quer?taro', '220010001', '20.1874445', '-100.0691552', 6, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 2),
(5, 'Nodo Sur. Incubadora Cultural', '1', 'S/D', 'Facultad de Letras Espa?olas de la UV', 'S/D', 'Curso. Dise?o de Proyectos para convocatorias 2018', '22/02/2018', '4', 2, 2018, 'Xalapa-Enr?quez', 'Xalapa', 'Veracruz', '300870001', '19.5408338', '-96.9146374', 7, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 5),
(6, 'Colectivo Cultural N?huatl Nikan Tipowi', '1', 'S/D', 'Universidad Veracruzana Intercultural (universidad p?blica)', 'S/D', 'Encuentro de Diversidad Ling??stica y Cultural. ', '23/02/2018', '1', 2, 2018, 'Tolapa', 'Tequila', 'Veracruz', '301680021', '18.738889', '-97.086111', 6, 1, 0, 0, 2, 6, 1, 1, 0, 0, 0, 0, 2),
(7, 'Casa 243. Foro-Galer?a. Foro-Galer?a', '3', 'S/D', 'S/D', 'S/D', 'Tatau. Instalaci?n. Luis Meza (iojos).', '23/02/2018', '1', 2, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 4),
(8, 'Cine Teatro Ar Z? Amealco', '1', 'S/D', 'S/D', 'Secretar?a de Educaci?n del Estado de Quer?taro, Centro Educativo y Cultural del Estado de Quer?taro', 'Proyecci?n de cortos. Festival de Lengua, Arte y Cultura Otom?', '25/02/2018', '1', 2, 2018, 'Amealco de Bonfil', 'Amealco de Bonfil', 'Quer?taro', '220010001', '20.1874445', '-100.0691552', 6, 0, 0, 0, 0, 2, 1, 0, 1, 0, 0, 0, 2),
(9, 'Joel Merino', '3', 'S/D', 'S/D', 'Secretar?a de Educaci?n del Estado de Quer?taro, Centro Educativo y Cultural del Estado de Quer?taro', 'Conferencia Arte Tijunei', '26/02/2018', '1', 2, 2018, 'Amealco de Bonfil', 'Amealco de Bonfil', 'Quer?taro', '220010001', '20.1874445', '-100.0691552', 6, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 2),
(10, 'ACCESA Cultura y espect?culos', '2', 'S/D', 'Foro Esc?nico El Nicho', 'S/D', '"Botitas". Direcci?n Itzel Villalobos', '02/03/2018', '5', 3, 2018, 'Her?ica Puebla de Zaragoza', 'Puebla', 'Puebla', '211140001', '19.0437227', '-98.1984744', 2, 0, 0, 0, 0, 6, 0, 0, 1, 0, 0, 0, 4),
(11, 'Betania Benitez Rodriguez', '3', 'Vulcanizadora Producciones, Compa??a Cabaret', ' Reflejos Teatro', ' Centro Recreativo Xalape?o del Ayuntamiento de Xalapa', 'Las Silla', '02/03/2018', '2', 3, 2018, 'Xalapa-Enr?quez', 'Xalapa', 'Veracruz', '300870001', '19.5408338', '-96.9146374', 6, 0, 0, 0, 0, 6, 0, 0, 1, 0, 0, 0, 4),
(12, 'Kaff? Real', '4', 'S/D', 'S/D', 'S/D', 'Jazz en Orizaba, Veracruz. Dueto Angie Alamillo y Crist?bal Cort?s. Invitado especial Olson Joseph', '03/03/2018', '1', 3, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 0, 0, 0, 0, 5, 0, 0, 1, 0, 0, 1, 5),
(13, 'Hotel Imperial. Centro Cultural Comunitario', '3', 'S/D', 'Consejo Editorial Cordob?s', 'S/D', 'Slam Po?tico. Poetas VS Raperos', '03/03/2018', '1', 3, 2018, 'C?rdoba', 'C?rdoba', 'Veracruz', '300440001', '18.8944704', '-96.9352891', 2, 0, 0, 0, 0, 4, 0, 1, 1, 0, 0, 0, 7),
(14, 'Casa de Cultura La Concepci?n', '1', 'S/D', 'Cinema Colecta Xalapa', 'S/D', 'Cine Club comunitario', '03/03/2018', '6', 3, 2018, 'La Concepci?n', 'Jilotepec', 'Veracruz', '300930002', '19.605556', '-96.899167', 6, 0, 0, 0, 0, 2, 0, 1, 1, 0, 0, 0, 4),
(15, 'Colectivo Cerro del Borrego', '2', 'S/D', 'Club Lobo Alpino, ?guilas Reales', 'S/D', 'Recorrido por el Cerro del Borrego. Ruta: Orizaba-Ixhuatlancillo.', '04/03/2018', '1', 3, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 0, 4, 0, 0, 0, 0, 0, 1, 0, 0, 0, 7),
(16, 'Betania Benitez Rodriguez', '3', 'Vulcanizadora Producciones, Compa??a Cabaret', 'Ayuntamiento de Xalapa', 'Ayuntamiento de Xalapa', 'Taller de introducci?n al Teatro Cabaret Europeo', '15/03/2018', '3', 3, 2018, 'Xalapa-Enr?quez', 'Xalapa', 'Veracruz', '300870001', '19.5408338', '-96.9146374', 2, 0, 0, 0, 0, 6, 0, 1, 0, 0, 0, 0, 1),
(17, 'Videoteca Bengala', '1', 'S/D', 'Documental Ambulante A.C. Veracruz', 'S/D', 'Taller Te?rico-Pr?ctico. Mapas y rutas para hacer un documental. Impartido por Christiane Burkhard', '16/03/2018', '3', 3, 2018, 'Ciudad Mendoza', 'Camerino Z. Mendoza', 'Veracruz', '300300001', '18.8040896', '-97.1811656', 7, 0, 0, 0, 2, 2, 0, 1, 0, 0, 0, 0, 1),
(18, 'Ciartes y S? A.C.', '4', 'S/D', 'La Casa Encantada y Mala Vida Promotora Cultural', 'S/D', 'Cuerpalabra', '17/03/2018', '1', 3, 2018, 'Pachuca de Soto', 'Pachuca de Soto', 'Hidalgo', '130480001', '20.1165413', '-98.7413535', 3, 0, 0, 0, 2, 6, 0, 1, 1, 0, 0, 0, 1),
(19, 'Casa de Cultura La Concepci?n', '1', 'S/D', 'S/D', 'S/D', 'Taller de Son Jarocho', '18/03/2018', '1', 3, 2018, 'La Concepci?n', 'Jilotepec', 'Veracruz', '300930002', '19.605556', '-96.899167', 3, 3, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1),
(20, 'Alma Delia Hern?ndez Herrera', '4', 'S/D', 'S/D', 'S/D', 'Prevenci?n de salud para el Adulto Mayor', '26/03/2018', '2', 3, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 4, 0, 0, 0, 2, 0, 0, 1, 0, 0, 0, 0, 7),
(21, 'Laboratorio Esc?nico A.C.', '1', 'S/D', 'Observatorio de Pol?ticas Culturales FAUV, Biblioteca Municipal Heriberto Jara de Nogales, Veracruz', 'Biblioteca Municipal Heriberto Jara de Nogales, Veracruz', 'Taller Comunicaci?n y redes de gestores culturales. Detonando Desarrollo Cultura', '31/03/2018', '1', 3, 2018, 'Nogales', 'Nogales', 'Veracruz', '301150001', '18.8222858', '-97.1647214', 7, 0, 0, 0, 2, 0, 0, 2, 0, 0, 0, 0, 5),
(22, 'Nodo Sur. Incubadora Cultural', '1', 'S/D', 'Fundaci?n UV, Facultad de Letras Espa?olas', 'S/D', 'Diplomados en Gesti?n Cultural Sustentable y Emprendimientos Sociales y Culturales.', '05/04/2018', '68', 4, 2018, 'Xalapa-Enr?quez', 'Xalapa', 'Veracruz', '300870001', '19.5408338', '-96.9146374', 7, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 5),
(23, 'Betania Benitez Rodriguez', '3', 'S/D', 'Compa??a Cabaret Xalapa, Facultad de Teatro Universidad Veracruzana, La Tasca, Cuatro Luces Laboratorio Multimedia,  Ojos Negros Caf? Lauder?a, ComiCompa?ia, Obra Negra, La Enamorada, El Refugio de Do?a Ade', 'Ayuntamiento de Xalapa', 'Primer Encuentro de Cabaret Xalapa 2018. Festival "D?a Mundial del Teatro', '06/04/2018', '3', 4, 2018, 'Xalapa-Enr?quez', 'Xalapa', 'Veracruz', '300870001', '19.5408338', '-96.9146374', 7, 0, 0, 0, 0, 6, 1, 0, 1, 0, 0, 0, 2),
(24, 'Orlando Bautista Villegas', '3', 'Kaff? Real', 'Entenados de Stanislavsky Teatro, Casa de Viento, Consultor?a Psicol?gica y Vocacional', 'S/D', 'Celos, infidelidad y otros Volcanes', '14/04/2018', '2', 4, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 7, 0, 0, 0, 2, 0, 0, 2, 0, 0, 0, 0, 1),
(25, 'Istante Studio', '4', 'La Nun.k', 'Museo El Centenario, Museo Metropolitano, Kirei Maid Caf?', 'S/D', 'Tiakesh', '20/04/2018', '3', 4, 2018, 'Monterrey', 'Monterrey', 'Nuevo Le?n', '190390001', '25.6802019', '-100.3152586', 6, 0, 0, 0, 0, 5, 1, 0, 1, 0, 0, 0, 4),
(26, 'Hotel Imperial. Centro Cultural Comunitario', '3', 'S/D', 'S/D', 'S/D', 'X Encuentro Hispanoamericano de Cine y Video Documental Independiente: Contra el Silencio todas las Voces', '21/04/2018', '1', 4, 2018, 'C?rdoba', 'C?rdoba', 'Veracruz', '300440001', '18.8944704', '-96.9352891', 6, 0, 0, 0, 0, 2, 1, 0, 0, 0, 0, 0, 2),
(27, 'Cine Teatro Ar Z? Amealco', '1', 'S/D', 'S/D', 'S/D', 'D?a de la Tierra. Luxoscopio "Traves?a". Foto instalaci?n', '23/04/2018', '1', 4, 2018, 'Amealco de Bonfil', 'Amealco de Bonfil', 'Quer?taro', '220010001', '20.1874445', '-100.0691552', 6, 0, 4, 0, 0, 1, 1, 1, 0, 0, 0, 0, 4),
(28, 'Casa 243. Foro-Galer?a', '3', 'S/D', 'Colectivo Chuleles', 'S/D', 'Talleres de Arte Textil Transdisciplinario', '26/04/2018', '4', 4, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 5, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1),
(29, 'Galer?a Casa de la Nube', '3', 'S/D', 'S/D', 'S/D', 'Cl?nica de poes?a con el poeta chileno V?ctor Hugo D?az', '27/04/2018', '1', 4, 2018, 'San Buenaventura Atempan', 'Tlaxcala', 'Tlaxcala', '290330009', '19.326667', '-98.221944', 6, 0, 0, 0, 2, 4, 0, 1, 0, 0, 0, 0, 1),
(30, 'Kaff? Real', '4', 'S/D', 'Calambre de Yeso, Leyendas del Bardo, Asociaci?n de Artistas Pl?sticos de M?xico, A.C., Nuestras Ra?ces Nuestro Cielo', 'S/D', 'Juegos sobre la Mesa. Rol Lengua Hermana', '28/04/2018', '1', 4, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 2, 0, 0, 0, 0, 7, 0, 1, 1, 0, 0, 0, 7),
(31, 'Casa 243. Foro-Galer?a', '3', 'S/D', 'S/D', 'S/D', 'Bombazo Tropical!', '29/04/2018', '1', 4, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 0, 0, 0, 0, 5, 0, 0, 1, 0, 0, 0, 7),
(32, 'Orlando Bautista Villegas', '3', 'S/D', 'Entenados de Stanislavsky', ' Casa de Cultura de Ciudad Mendoza', 'Teatro para los ni?os y no tan ni?os', '30/04/2018', '1', 4, 2018, 'Ciudad Mendoza', 'Camerino Z. Mendoza', 'Veracruz', '300300001', '18.8040896', '-97.1811656', 6, 0, 0, 0, 0, 6, 0, 1, 1, 0, 0, 0, 4),
(33, 'Casa de Cultura La Concepci?n', '1', 'S/D', 'S/D', 'S/D', 'Talleres de Artes Pl?sticas', 'S/D', 'S/D', 4, 2018, 'La Concepci?n', 'Jilotepec', 'Veracruz', '300930002', '19.605556', '-96.899167', 6, 0, 0, 0, 2, 1, 0, 1, 0, 0, 0, 0, 1),
(34, 'Casa de Cultura La Concepci?n', '1', 'S/D', 'S/D', 'S/D', 'Taller de Baile Ritmos Latinos', 'S/D', 'S/D', 4, 2018, 'La Concepci?n', 'Jilotepec', 'Veracruz', '300930002', '19.605556', '-96.899167', 2, 0, 0, 0, 2, 0, 0, 0, 1, 0, 0, 0, 1),
(35, 'Cine Teatro Ar Z? Amealco', '1', 'S/D', 'S/D', 'S/D', 'Cinedebate', '03/05/2018', '5', 5, 2018, 'Amealco de Bonfil', 'Amealco de Bonfil', 'Quer?taro', '220010001', '20.1874445', '-100.0691552', 6, 0, 0, 0, 2, 2, 0, 1, 1, 0, 0, 0, 7),
(36, 'Cine Teatro Ar Z? Amealco', '1', 'S/D', 'S/D', 'S/D', 'Concierto de Trova Vida: con Hugo Rojas y Rodrigo Olmedo Fao', '06/05/2018', '1', 5, 2018, 'Amealco de Bonfil', 'Amealco de Bonfil', 'Quer?taro', '220010001', '20.1874445', '-100.0691552', 6, 0, 0, 0, 0, 5, 0, 0, 1, 0, 0, 0, 4),
(37, 'Orlando Bautista Villegas', '3', 'Kaff? Real', 'Entenados de Stanislavsky, Teatro Paso Nocturno, Teatro Cavern?cola, Universidad Veracruzana, Caja de Wevos, Idea de Orienta, Teatro en Casa', 'S/D', 'Doceavo Festival independiente de artes esc?nicas Entenados de Stanislavsky', '09/05/2018', '3', 5, 2018, 'C?rdoba ', 'C?rdoba', 'Veracruz', '300440001', '18.8944704', '-96.9352891', 6, 0, 0, 0, 0, 6, 0, 0, 1, 0, 0, 0, 2),
(38, 'Orlando Bautista Villegas', '3', 'Kaff? Real', 'Entenados de Stanislavsky, Teatro Paso Nocturno, Teatro Cavern?cola, Universidad Veracruzana, Caja de Wevos, Idea de Orienta, Teatro en Casa', 'S/D', 'Doceavo Festival independiente de artes esc?nicas Entenados de Stanislavsky', '09/05/2018', '3', 5, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 0, 0, 0, 0, 6, 0, 0, 1, 0, 0, 0, 2),
(39, 'Kaff? Real', '4', 'S/D', 'S/D', 'S/D', 'Celebrando a Mam?. M?sica de: Jaime Reyes, Pepe Reyes y Andrea Kamarillo', '10/05/2018', '1', 5, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 3, 0, 0, 0, 0, 5, 0, 0, 1, 0, 0, 0, 4),
(40, 'Casa 243. Foro-Galer?a', '3', 'S/D', 'Colectivo de Familias Desaparecidas de Orizaba', 'S/D', 'Una Madre Nunca Olvida', '10/05/2018', '1', 5, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 7),
(41, 'Cine Teatro Ar Z? Amealco', '1', 'S/D', 'S/D', 'S/D', 'Mujeres y Tejidos: elaboraci?n de artesan?a con lana de Tlaquilpa, Ver.', '11/05/2018', '1', 5, 2018, 'Amealco de Bonfil', 'Amealco de Bonfil', 'Quer?taro', '220010001', '20.1874445', '-100.0691552', 6, 5, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1),
(42, 'Videoteca Bengala', '1', 'S/D', 'S/D', 'S/D', 'Jornada de Limpieza de la Videoteca Bengala', '11/05/2018', '1', 5, 2018, 'Ciudad Mendoza', 'Camerino Z. Mendoza', 'Veracruz', '300300001', '18.8040896', '-97.1811656', 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 7),
(43, 'Hotel Imperial. Centro Cultural Comunitario', '3', 'S/D', 'S/D', 'S/D', 'Taller de Tareas. Aprender a ser y saber hacer.', '12/05/2018', '1', 5, 2018, 'C?rdoba ', 'C?rdoba', 'Veracruz', '300440001', '18.8944704', '-96.9352891', 1, 0, 0, 0, 2, 0, 1, 2, 0, 0, 0, 0, 1),
(44, 'Hotel Imperial. Centro Cultural Comunitario', '3', 'S/D', 'S/D', 'S/D', 'Construcci?n Huerto Comunitario', '12/05/2018', '1', 5, 2018, 'C?rdoba', 'C?rdoba', 'Veracruz', '300440001', '18.8944704', '-96.9352891', 6, 0, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1),
(45, 'ACCESA Cultura y espect?culos', '2', 'Casa 243', 'Red Cultural de las Altas Monta?as', 'S/D', 'Red Cultural de las Altas Monta?as', '16/05/2018', '1', 5, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 7, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 5),
(46, 'Colectivo Cultural N?huatl Nikan Tipowi', '1', 'Casa 243', 'S/D', 'S/D', 'Curso de lengua y cultural N?huatl', '22/05/2018', 'S/D', 5, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1),
(47, 'Videoteca Bengala', '1', 'S/D', 'Consejo Editorial Cordob?s, Casa Baltazar', 'Direcci?n de Cultura del Ayuntamiento de Camerino Z. Mendoza', 'Palomazo Literario', '24/05/2018', '1', 5, 2018, 'Ciudad Mendoza', 'Camerino Z. Mendoza', 'Veracruz', '300300001', '18.8040896', '-97.1811656', 6, 0, 0, 0, 0, 4, 0, 0, 1, 0, 0, 0, 7),
(48, 'Casa 243. Foro-Galer?a', '3', 'S/D', 'Galer?a Borbua.', 'S/D', 'Galeria Borbua. 1? Exposici?n Colectiva', '24/05/2018', '1', 5, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 4),
(49, 'Kaff? Real', '4', 'S/D', 'S/D', 'S/D', 'Los Choclok. Sonido M?stico', '25/05/2018', '1', 5, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 0, 0, 0, 0, 5, 0, 0, 1, 0, 0, 0, 4),
(50, 'ZienCuentos narrador oral', '3', 'S/D', 'S/D', 'Museo del Barro, Gobierno del Estado de M?xico', 'ZienCuentos', '25/05/2018', '3', 5, 2018, 'Barrio Santiaguito', 'Metepec', 'M?xico', '150540102', '19.22899', '-99.6235', 6, 0, 0, 0, 0, 4, 0, 0, 1, 0, 0, 0, 7),
(51, 'Cine Teatro Ar Z? Amealco', '1', 'S/D', 'S/D', 'S/D', 'Magno concierto de viol?n del maestro Hiram Herrera. Concierto Barroco.', '03/06/2018', '1', 6, 2018, 'Amealco de Bonfil', 'Amealco de Bonfil', 'Quer?taro', '220010001', '20.1874445', '-100.0691552', 6, 0, 0, 0, 0, 5, 1, 0, 1, 0, 0, 0, 4),
(52, 'CicloCinema', '1', 'S/D', 'Universidad Descartes, Cocoliche, Galer?a Latozi, La Vi?a de Baco, Cine Bajo el Cielo, Open Air Cinema', 'Secretar?a de Cultura, FONCA, Consejo Estatal para la Cultura y las Artes de Chiapas, Casa de Cultura de Zinacant?n, Sistema chiapaneco de radio televisi?n y cinematograf?a', 'Taller de Animaci?n para ni?os y ni?as', '04/06/2018', '6', 6, 2018, 'Zinacant?n', 'Zinacant?n', 'Chiapas', '71110001', '16.7604118', '-92.7217949', 1, 0, 0, 0, 2, 2, 0, 1, 0, 0, 0, 0, 1),
(53, 'Ciartes y S? A.C.', '4', 'S/D', 'Corredor Latinoamericano de Teatro', 'Sala de Arte P?blico Siqueiros, Secretar?a de Cultura/INBA\n', 'Taller de Separar la Emoci?n del Sentimiento. Festival Latinoasmericano de Teatro. Version 7mx', '04/06/2018', '9', 6, 2018, 'Pachuca de Soto', 'Pachuca de Soto', 'Hidalgo', '130480001', '20.1165413', '-98.7413535', 7, 0, 0, 0, 0, 6, 0, 1, 0, 0, 0, 0, 2),
(54, 'Betania Benitez Rodriguez', '3', 'S/D', 'Vulcanizadora Producciones, Compa??a Cabaret, Ojos Negros Caf? Lauder?a, Cuatro Luces Laboratorio Multimedia', 'Centro Recreativo Xalape?o del Ayuntamiento de Xalapa', 'Taller de Teatro Cabaret', '06/06/2018', '4', 6, 2018, 'Xalapa-Enr?quez', 'Xalapa', 'Veracruz', '300870001', '19.5408338', '-96.9146374', 2, 0, 0, 0, 0, 6, 0, 2, 0, 0, 0, 0, 1),
(55, 'Cine Teatro Ar Z? Amealco', '1', 'S/D', 'S/D', 'S/D', 'Proyecci?n de "La Educaci?n Prohibida"', '07/06/2018', '1', 6, 2018, 'Amealco de Bonfil', 'Amealco de Bonfil', 'Quer?taro', '220010001', '20.1874445', '-100.0691552', 7, 0, 0, 0, 0, 2, 0, 1, 1, 0, 0, 0, 4),
(56, 'Laboratorio Esc?nico A.C.', '1', 'Arte en Ra?z', 'Fiesta Patronal del Sagrado Coraz?n de Jes?s', 'S/D', 'III Encuentro de Escritores Poes?a en el Volc?n', '08/06/2018', '3', 6, 2018, 'Mariano Escobedo', 'Mariano Escobedo', 'Veracruz', '301010001', '18.914167', '-97.13', 6, 0, 0, 0, 0, 4, 0, 1, 1, 0, 0, 0, 2),
(57, 'Cine Teatro Ar Z? Amealco', '1', 'S/D', 'S/D', 'S/D', 'Ponencia: Derechos Humanos de las mujeres, en el ejercicio de crear comunidad en espacios seguros', '09/06/2018', '1', 6, 2018, 'Amealco de Bonfil', 'Amealco de Bonfil', 'Quer?taro', '220010001', '20.1874445', '-100.0691552', 6, 0, 0, 0, 2, 0, 0, 1, 0, 0, 0, 0, 3),
(58, 'Cine Teatro Ar Z? Amealco', '1', 'S/D', 'S/D', 'S/D', 'Conferencia: Nacimiento respetado', '10/06/2018', '1', 6, 2018, 'Amealco de Bonfil', 'Amealco de Bonfil', 'Quer?taro', '220010001', '20.1874445', '-100.0691552', 6, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 3),
(59, 'Casa 243. Foro-Galer?a', '3', 'S/D', 'S/D', 'S/D', 'Jarana y Voz Taller. Tereso Vega', '11/06/2018', '5', 6, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 3, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1),
(60, 'Casa 243. Foro-Galer?a', '3', 'S/D', 'S/D', 'S/D', 'Biblioteca de lo Diminuto. Libro-objeto y bestiario entomol?gico de Vidriana Aguirre Luna.', '15/06/2018', '1', 6, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 0, 0, 0, 0, 4, 1, 1, 0, 0, 0, 0, 4),
(61, 'Casa 243. Foro-Galer?a', '3', 'S/D', 'S/D', 'S/D', 'Fandango. Clausura del Taller con Tereso Vega', '15/06/2018', '1', 6, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 3, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 7),
(62, 'Galer?a Casa de la Nube', '3', 'S/D', 'S/D', 'S/D', 'Presentaci?n del libro La virgen de Ocotl?n', '15/06/2018', '1', 6, 2018, 'San Buenaventura Atempan', 'Tlaxcala', 'Tlaxcala', '290330009', '19.326667', '-98.221944', 7, 0, 0, 0, 0, 4, 1, 0, 0, 0, 0, 0, 4),
(63, 'CicloCinema', '1', 'S/D', 'Open Air Cinema, Cine Bajo el Cielo, Universidad Descartes, Zarape Films', 'Secretar?a de Cultura, FONCA, Sistema Chiapaneco de Radio, Televisi?n y Cinematograf?a', 'Muestra itinerante de Cine', '22/06/2018', '3', 6, 2018, 'Comit?n de Dominguez', 'Comit?n de Dominguez', 'Chiapas', '70190001', '16.2441097', '-92.1348514', 6, 0, 0, 0, 0, 2, 1, 0, 1, 0, 0, 0, 7),
(64, 'Kaff? Real', '4', 'S/D', 'S/D', 'S/D', 'Ac?stico. Denis Fabela, Audioestopista, Mercedes Barranco y Sinhu?', '22/06/2018', '1', 6, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 0, 0, 0, 0, 5, 0, 0, 1, 0, 0, 0, 3),
(65, 'Betania Benitez Rodriguez', '3', 'Vulcanizadora Producciones', 'El Timbre A.C., El Tel?n de Asfalto, La Casa de Nadie, Ojos Negros Caf? Lauder?a', 'Ayuntamiento de Xalapa', 'El divertido show del T?o Tlacuache', '22/06/2018', '3', 6, 2018, 'Xalapa-Enr?quez', 'Xalapa', 'Veracruz', '300870001', '19.5408338', '-96.9146374', 1, 0, 0, 0, 0, 6, 1, 1, 1, 0, 0, 0, 7),
(66, 'Kaff? Real', '4', 'S/D', 'S/D', 'S/D', 'Cuentos de una Noche de Verano', '23/06/2018', '1', 6, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 0, 0, 0, 0, 4, 0, 0, 1, 0, 0, 0, 7),
(67, 'Cine Teatro Ar Z? Amealco', '1', 'S/D', 'S/D', 'S/D', 'Taller de Found poetry/poes?a hallada', '23/06/2018', '1', 6, 2018, 'Amealco de Bonfil', 'Amealco de Bonfil', 'Quer?taro', '220010001', '20.1874445', '-100.0691552', 2, 0, 0, 0, 0, 4, 0, 1, 0, 0, 0, 0, 1),
(68, 'Casa de Cultura La Concepci?n', '1', 'S/D', 'S/D', 'S/D', 'Caminata Familiar ', '23/06/2018', '2', 6, 2018, 'La Concepci?n', 'Jilotepec', 'Veracruz', '300930002', '19.605556', '-96.899167', 6, 0, 4, 0, 0, 0, 1, 0, 1, 0, 0, 0, 7),
(69, 'Kaff? Real', '4', 'S/D', 'S/D', 'S/D', 'Body & Soul', '30/06/2018', '1', 6, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 0, 0, 0, 0, 5, 0, 0, 1, 0, 0, 0, 4),
(70, 'Ciartes y S? A.C.', '4', 'S/D', 'Circuito V', 'S/D', 'Circu?to V Presenta: Ciclo de Proyecciones', 'S/D', '5', 6, 2018, 'Pachuca de Soto', 'Pachuca de Soto', 'Hidalgo', '130480001', '20.1165413', '-98.7413535', 6, 0, 0, 0, 0, 2, 1, 0, 1, 0, 0, 0, 4),
(71, 'Ciartes y S? A.C.', '4', 'S/D', 'S/D', 'S/D', 'Samsara-Arasmas. EnriqueSoberanes, Poes?a.Guillermo Nava, Retroproyecciones', 'S/D', '1', 6, 2018, 'Pachuca de Soto', 'Pachuca de Soto', 'Hidalgo', '130480001', '20.1165413', '-98.7413535', 6, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 4),
(72, 'Centro Cultural Autogestivo Conejo Clandestino ', '2', 'S/D', 'S/D', 'S/D', 'Bordamos la memoria', '27/06/2018', '2', 7, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 0, 0, 0, 0, 7, 1, 0, 0, 0, 0, 1, 7),
(73, 'CicloCinema', '1', 'S/D', 'Open Air Cinema, Cine Bajo el Cielo', 'Secretar?a de Cultura, FONCA, Casa de la Cultura de Palenque', 'Muestra itinerante de Cine', '08/07/2018', '2', 7, 2018, 'Palenque', 'Palenque', 'Chiapas', '70650001', '17.509188', '-91.980922', 1, 0, 0, 0, 2, 2, 0, 1, 0, 0, 0, 0, 7),
(74, 'Betania Benitez Rodriguez', '3', 'Vulcanizadora Producciones', 'Casa Naran', 'S/D', 'Taller de Verano', '09/07/2018', '11', 7, 2018, 'Xalapa-Enr?quez', 'Xalapa', 'Veracruz', '300870001', '19.5408338', '-96.9146374', 1, 0, 0, 0, 0, 7, 0, 0, 1, 0, 0, 0, 1),
(75, 'Talleres de Son Jarocho', '2', 'S/D', 'La Casa de Nadie', 'S/D', 'Presentaci?n de la P?gina web Talleres de Son jarocho y Fandango', '13/07/2018', '1', 7, 2018, 'Xalapa-Enr?quez', 'Xalapa', 'Veracruz', '300870001', '19.5408338', '-96.9146374', 7, 3, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 5),
(76, 'Colectivo Cerro del Borrego', '2', 'S/D', 'Club Lobo Alpino, ?guilas Reales', 'Club Lobo Alpino, ?guilas Reales', 'Reforestaci?n Ciudadana Cerro del Borrego 2018', '15/07/2018', '1', 7, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 0, 1, 0, 0, 0, 0, 2, 0, 0, 0, 0, 7),
(77, 'Casa 243. Foro-Galer?a', '3', 'S/D', 'S/D', 'S/D', 'C?mara en Movimiento. Taller de introducci?n a la producci?n y realizaci?n de documental', '16/07/2018', '5', 7, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 2, 0, 0, 0, 2, 2, 0, 1, 0, 0, 0, 0, 1),
(78, 'Casa de Cultura La Concepci?n', '1', 'S/D', 'S/D', 'S/D', 'Talleres de Verano (Idiomas, artes, baile, acondicionamiento f?sico)', '16/07/2018', '15', 7, 2018, 'La Concepci?n', 'Jilotepec', 'Veracruz', '300930002', '19.605556', '-96.899167', 1, 0, 0, 0, 2, 0, 0, 0, 1, 0, 0, 0, 1),
(79, 'Casa 243. Foro-Galer?a', '3', 'S/D', 'Red Cultural de las Altas Monta?as', 'S/D', 'Talleres de Verano', '16/07/2018', '19', 7, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 1, 0, 0, 0, 2, 0, 0, 0, 1, 0, 0, 0, 1),
(80, 'Espora Producciones', '3', 'S/D', 'Cine de la Cuenca, R?o Salvaje', 'S/D', '2? Taller de Cine Documental Cine del R?o. Historias de Jalcomulco', '16/07/2018', '8', 7, 2018, 'Jalcomulco', 'Jalcomulco', 'Veracruz', '300880001', '19.33333', '-96.66667', 2, 0, 0, 0, 0, 2, 0, 1, 0, 0, 0, 0, 5),
(81, 'ZienCuentos narrador oral', '3', 'S/D', 'Fundaci?n Ideas Libres, Be Executive Coach', 'S/D', 'Conferencia Neuro Oratoria. 10 T?cnicas de comunicaci?n afectiva', '21/07/2018', '1', 7, 2018, 'Toluca de Lerdo', 'Toluca', 'M?xico', '151060001', '19.292545', '-99.6569007', 7, 0, 0, 0, 2, 0, 0, 1, 0, 0, 0, 0, 4),
(82, 'Academia de las Artes y las Ciencias Cinematogr?ficas de Hidalgo', '1', 'S/D', 'Red Mexicana de Festivales Cinematogr?ficos, eduMac Pachuca, Quimera Producciones, El Capit?n Comedor, Estudios Galaz Sennheiser R?DE Microphones TASCAM, PNY M?xico, SOLIA Soluciones Integrales & Asistencia', 'Secretar?a de Cultura, Instituto Mexicano de Cinematograf?a', 'Hidalgo Film Fest VII Edici?n', '23/07/2018', '6', 7, 2018, 'Pachuca de Soto', 'Pachuca de Soto', 'Hidalgo', '130480001', '20.1165413', '-98.7413535', 6, 0, 0, 0, 0, 2, 1, 0, 1, 0, 0, 0, 2),
(83, 'Coordinaci?n de cultura de Nogales Veracruz', '1', 'Biblioteca municipal El Encinar', 'S/D', 'S/D', 'Talleres de Verano (computaci?n, artes, ciencia, cocina, primeros auxilios)', '23/07/2018', '18', 7, 2018, 'Nogales', 'Nogales', 'Veracruz', '301150001', '18.8222858', '-97.1647214', 1, 0, 0, 0, 2, 0, 0, 1, 1, 0, 0, 0, 1),
(84, 'Kaff? Real', '4', 'S/D', 'S/D', 'S/D', 'Conciertos de Leonel Soto y Jair MarVera', '26/07/2018', '1', 7, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 0, 0, 0, 0, 5, 0, 0, 1, 0, 0, 0, 4),
(85, 'Cine Teatro Ar Z? Amealco', '1', 'S/D', 'S/D', 'S/D', 'Taller de plantas medicinales para la mujer', '28/07/2018', '2', 7, 2018, 'Amealco de Bonfil', 'Amealco de Bonfil', 'Quer?taro', '220010001', '20.1874445', '-100.0691552', 3, 4, 4, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1),
(86, 'Colectivo Cultural N?huatl Nikan Tipowi', '1', 'S/D', 'Cafenatlan Buen Caf? Buen vivir', 'S/D', 'Curso-Taller Mitolog?a Nahua', '06/07/02018', '1', 7, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1),
(87, 'Cine Teatro Ar Z? Amealco', '1', 'S/D', 'S/D', 'S/D', 'Proyecci?n pel?cula "Isle of Dogs"', 'S/D', 'S/D', 7, 2018, 'Amealco de Bonfil', 'Amealco de Bonfil', 'Quer?taro', '220010001', '20.1874445', '-100.0691552', 6, 0, 0, 0, 0, 2, 0, 0, 1, 0, 0, 0, 4),
(88, 'Casa 243. Foro-Galer?a', '3', 'S/D', 'Red Cultural de las Altas Monta?as', 'S/D', 'Fiesta Can?cula', '03/08/2018', '1', 8, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 7, 0, 0, 0, 0, 5, 0, 0, 1, 0, 0, 0, 7),
(89, 'Espora Producciones', '3', 'S/D', 'Estudios Galaz, Espora Producciones', 'S/D', 'Taller de sonido directo y dise?o sonoro en Xalapa, Veracruz.', '05/08/2018', '1', 8, 2018, 'Xalapa-Enr?quez', 'Xalapa', 'Veracruz', '300870001', '19.5408338', '-96.9146374', 7, 0, 0, 0, 0, 2, 0, 1, 0, 0, 0, 0, 1),
(90, 'Casa 243. Foro-Galer?a', '3', 'Videoteca Bengala, Kaffe Real', 'Documental Ambulante A.C. Veracruz, Agencia de los Estados Unidos para el Desarrollo Internacional', 'EFICINE 189 - Instituto Mexicano de Cinematograf?a, Ayuntamiento de Orizaba, Secretar?a de Cultura', 'Justicia en tu comunidad', '07/08/2018', '9', 8, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 0, 0, 0, 2, 2, 1, 0, 0, 0, 0, 0, 2),
(91, 'Galer?a Casa de la Nube', '3', 'S/D', 'S/D', 'S/D', 'Laboratorio de poes?a: Cultivando versos', '08/08/2018', 'S/D', 8, 2018, 'San Buenaventura Atempan', 'Tlaxcala', 'Tlaxcala', '290330009', '19.326667', '-98.221944', 2, 0, 0, 0, 2, 4, 0, 1, 0, 0, 0, 0, 1),
(92, 'Casa 243. Foro-Galer?a', '3', 'S/D', 'Red Cultural de las Altas Monta?as', 'S/D', 'Recicla y enchula tu prenda o indumentaria', '09/08/2018', '2', 8, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 2, 0, 0, 0, 2, 7, 0, 0, 1, 0, 0, 0, 7),
(93, 'Cine Teatro Ar Z? Amealco', '1', 'S/D', 'S/D', 'S/D', 'Proyecci?n de documentales "En el Hoyo" y "Etnocidio notas sobre el mezquital"', '10/08/2018', '1', 8, 2018, 'Amealco de Bonfil', 'Amealco de Bonfil', 'Quer?taro', '220010001', '20.1874445', '-100.0691552', 6, 3, 0, 0, 0, 2, 1, 0, 0, 0, 0, 0, 7),
(94, 'Laboratorio Esc?nico A.C.', '1', 'Hotel Imperial. Centro Cultural Comunitario, Gisela Hernandez Mu?oz', '#Cultura271, Casa Baltazar, Centro Cultural de Estudios Esc?nicos de Orizaba, Red Cultural de las Altas Monta?as', 'Casa de la Cultura de C?rdoba', 'La Cultura la hacemos todos', '11/08/2018', '2', 8, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 7, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 5),
(95, 'Casa 243. Foro-Galer?a', '3', 'S/D', 'Red Cultural de las Altas Monta?as', 'S/D', 'Fandango en casa 243', '11/08/2018', '1', 8, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 3, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 7),
(96, 'Kaff? Real', '4', 'S/D', 'S/D', 'S/D', 'Jazz Real Street', '11/08/2018', '1', 8, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 2, 0, 0, 0, 0, 5, 0, 0, 1, 0, 0, 0, 4),
(97, 'Casa 243. Foro-Galer?a', '3', 'Videoteca Bengala', 'Documental Ambulante A.C. Veracruz, Agencia de los Estados Unidos para el Desarrollo Internacional', 'EFICINE 189 - Instituto Mexicano de Cinematograf?a, Secretar?a de Cultura, Biblioteca Municipal de R?o Blanco', 'Justicia en tu comunidad', '14/08/2018', '2', 8, 2018, 'R?o Blanco', 'R?o Blanco', 'Veracruz', '301380001', '18.8369927', '-97.1216906', 6, 0, 0, 0, 2, 2, 1, 0, 0, 0, 0, 0, 2),
(98, 'Casa 243. Foro-Galer?a', '3', 'Videoteca Bengala', 'Documental Ambulante A.C. Veracruz, Agencia de los Estados Unidos para el Desarrollo Internacional, Centro Cultural Casa Baltazar', 'EFICINE 189 - Instituto Mexicano de Cinematograf?a, Secretar?a de Cultura', 'Justicia en tu comunidad', '17/08/2018', '3', 8, 2018, 'C?rdoba', 'C?rdoba', 'Veracruz', '300440001', '18.8944704', '-96.9352891', 6, 0, 0, 0, 2, 2, 1, 0, 0, 0, 0, 0, 2),
(99, 'Galer?a Casa de la Nube', '3', 'S/D', 'S/D', 'S/D', 'Tabla flamenco en Casa de la Nube', '17/08/2018', '1', 8, 2018, 'San Buenaventura Atempan', 'Tlaxcala', 'Tlaxcala', '290330009', '19.326667', '-98.221944', 6, 0, 0, 0, 0, 3, 1, 0, 1, 0, 0, 0, 4),
(100, 'Ccinemedia', '2', 'S/D', 'Jos? Recek Saade Teatro Popular', 'S/D', 'Cinecomuna. Taller de Cine Participativo', '18/08/2018', 'S/D', 8, 2018, 'Her?ica Puebla de Zaragoza', 'Puebla', 'Puebla', '211140001', '19.0437227', '-98.1984744', 2, 0, 0, 0, 2, 2, 0, 1, 0, 0, 0, 0, 5),
(101, 'Kaff? Real', '4', 'S/D', 'S/D', 'S/D', 'Noche Bohemia', '18/08/2018', '1', 8, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 2, 0, 0, 0, 0, 5, 0, 0, 1, 0, 0, 0, 4),
(102, 'Colectivo Cultural N?huatl Nikan Tipowi', '1', 'Casa 243', 'Navegantes del Son', 'S/D', 'Curso de Son Jarocho y Zapateado. Los Animales en el Son', '21/08/2018', '2', 8, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 2, 3, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1),
(103, 'Kaff? Real', '4', 'S/D', 'S/D', 'S/D', 'Los Tiernos', '24/08/2018', '1', 8, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 2, 0, 0, 0, 0, 5, 0, 0, 1, 0, 0, 0, 4),
(104, 'Centro Cultural Autogestivo Conejo Clandestino ', '2', 'S/D', 'Colectivo Akelarre A.C., Redefine Veracruz', 'S/D', 'Primera Jornada informativa: aborto legal, seguro y gratuito', '25/08/2018', '1', 8, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 3, 0, 0, 0, 2, 0, 1, 1, 0, 0, 0, 0, 4),
(105, 'Kaff? Real', '4', 'S/D', 'S/D', 'S/D', 'Mike Carballido', '25/08/2018', '1', 8, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 2, 0, 0, 0, 0, 5, 0, 0, 1, 0, 0, 0, 4),
(106, 'Casa 243. Foro-Galer?a', '3', 'S/D', 'S/D', 'S/D', 'Taller de Creaci?n Po?tica', '26/08/2018', '1', 8, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 2, 0, 0, 0, 2, 4, 0, 1, 0, 0, 0, 0, 1),
(107, 'Casa 243. Foro-Galer?a', '3', 'S/D', 'S/D', 'S/D', 'Sesiones de Narrativa Corporal', '30/08/2018', '1', 8, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 2, 0, 0, 0, 0, 4, 1, 0, 1, 0, 0, 0, 7),
(108, 'Kaff? Real', '4', 'S/D', 'S/D', 'S/D', 'Shay Madrigal', 'S/D', '1', 8, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 2, 0, 0, 0, 0, 5, 0, 0, 1, 0, 0, 0, 4),
(109, 'Betania Benitez Rodriguez', '3', 'Vulcanizadora Producciones', 'Casa Naran', 'S/D', 'Danza africana para ni?os', '03/09/2018', 'S/D', 9, 2018, 'Xalapa-Enr?quez', 'Xalapa', 'Veracruz', '300870001', '19.5408338', '-96.9146374', 1, 0, 0, 0, 0, 7, 1, 1, 0, 0, 0, 0, 1),
(110, 'Cine Teatro Ar Z? Amealco', '1', 'S/D', 'Universidad Aut?noma de Quer?taro, Centro Intercultural ?h??h?', 'S/D', 'Festival de Hongos', '07/09/2018', '3', 9, 2018, 'San Ildefonso Tultepec (Centro)', 'Amealco de Bonfil', 'Quer?taro', '220010039', '20.1444444', '-99.9569444', 6, 4, 4, 0, 0, 0, 1, 0, 0, 0, 0, 0, 2),
(111, 'Kaff? Real', '4', 'S/D', 'S/D', 'S/D', 'De la Mano al Coraz?n. Concierto Cantautores', '07/09/2018', '1', 9, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 2, 0, 0, 0, 0, 5, 0, 0, 1, 0, 0, 0, 4),
(112, 'Centro Cultural Autogestivo Conejo Clandestino ', '2', 'S/D', 'Colectivo Akelarre A.C., Redefine Veracruz', 'S/D', 'Bordamos la memoria. Por las v?ctimas de feminicidio en Veracruz', '08/09/2018', '1', 9, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 0, 0, 0, 0, 7, 1, 0, 0, 0, 0, 1, 7),
(113, 'Kaff? Real', '4', 'S/D', 'S/D', 'S/D', 'Jazz & Jam Session', '08/09/2018', '1', 9, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 2, 0, 0, 0, 0, 5, 0, 0, 1, 0, 0, 0, 4),
(114, 'Academia de las Artes y las Ciencias Cinematogr?ficas de Hidalgo', '1', 'S/D', 'Festival Internacional de Cine de Hidalgo 2018, Hostal Casa Viajero', 'Instituto Mexicano de Cinematograf?a, Secretar?a de Cultura del Estado de Hidalgo', 'Cineclub: Ciclo ?Viva M?xico, viva Hidalgo!', '12/09/2018', '3', 9, 2018, 'Pachuca de Soto', 'Pachuca de Soto', 'Hidalgo', '130480001', '20.1165413', '-98.7413535', 6, 0, 0, 0, 0, 2, 1, 0, 1, 0, 0, 0, 4),
(115, 'ZienCuentos narrador oral', '3', 'S/D', 'S/D', 'Centro Cultural Tijuana, Secretar?a de Cultura', 'Conferencia "Descubre el poder de crear buenas historias"', '12/09/2018', '1', 9, 2018, 'Tijuana', 'Tijuana', 'Baja California', '20040001', '32.5264569', '-116.9999002', 7, 0, 0, 0, 2, 4, 0, 1, 0, 0, 0, 0, 4),
(116, 'Laboratorio Esc?nico A.C.', '1', 'Cirilo Hernandez de la Luz', 'S/D', 'S/D', 'Taller de conservaci?n de alimentos', '22/09/2018', '1', 9, 2018, 'Loma Grande', 'Mariano Escobedo', 'Veracruz', '301010007', '18.928889', '-97.232778', 6, 0, 0, 0, 2, 0, 0, 2, 0, 0, 0, 0, 1),
(117, 'Pan de Burro', '3', 'Kaff? Real', 'S/D', 'S/D', '#Ilustra por M?xico', '23/09/2018', '1', 9, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 2, 0, 0, 0, 0, 7, 1, 0, 0, 0, 0, 0, 7),
(118, 'Red de Huertos Escolares y Comunitarios. Xalapa-Coatepec, Veracruz, M?xico', '1', 'S/D', 'Escuela Normal Veracruzana "Enrique C. R?bsamen"', 'S/D', 'Taller Consumo Responsable y Compostaje', '29/09/2018', '1', 9, 2018, 'Xalapa-Enr?quez', 'Xalapa', 'Veracruz', '300870001', '19.5408338', '-96.9146374', 7, 0, 4, 0, 2, 0, 0, 1, 0, 0, 0, 0, 1),
(119, 'Casa de Cultura La Concepci?n', '1', 'S/D', 'S/D', 'S/D', 'Taller de Baile Ritmos Latinos', 'S/D', 'S/D', 9, 2018, 'La Concepci?n', 'Jilotepec', 'Veracruz', '300930002', '19.605556', '-96.899167', 2, 0, 0, 0, 2, 0, 0, 0, 1, 0, 0, 0, 1),
(120, 'Casa de Cultura La Concepci?n', '1', 'S/D', 'S/D', 'S/D', 'Taller de Danza Folkl?rica', 'S/D', 'S/D', 9, 2018, 'La Concepci?n', 'Jilotepec', 'Veracruz', '300930002', '19.605556', '-96.899167', 6, 0, 0, 0, 2, 0, 0, 1, 0, 0, 0, 0, 1),
(121, 'Laboratorio Esc?nico A.C.', '1', 'S/D', 'Escuela Telesecundaria Emiliano Zapata, Sociedad de Padres de Familia y comunidad de Loma Grande', 'S/D', '17va Estancia de Creaci?n Literaria', '22/10/2016', '5', 10, 2018, 'Loma Grande', 'Mariano Escobedo', 'Veracruz', '301010007', '18.928889', '-97.232778', 2, 0, 0, 0, 2, 4, 1, 1, 0, 0, 0, 0, 7),
(122, 'Pedro Montalvo Nolasco', '4', 'S/D', 'Universidad Veracruzana, UNAM', 'S/D', 'Xochitlalli, Mayordom?a, Todos Santos, Ritos Funerarios y Cosmovisi?n en San Juan del R?o (hoy Rafael Delgado), Veracruz. ', '02/10/2018', '1', 10, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 7, 3, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 4),
(123, 'Academia de las Artes y las Ciencias Cinematogr?ficas de Hidalgo', '1', 'S/D', 'Hostal Casa Viajero', 'S/D', 'Cineclub Octubre', '03/10/2018', '6', 10, 2018, 'Pachuca de Soto', 'Pachuca de Soto', 'Hidalgo', '130480001', '20.1165413', '-98.7413535', 6, 0, 0, 0, 0, 2, 1, 0, 1, 0, 0, 0, 4),
(124, 'Espora Producciones', '3', 'S/D', 'S/D', '?gora de la Ciudad de Xalapa, IVEC, Secretar?a de Cultura', 'Estreno documentales Cine del R?o: Historias de Jalcomulco 2018', '04/10/2018', '1', 10, 2018, 'Xalapa-Enr?quez', 'Xalapa', 'Veracruz', '300870001', '19.5408338', '-96.9146374', 6, 0, 4, 0, 0, 2, 1, 0, 0, 0, 0, 0, 4),
(125, 'Academia de las Artes y las Ciencias Cinematogr?ficas de Hidalgo', '1', 'S/D', 'Festival Internacional de Cine de Hidalgo 2018', 'Instituto Mexicano de Cinematograf?a, Secretar?a de Cultura del Estado de Hidalgo', 'DoctubreMX - Hidalgo', '05/10/2018', '5', 10, 2018, 'Pachuca de Soto', 'Pachuca de Soto', 'Hidalgo', '130480001', '20.1165413', '-98.7413535', 6, 0, 0, 0, 0, 2, 1, 1, 0, 0, 0, 0, 4),
(126, 'Red de Huertos Escolares y Comunitarios. Xalapa-Coatepec, Veracruz, M?xico', '1', 'S/D', 'Colegio Nuestro Mundo', 'S/D', 'Capacitaci?n en Principios de la Horticultura', '09/10/2018', '6', 10, 2018, 'Xalapa-Enr?quez', 'Xalapa', 'Veracruz', '300870001', '19.5408338', '-96.9146374', 7, 0, 2, 0, 2, 0, 0, 2, 0, 0, 0, 0, 7),
(127, 'Ccinemedia', '2', 'S/D', 'Colectivo El Teatro del Puente, Comala Espacio Art?stico', 'S/D', 'Curso/Taller B?sico e Intensivo. Dise?o de Carteles', '23/10/2018', 'S/D', 10, 2018, 'Her?ica Puebla de Zaragoza', 'Puebla', 'Puebla', '211140001', '19.0437227', '-98.1984744', 2, 0, 0, 0, 2, 1, 0, 1, 0, 0, 0, 0, 5),
(128, 'Red de Huertos Escolares y Comunitarios. Xalapa-Coatepec, Veracruz, M?xico', '1', 'S/D', 'Red Chiapaneca de Huertos Educativos, Red Internacional de Huertos Escolares', 'S/D', 'Encuentro Mexicano de Huertos Educativos. Celebrando Or?genes y Tejiendo Redes', '26/10/2018', '3', 10, 2018, 'San Crist?bal de las Casas', 'San Crist?bal de las Casas', 'Chiapas', '70780001', '16.735377', '-92.63873', 7, 0, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 5),
(129, 'Casa de Cultura La Concepci?n', '1', 'S/D', 'Comit? Local del Sindicato Nacional de Trabajadores del Az?car', 'S/D', 'Exposici?n de Altares de Muertos 2018', '27/10/2018', '1', 10, 2018, 'La Concepci?n', 'Jilotepec', 'Veracruz', '300930002', '19.605556', '-96.899167', 6, 3, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 7),
(130, 'Betania Benitez Rodriguez', '3', 'S/D', 'Ayuntamiento de Xalapa', 'Ayuntamiento de Xalapa', 'El amor mas all? de la Muerte', '28/10/2018', '2', 10, 2018, 'Xalapa-Enr?quez', 'Xalapa', 'Veracruz', '300870001', '19.5408338', '-96.9146374', 2, 0, 0, 0, 0, 6, 0, 0, 1, 0, 0, 0, 4),
(131, 'Coordinaci?n de cultura de Nogales Veracruz', '1', 'Biblioteca municipal El Encinar', 'S/D', 'S/D', 'Concurso de Altares de Vida (ofrendas) tradicionales y catrinas', '31/10/2018', '1', 10, 2018, 'Nogales', 'Nogales', 'Veracruz', '301150001', '18.8222858', '-97.1647214', 6, 3, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 7),
(132, 'Desarrollo Sustentable del r?o Sede?o', '1', 'S/D', 'S/D', 'S/D', 'Elaboraci?n de Lombricomposta', 'S/D', '2', 10, 2018, 'Xalapa-Enr?quez', 'Xalapa', 'Veracruz', '300870001', '19.5408338', '-96.9146374', 6, 0, 2, 0, 0, 0, 1, 1, 0, 0, 0, 0, 7),
(133, 'Casa de Cultura La Concepci?n', '1', 'S/D', 'S/D', 'S/D', 'Primer Gran Carrera "Vivos vs Muertos"', '02/11/2018', '1', 11, 2018, 'La Concepci?n', 'Jilotepec', 'Veracruz', '300930002', '19.605556', '-96.899167', 6, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 7),
(134, 'Ccinemedia', '2', 'S/D', 'Jos? Recek Saade Teatro Popular', 'S/D', 'Cinecomuna. Taller de Cine Participativo', '03/11/2018', 'S/D', 11, 2018, 'Her?ica Puebla de Zaragoza', 'Puebla', 'Puebla', '211140001', '19.0437227', '-98.1984744', 2, 0, 0, 0, 2, 2, 0, 1, 0, 0, 0, 0, 5),
(135, 'Betania Benitez Rodriguez', '3', 'S/D', 'Teatro Bar La Culpa, Ondina Cabaret', 'S/D', 'Taller Intensivo de Cabaret', '03/11/2018', '1', 11, 2018, 'Xalapa-Enr?quez', 'Xalapa', 'Veracruz', '300870001', '19.5408338', '-96.9146374', 2, 0, 0, 0, 0, 6, 0, 2, 0, 0, 0, 0, 4),
(136, 'Casa de Cultura La Concepci?n', '', 'S/D', 'Documental Ambulante A.C. Veracruz, Agencia de los Estados Unidos para el Desarrollo Internacional', 'EFICINE 189 - Instituto Mexicano de Cinematograf?a, Secretar?a de Cultura', 'Justicia en tu comunidad', '17/11/2018', '2', 11, 2018, 'La Concepci?n', 'Jilotepec', 'Veracruz', '300930002', '19.605556', '-96.899167', 6, 0, 0, 0, 0, 2, 1, 1, 0, 0, 0, 0, 4),
(137, 'Desarrollo Sustentable del r?o Sede?o', '1', 'S/D', 'INANA A.C., Red de Custodios del Archipi?lago', 'S/D', 'Taller de abejas nativas y la siembra de ?rboles', '17/11/2018', '1', 11, 2018, 'Xalapa-Enr?quez', 'Xalapa', 'Veracruz', '300870001', '19.5408338', '-96.9146374', 6, 0, 2, 0, 0, 0, 1, 1, 0, 0, 0, 0, 7),
(138, 'Alma Delia Hern?ndez Herrera', '4', 'S/D', 'S/D', 'S/D', 'Sesi?n de salud "Consejos para prevenir y mejorar la salud del Adulto Mayor"', '20/11/2018', '1', 11, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 4, 0, 0, 0, 2, 0, 0, 1, 0, 0, 0, 0, 4),
(139, 'Casa de Cultura La Concepci?n', '1', 'S/D', 'Festival Internacional de Guitarras Xalapa', 'S/D', 'Festival Internacional de Guitarras Xalapa', '28/11/2018', '2', 11, 2018, 'La Concepci?n', 'Jilotepec', 'Veracruz', '300930002', '19.605556', '-96.899167', 6, 0, 0, 0, 0, 5, 1, 0, 1, 0, 0, 0, 4),
(140, 'Alma Delia Hern?ndez Herrera', '4', 'S/D', 'S/D', 'S/D', 'Sesi?n de salud Psicol?gica', '29/11/2018', '1', 11, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 4, 0, 0, 0, 2, 0, 0, 1, 0, 0, 0, 0, 4),
(141, 'Alma Delia Hern?ndez Herrera', '4', 'S/D', 'S/D', 'S/D', 'Talleres de Manualidades, lectura, Activaci?n F?sica y mental', 'S/D', '12', 11, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 4, 0, 0, 0, 2, 0, 0, 1, 0, 0, 0, 0, 1),
(142, 'Desarrollo Sustentable del r?o Sede?o', '1', 'S/D', 'S/D', 'S/D', 'Elaboraci?n de Lombricomposta', 'S/D', '2', 11, 2018, 'Xalapa-Enr?quez', 'Xalapa', 'Veracruz', '300870001', '19.5408338', '-96.9146374', 6, 0, 2, 0, 0, 0, 1, 1, 0, 0, 0, 0, 7),
(143, 'Red de Huertos Escolares y Comunitarios. Xalapa-Coatepec, Veracruz, M?xico', '1', 'S/D', 'Preescolar Primaria Tlalnecapam', 'S/D', 'Taller de Reproducci?n de Plantas, semilleros y esquejes', '01/12/2018', '1', 12, 2018, 'Colonia Plan de la Cruz', 'Coatepec', 'Veracruz', '300380023', '19.506389', '-96.9425', 6, 0, 2, 0, 2, 0, 0, 1, 0, 0, 0, 0, 1),
(144, 'Desarrollo Sustentable del r?o Sede?o', '1', 'S/D', 'Red ciudadana Custodios del Archipi?lago, INANA A.C.', 'Ayuntamiento de Xalapa', 'Festival de la Niebla', '01/12/2018', '1', 12, 2018, 'Xalapa-Enr?quez', 'Xalapa', 'Veracruz', '300870001', '19.5408338', '-96.9146374', 6, 0, 4, 0, 0, 0, 1, 0, 1, 0, 0, 0, 2),
(145, 'Talleres de Son Jarocho', '2', 'S/D', 'Nexus', 'S/D', 'Talleres de Son Jarocho', '06/12/2018', '1', 12, 2018, 'Her?ica Puebla de Zaragoza', 'Puebla', 'Puebla', '211140001', '19.0437227', '-98.1984744', 7, 3, 0, 0, 2, 0, 1, 1, 0, 0, 0, 0, 1),
(146, 'Red de Huertos Escolares y Comunitarios. Xalapa-Coatepec, Veracruz, M?xico', '1', 'S/D', 'Centro Comunitario de Tradiciones, Oficios y Saberes de Chiltoyac', 'Direcci?n de Participaci?n Social Ciudadana del Municipio de Xalapa', 'Encuentro Comunitario', '09/12/2018', '1', 12, 2018, 'Xalapa-Enr?quez', 'Xalapa', 'Veracruz', '300870001', '19.5408338', '-96.9146374', 6, 0, 4, 0, 0, 0, 1, 0, 1, 0, 0, 0, 7),
(147, 'Galer?a Casa de la Nube', '3', 'S/D', 'Los Contenedores, El Taller Estampa Venus', 'S/D', 'Subasta navide?a y cena al estilo japon?s', '14/12/2018', '1', 12, 2018, 'San Buenaventura Atempan', 'Tlaxcala', 'Tlaxcala', '290330009', '19.326667', '-98.221944', 7, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 7),
(148, 'Betania Benitez Rodriguez', '3', 'Vulcanizadora Producciones', 'Ojos Negros Caf? Lauder?a', 'Centro Recreativo Xalape?o del Ayuntamiento de Xalapa', 'Danza africana para ni?os', '15/12/2018', '1', 12, 2018, 'Xalapa-Enr?quez', 'Xalapa', 'Veracruz', '300870001', '19.5408338', '-96.9146374', 1, 0, 0, 0, 0, 7, 1, 1, 0, 0, 0, 0, 1),
(149, 'Desarrollo Sustentable del r?o Sede?o', '1', 'S/D', 'S/D', 'S/D', 'Posada-Kermes', '15/12/2018', '1', 12, 2018, 'Xalapa-Enr?quez', 'Xalapa', 'Veracruz', '300870001', '19.5408338', '-96.9146374', 6, 0, 4, 0, 0, 0, 0, 0, 1, 0, 0, 0, 7),
(150, 'Galer?a Casa de la Nube', '3', 'Arte en Ra?z', 'S/D', 'S/D', 'Posada Literario', '21/12/2018', '1', 12, 2018, 'San Buenaventura Atempan', 'Tlaxcala', 'Tlaxcala', '290330009', '19.326667', '-98.221944', 6, 0, 0, 0, 0, 4, 1, 0, 1, 0, 0, 0, 4),
(151, 'Coordinaci?n de cultura de Nogales Veracruz', '1', 'Biblioteca municipal El Encinar', 'S/D', 'S/D', 'Concurso de Ramas. ?Ya lleg? la Rama!', '22/12/2018', '1', 12, 2018, 'Nogales', 'Nogales', 'Veracruz', '301150001', '18.8222858', '-97.1647214', 6, 3, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 7),
(152, 'Kaff? Real', '4', 'S/D', 'S/D', 'S/D', 'M?sica en Vivo: Andrea Kamarillo, Marco Rodriguez y Emmanuel Hern?ndez', 'S/D', 'S/D', 0, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 0, 0, 0, 0, 5, 0, 0, 1, 0, 0, 0, 4),
(153, 'Cine Teatro Ar Z? Amealco', '1', 'S/D', 'Ambulante A.C., ATENTO', 'Eficine Distribuci?n y Producci?n 189, Secretar?a de Cultura', 'Ambulante. Circuito de exhibici?n documental', '03/10/2018', '6', 10, 2018, 'Amealco de Bonfil', 'Amealco de Bonfil', 'Quer?taro', '220010001', '20.1874445', '-100.0691552', 6, 0, 0, 0, 0, 2, 1, 0, 0, 0, 0, 0, 7),
(154, 'Cine Teatro Ar Z? Amealco', '1', 'S/D', 'S/D', 'S/D', 'Pl?tica: Las p?rdidas y el manejo de sus duelos', '31/10/2018', '1', 10, 2018, 'Amealco de Bonfil', 'Amealco de Bonfil', 'Quer?taro', '220010001', '20.1874445', '-100.0691552', 2, 0, 0, 0, 2, 0, 0, 1, 0, 0, 0, 0, 4),
(155, 'Cine Teatro Ar Z? Amealco', '1', 'S/D', 'Chipotle Teatro', 'S/D', 'Teatro de sombras "Ya nos cay? el chahuistle"', '05/10/2018', '1', 10, 2018, 'Amealco de Bonfil', 'Amealco de Bonfil', 'Quer?taro', '220010001', '20.1874445', '-100.0691552', 6, 0, 0, 0, 0, 6, 0, 0, 1, 0, 0, 0, 4),
(156, 'Videoteca Bengala', '1', 'S/D', 'Ambulante A.C., ATENTO', 'Eficine Distribuci?n y Producci?n 189, Secretar?a de Cultura', 'Justicia en tu comunidad', '05/10/2018', '1', 10, 2018, 'Ciudad Mendoza', 'Camerino Z. Mendoza', 'Veracruz', '300300001', '18.8040896', '-97.1811656', 2, 0, 0, 0, 0, 2, 0, 0, 1, 0, 0, 0, 4),
(157, 'Videoteca Bengala', '1', 'S/D', 'Ciclo Colectivo de Cine Mexicano', 'S/D', '4to Ciclo Colectivo de Cine Mexicano. Memorias del Olvido, miradas del 68', '11/10/2018', '3', 10, 2018, 'Ciudad Mendoza', 'Camerino Z. Mendoza', 'Veracruz', '300300001', '18.8040896', '-97.1811656', 2, 0, 0, 0, 0, 2, 0, 0, 1, 0, 0, 0, 4),
(158, 'Videoteca Bengala', '1', 'S/D', 'S/D', 'Ayuntamiento de Camerino Z Mendoza', 'Teatro biling?e (n?huatl-espa?ol) Alt iwan s?wameh Agua y mujeres', '13/11/2018', '1', 11, 2018, 'Ciudad Mendoza', 'Camerino Z. Mendoza', 'Veracruz', '300300001', '18.8040896', '-97.1811656', 6, 1, 0, 0, 0, 6, 1, 0, 1, 0, 0, 0, 4),
(159, 'Videoteca Bengala', '1', 'S/D', 'Museo Comunitario de Ciudad Mendoza, Teatro de Tres', 'S/D', 'Pedro y el Capit?n. Ensayo con p?blico', '01/12/2018', '1', 12, 2018, 'Ciudad Mendoza', 'Camerino Z. Mendoza', 'Veracruz', '300300001', '18.8040896', '-97.1811656', 2, 0, 0, 0, 0, 6, 1, 0, 1, 0, 0, 0, 4),
(160, 'Orlando Bautista Villegas', '3', 'S/D', 'S/D', 'Ayuntamiento de Camerino Z Mendoza', 'Encuentro Regional de Teatro', '23/03/2018', '1', 3, 2018, 'Ciudad Mendoza', 'Camerino Z. Mendoza', 'Veracruz', '300300001', '18.8040896', '-97.1811656', 6, 0, 0, 0, 0, 6, 0, 0, 1, 0, 0, 0, 4),
(161, 'Zona de Creaci?n', '1', 'S/D', 'Ambulante A.C., ATENTO', 'Eficine Distribuci?n y Producci?n 189, Secretar?a de Cultura', 'Justicia en tu comunidad', '15/10/2018', '1', 10, 2018, 'Ixhuatlancillo', 'Ixhuatlancillo', 'Veracruz', '300810001', '18.894444', '-97.149444', 6, 0, 0, 0, 0, 2, 1, 1, 0, 0, 0, 0, 4),
(162, 'Zona de Creaci?n', '1', 'S/D', 'Ixhuatlancillo: mi comunidad, mi familia y yo', 'S/D', 'En busca del h?bitat. Tercer periodo-bimensual- (talleres de audiovisuales, m?scaras, pintura, limpieza y construcci?n del espacio de cultural)', '03/09/2018', '20', 9, 2018, 'Ixhuatlancillo', 'Ixhuatlancillo', 'Veracruz', '300810001', '18.894444', '-97.149444', 1, 1, 0, 0, 2, 1, 0, 1, 0, 0, 0, 0, 7),
(163, 'Zona de Creaci?n', '1', 'S/D', 'LoboAlpino monta?ismo, Cuahut?moc Wetzca', 'S/D', 'En busca del h?bitat. Segundo periodo -bimensual (talleres de fotograf?a, cartoner?a, recorrido plantas medicinales, tejidos y bordados, salud)', '03/07/2018', '20', 7, 2018, 'Rancho del Cristo', 'Ixhuatlancillo', 'Veracruz', '300810005', '18.873333', '-97.131667', 1, 1, 0, 0, 2, 1, 0, 1, 0, 0, 0, 0, 7),
(164, 'Zona de Creaci?n', '1', 'S/D', 'Calambre de Yeso,  Museo de Arte del Estado en Orizaba, ArteNativas', 'Museo de Arte del Estado en Orizaba', 'En busca del h?bitat. Primer periodo- trimestral (talleres de fotograf?a, teatro, sellos, serigraf?a, visita al Museo de Arte del Estado en Orizaba)', '03/03/2018', '30', 5, 2018, 'Rancho del Cristo', 'Ixhuatlancillo', 'Veracruz', '300810005', '18.873333', '-97.131667', 1, 1, 0, 0, 2, 1, 0, 1, 0, 0, 0, 0, 7),
(165, 'La Nun.k Muerta Rebeli?n', '3', 'S/D', 'Rock in Plaza R?o ', 'S/D', 'Rock in Plaza R?o Orizaba 5', '10/07/2018', '2', 7, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 2, 0, 0, 0, 0, 5, 1, 0, 1, 0, 0, 0, 4);
INSERT INTO `boletin1` (`id`, `NOM`, `TIPO_ORG`, `COLABS_RM`, `COLAB_EXT`, `COLAB_GOB`, `NOM_EVENT`, `FECHA`, `NUM_DIAS`, `MES`, `ANIO`, `LOC`, `MPIO`, `EDO`, `CVE_GEO`, `LATITUD`, `LONGITUD`, `PUBLIC_OBJ`, `PAT_INT`, `PAT_NAT`, `PAT_TAN`, `ED`, `AR`, `OBJ_EVNT1`, `OBJ_EVNT2`, `OBJ_EVNT3`, `OBJ_EVNT4`, `OBJ_EVNT5`, `OBJ_EVNT6`, `PRINS_ACT`) VALUES
(166, 'Colegio Constructivista Descubridores', '2', 'S/D', 'Casa del Kamishibai M?xico', 'S/D', 'Cuenta Cuentos en Teatro Kamishibai', '29/04/2018', '1', 4, 2018, 'Monterrey', 'Monterrey', 'Nuevo Le?n', '190390001', '25.6802019', '-100.3152586', 1, 0, 0, 0, 0, 6, 1, 0, 1, 0, 0, 0, 4),
(167, 'Colegio Constructivista Descubridores', '2', 'S/D', 'Centro Cultural Loyola de Monterrey', 'S/D', 'Conferencia "Superh?roes de verdad o ficci?n"', '14/06/2018', '1', 6, 2018, 'Monterrey', 'Monterrey', 'Nuevo Le?n', '190390001', '25.6802019', '-100.3152586', 7, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 4),
(168, 'Colegio Constructivista Descubridores', '2', 'S/D', 'S/D', 'S/D', 'Taller Introductorio Entrenamiento en Competencias Sociales', '13/04/2018', '1', 4, 2018, 'Monterrey', 'Monterrey', 'Nuevo Le?n', '190390001', '25.6802019', '-100.3152586', 7, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1),
(169, 'ACCESA Cultura y espect?culos', '2', 'Nikan Tipowih', 'Red Cultural de las Altas Monta?as, PHILOSOPHICA, Taller la Benjamina, Postres Verde Pastel', 'S/D', 'La Catrina Garbancera. Festival Cultural Orizaba 2018', '03/11/2018', '1', 11, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 3, 0, 0, 0, 6, 1, 1, 1, 0, 0, 0, 2),
(170, 'Adri?n S?nchez Oropeza', '3', 'S/D', 'S/D', 'S/D', 'Taller de dibujo y pintura', '04/10/2018', 'S/D', 10, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 2, 0, 0, 0, 2, 1, 0, 1, 0, 0, 0, 0, 1),
(171, 'Adri?n S?nchez Oropeza', '3', 'Octavio S?nchez Oropeza', 'S/D', 'Escuela Enrique Laucher, Escuela Morelos, Ayuntamiento Orizaba', 'elaboraci?n de mural', '06/10/2018', 'S/D', 10, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 6),
(172, 'Adri?n S?nchez Oropeza', '3', 'S/D', 'S/D', 'Instituto Veracruzano de la Cultura, Museo de Arte del Estado de Veracruz', 'Exposici?n y venta de obra', '28/09/2018', '2', 9, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 7, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 5),
(173, 'Adri?n S?nchez Oropeza', '3', 'S/D', 'S/D', 'Museo de Arte del Estado de Veracruz', 'Curso Artes aplicadas a decoraci?n', '28/09/2018', 'S/D', 9, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 2, 0, 0, 0, 2, 1, 0, 1, 0, 0, 0, 0, 1),
(174, 'Adri?n S?nchez Oropeza', '3', 'S/D', 'S/D', 'S/D', 'Taller de dibujo y pintura', '04/09/2018', 'S/D', 9, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 0, 0, 0, 2, 1, 0, 1, 0, 0, 0, 0, 1),
(175, 'Adri?n S?nchez Oropeza', '3', 'S/D', 'S/D', 'S/D', 'Taller de dibujo y pintura', '04/08/2018', 'S/D', 8, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 0, 0, 0, 2, 1, 0, 1, 0, 0, 0, 0, 1),
(176, 'Adri?n S?nchez Oropeza', '3', 'S/D', 'Facultad de Artes Pl?sticas de la Universidad Veracruzana', 'Ayuntamiento de C?rdoba, Centro Cultural C?rdoba', 'Inauguraci?n Exposici?n "Rostros UV"', '12/07/2018', '14', 7, 2018, 'C?rdoba', 'C?rdoba', 'Veracruz', '300440001', '18.9239713', '-96.9278109', 6, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 4),
(177, 'Adri?n S?nchez Oropeza', '3', 'S/D', 'Rinc?n de Irma Arte Caf?', 'Teatro Llave, Ayuntamiento de Orizaba, Rinc?n de Irma Arte Caf?', 'Exposici?n "Ver, interpretar, crear". Muestra anual taller de dibujo y pintura', '14/08/2018', 'S/D', 8, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 4),
(178, 'Adri?n S?nchez Oropeza', '3', 'S/D', 'Facultad de Artes Pl?sticas de la Universidad Veracruzana', 'Ayuntamiento de Orizaba, Poliforum Orizaba', 'Inauguraci?n Exposici?n "Rostros UV"', '17/08/2018', '14', 8, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 4),
(179, 'Adri?n S?nchez Oropeza', '3', 'S/D', 'S/D', 'S/D', 'Taller de dibujo y pintura. Verano', '09/07/2018', '25', 7, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 1, 0, 0, 0, 2, 1, 0, 1, 0, 0, 0, 0, 1),
(180, 'Adri?n S?nchez Oropeza', '3', 'S/D', 'Rinc?n de Irma Arte Caf?', 'Teatro Llave, Ayuntamiento de Orizaba', 'Exposici?n "Poes?a Er?tica"', '09/05/2018', '11', 5, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 4),
(181, 'Adri?n S?nchez Oropeza', '3', 'S/D', 'S/D', 'S/D', 'Taller intensivo de dibujo y pintura. Vacaciones.', '27/03/2018', '6', 3, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 1, 0, 0, 0, 2, 1, 0, 1, 0, 0, 0, 0, 1),
(182, 'Centro Cultural Autogestivo Conejo Clandestino ', '2', 'S/D', 'S/D', 'S/D', 'IV Encuentro. Bordamos la memoria. Por las v?ctimas de feminicidio en Veracruz', '05/10/2018', '1', 10, 2018, 'Xalapa-Enr?quez', 'Xalapa', 'Veracruz', '300870001', '19.5408338', '-96.9146374', 6, 0, 0, 0, 0, 7, 1, 0, 0, 0, 0, 1, 7),
(183, 'Centro Cultural Autogestivo Conejo Clandestino ', '2', 'S/D', 'S/D', 'S/D', '5ta Jornada. Bordamos la memoria. Por las v?ctimas de feminicidio en Veracruz', '20/10/2018', '1', 10, 2018, 'Xalapa-Enr?quez', 'Xalapa', 'Veracruz', '300870001', '19.5408338', '-96.9146374', 6, 0, 0, 0, 0, 7, 1, 0, 0, 0, 0, 1, 7),
(184, 'Centro Cultural Autogestivo Conejo Clandestino ', '2', 'S/D', 'S/D', 'S/D', 'Sexta Jornada. Bordamos la memoria. Por las v?ctimas de feminicidio en Veracruz', '01/11/20018', '1', 11, 2018, 'Naolinco de Victoria', 'Naolinco', 'Veracruz', '301120001', '19.6546437', '-96.8731884', 6, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 1, 7),
(185, 'Orquesta Filarmonica de las Altas Monta?as (OFAM)', '4', 'S/D', 'S/D', 'Ayuntamiento Orizaba', 'Temporada Invierno 2018', '15/12/2018', '6', 12, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 0, 0, 0, 0, 5, 0, 0, 1, 0, 0, 0, 4),
(186, 'Orquesta Filarmonica de las Altas Monta?as (OFAM)', '4', 'S/D', 'S/D', 'Ayuntamiento Orizaba', 'Temporada Verano 2018', '09/08/2018', '4', 8, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 0, 0, 0, 0, 5, 0, 0, 1, 0, 0, 0, 4),
(187, 'Orquesta Filarmonica de las Altas Monta?as (OFAM)', '4', 'S/D', 'Festival Internacional Cervantino', 'Ayuntamiento de Orizaba', 'Concierto Festival Internacional Cervantino', '16/10/2018', '1', 10, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 0, 0, 0, 0, 5, 0, 0, 1, 0, 0, 0, 4),
(188, 'Orquesta Filarmonica de las Altas Monta?as (OFAM)', '4', 'S/D', 'S/D', 'DIF Ixtaczoquitl?n', 'Gala Mexicana OFAM', '14/09/2018', '1', 9, 2018, 'Ixtaczoquitl?n', 'Ixtaczoquitl?n', 'Veracruz', '300850001', '18.8513952', '-97.0624498', 6, 0, 0, 0, 0, 5, 0, 0, 1, 0, 0, 0, 4),
(189, 'Orquesta Filarmonica de las Altas Monta?as (OFAM)', '4', 'S/D', 'S/D', 'Ayuntamiento de Orizaba ', 'Concierto Fiestas Patrias', '16/09/2018', '1', 9, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 0, 0, 0, 0, 5, 0, 0, 1, 0, 0, 0, 4),
(190, 'Orquesta Filarmonica de las Altas Monta?as (OFAM)', '4', 'S/D', 'S/D', 'Ayuntamiento de Orizaba, DIF Orizaba, Loter?a Nacional, Teatro I. de la Llave', 'Sorteo en vivo de la Loter?a Nacional', '31/08/2018', '1', 8, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 0, 0, 0, 0, 5, 0, 0, 1, 0, 0, 0, 4),
(191, 'Orquesta Filarmonica de las Altas Monta?as (OFAM)', '4', 'S/D', 'Feria Iberoamericana del Libro Orizaba', 'Ayuntamiento de Orizaba,Teatro I. de la Llave, Secretar?a de Cultura San Luis Potos? ', 'Concierto para Viol?n y Contrabajo', '20/05/2018', '1', 5, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 0, 0, 0, 0, 5, 0, 0, 1, 0, 0, 0, 4),
(192, 'Orquesta Filarmonica de las Altas Monta?as (OFAM)', '4', 'S/D', 'S/D', 'Ayuntamiento Orizaba', 'Temporada Primaver 2018', '15/03/2018', '4', 3, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 0, 0, 0, 0, 5, 0, 0, 1, 0, 0, 0, 4),
(193, 'Arte en Ra?z', '2', 'Kaff? Real', 'S/D', 'S/D', 'Charla/Taller sobre la autogesti?n y el desplazamiento cultural', '17/10/2018', '1', 10, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 7, 0, 0, 0, 2, 0, 1, 1, 0, 0, 0, 0, 1),
(194, 'Arte en Ra?z', '2', 'S/D', 'Centro Cultural Casa Baltazar', 'S/D', 'Poes?a al ausente', '02/11/2018', '1', 11, 2018, 'C?rdoba', 'C?rdoba', 'Veracruz', '300440001', '18.9239713', '-96.9278109', 6, 0, 0, 0, 0, 4, 0, 0, 1, 0, 0, 0, 7),
(195, 'Luis Alfonso Gonz?lez Valdivia', '3', 'S/D', 'S/D', 'Casa de Cultura de Ciudad Mendoza', 'M?sicos, Po3tas y & Locos', '28/04/2018', '1', 4, 2018, 'Ciudad Mendoza', 'Camerino Z. Mendoza', 'Veracruz', '300300001', '18.8040896', '-97.1811656', 6, 0, 0, 0, 0, 4, 1, 0, 0, 0, 0, 0, 4),
(196, 'Luis Alfonso Gonz?lez Valdivia', '3', 'S/D', 'S/D', 'Casa de Cultura de Ciudad Mendoza', 'M?sicos, Po3tas y & Locos', '27/10/2018', '1', 10, 2018, 'Ciudad Mendoza', 'Camerino Z. Mendoza', 'Veracruz', '300300001', '18.8040896', '-97.1811656', 6, 0, 0, 0, 0, 4, 1, 0, 0, 0, 0, 0, 4),
(197, 'Luis Alfonso Gonz?lez Valdivia', '3', 'S/D', 'S/D', 'Casa de Cultura de Ciudad Mendoza', 'M?sicos, Po3tas y & Locos', '31/03/2018', '1', 3, 2018, 'Ciudad Mendoza', 'Camerino Z. Mendoza', 'Veracruz', '300300001', '18.8040896', '-97.1811656', 6, 0, 0, 0, 0, 4, 1, 0, 0, 0, 0, 0, 4),
(198, 'Ciartes y S? A.C.', '4', 'S/D', 'La Casa Encantada ', 'S/D', 'Taller de Escritura Creativa', '17/10/2018', 'S/D', 10, 2018, 'Pachuca de Soto', 'Pachuca de Soto', 'Hidalgo', '130480001', '20.1165413', '-98.7413535', 6, 0, 0, 0, 0, 4, 0, 1, 0, 0, 0, 0, 1),
(199, 'Ciartes y S? A.C.', '4', 'S/D', 'La Casa Encantada ', 'S/D', 'Bismarck Antonio Calder?n. Exposici?n Individual El Halago del Inconsciente', '19/10/2018', '1', 10, 2018, 'Pachuca de Soto', 'Pachuca de Soto', 'Hidalgo', '130480001', '20.1165413', '-98.7413535', 6, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 4),
(200, 'Ciartes y S? A.C.', '4', 'S/D', 'La Casa Encantada ', 'S/D', 'Taller de desarrollo y sensibilizaci?n a trav?s de la escultura', '16/10/2018', 'S/D', 10, 2018, 'Pachuca de Soto', 'Pachuca de Soto', 'Hidalgo', '130480001', '20.1165413', '-98.7413535', 6, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1),
(201, 'Ciartes y S? A.C.', '4', 'S/D', 'S/D', 'Secretar?a de Cultura Federal y el Municipio de Pachuca a trav?s del Instituto Municipal para la Cultura de Pachuca', 'Tercera Fiesta de Mujeres y Arte', '17/11/2018', '8', 11, 2018, 'Pachuca de Soto', 'Pachuca de Soto', 'Hidalgo', '130480001', '20.1165413', '-98.7413535', 6, 0, 0, 0, 0, 6, 1, 1, 0, 0, 0, 0, 2),
(202, 'Ciartes y S? A.C.', '4', 'S/D', 'La Casa Encantada, Academia de las Artes y las Ciencias Cinematogr?ficas de Hidalgo', 'S/D', 'Semillas para so?ar. Proyecci?n de documentales', '20/11/2018', '3', 11, 2018, 'Pachuca de Soto', 'Pachuca de Soto', 'Hidalgo', '130480001', '20.1165413', '-98.7413535', 6, 0, 0, 0, 0, 2, 1, 0, 1, 0, 0, 0, 4),
(203, 'MIKISTLI, banda de metal en lengua n?huatl. ', '3', 'S/D', 'S/D', 'Casa de la Cultura de Zongolica, Ayuntamiento de Zongolica', 'Penumbra 2018. Festival de Metal Extremos para vivos y muertos', '01/11/2018', '1', 11, 2018, 'Zongolica', 'Zongolica', 'Veracruz', '302010001', '18.6667231', '-96.9982102', 7, 1, 0, 0, 0, 5, 0, 0, 1, 0, 0, 0, 4),
(204, 'MIKISTLI, banda de metal en lengua n?huatl. ', '3', 'S/D', 'S/D', 'Secretar?a de Cultura a trav?s de Centro de las Artes Quer?taro', '17 Encuentro de las Culturas Populares y Pueblos Ind?genas en Quer?taro', '17/11/2018', '1', 11, 2018, 'Santiago de Quer?taro', 'Quer?taro', 'Quer?taro', '220140001', '20.5878372', '-100.3879904', 6, 1, 0, 0, 0, 5, 1, 0, 1, 0, 0, 0, 4),
(205, 'MIKISTLI, banda de metal en lengua n?huatl. ', '3', 'S/D', 'S/D', 'Comisi?n Nacional para el Desarrollo de los Pueblos Ind?genas', 'Festival de M?sica Contempor?nea Ind?gena', '26/11/2018', '1', 11, 2018, 'San Crist?bal de las Casas', 'San Crist?bal de las Casas', 'Chiapas', '70780001', '16.735377', '-92.63873', 6, 1, 0, 0, 0, 5, 1, 0, 1, 0, 0, 0, 4),
(206, 'MIKISTLI, banda de metal en lengua n?huatl. ', '3', 'S/D', 'Tlane Rock', 'S/D', 'Tlane Rock Fest', '17/02/2018', '1', 2, 2018, 'Tlanepantla', 'Tlanepantla', 'Puebla', '211820001', '18.8658333', '-97.8855556', 7, 1, 0, 0, 0, 5, 0, 0, 1, 0, 0, 0, 4),
(207, 'MIKISTLI, banda de metal en lengua n?huatl. ', '3', 'S/D', 'S/D', 'Direcci?n de Desarrollo Cultural del Ayuntamiento de Zongolica', 'Feria de Zongolica. Encuentro de Bandas de Rock', '02/05/2018', '1', 5, 2018, 'Zongolica', 'Zongolica', 'Veracruz', '302010001', '18.6667231', '-96.9982102', 7, 1, 0, 0, 0, 5, 0, 0, 1, 0, 0, 0, 4),
(208, 'MIKISTLI, banda de metal en lengua n?huatl. ', '3', 'S/D', 'S/D', 'Comisi?n Nacional para el Desarrollo de los Pueblos Ind?genas', 'Nuestras Ra?ces, Nuestra Riqueza', '26/08/2018', '1', 8, 2018, 'Zongolica', 'Zongolica', 'Veracruz', '302010001', '18.6667231', '-96.9982102', 7, 1, 0, 0, 0, 5, 0, 0, 1, 0, 0, 0, 4),
(209, 'Taller de arte y dise?o. Orizaba', '3', 'S/D', 'S/D', 'S/D', '8 Talleres (dise?o digital, ilustraci?n y narrativa, redes sociales para pap?s, video y animaci?n, nutrici?n y ejercicio familiar, cupcakes)', '01/07/2018', '13', 7, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 2, 0, 0, 0, 2, 7, 0, 1, 0, 1, 0, 0, 1),
(210, 'Hotel Imperial. Centro Cultural Comunitario', '3', 'S/D', 'S/D', 'S/D', 'Recorrido Literario', '04/02/2018', '1', 2, 2018, 'C?rdoba', 'C?rdoba', 'Veracruz', '300440001', '18.9239713', '-96.9278109', 6, 0, 0, 0, 0, 4, 1, 0, 1, 0, 0, 0, 7),
(211, 'Hotel Imperial. Centro Cultural Comunitario', '3', 'S/D', 'S/D', 'S/D', 'Convocatoria a realizar Exposici?n fotogr?fica sobre historia Colonia Las Estaciones', '07/03/2018', 'S/D', 3, 2018, 'C?rdoba', 'C?rdoba', 'Veracruz', '300440001', '18.9239713', '-96.9278109', 6, 6, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 7),
(212, 'Taller de arte y dise?o. Orizaba', '3', 'S/D', 'S/D', 'S/D', 'Cursos Sabatinos en Orizaba. Animaci?n, Marketing, Dise?o Arquitect?nico y de Producto', '06/05/2018', '24', 5, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 2, 0, 0, 0, 2, 7, 0, 1, 0, 1, 0, 0, 1),
(213, 'Taller de arte y dise?o. Orizaba', '3', 'S/D', 'S/D', 'S/D', 'Cursos de ilustraci?n digital y dise?o editorial', '07/09/2018', 'S/D', 9, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 2, 0, 0, 0, 2, 7, 0, 1, 0, 1, 0, 0, 1),
(214, 'Taller de arte y dise?o. Orizaba', '3', 'S/D', 'S/D', 'S/D', 'Cursos de ilustraci?n digital, edici?n de video y dise?o gr?fico', '12/02/2018', 'S/D', 2, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 2, 0, 0, 0, 2, 7, 0, 1, 0, 1, 0, 0, 1),
(215, 'Librer?a Biul?', '2', 'S/D', 'S/D', 'Programa Nacional Salas de Lectura, Instituto Veracruzano de la Cultura (Subdirecci?n de Educaci?n e Investigaci?n Art?stica y oficina de Fomento a la lectura), Secretar?a de Cultura (Direcci?n Genera', 'Asesor?a para la elaboraci?n de planes de lectura y talleres de t?cnicas de animaci?n a la lectura', '29/12/2018', 'S/D', 12, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 7, 0, 0, 0, 2, 4, 0, 1, 0, 0, 0, 0, 5),
(216, 'Librer?a Biul?', '2', 'S/D', 'Feria Iberoamericana del Libro Orizaba ', 'Ayuntamiento de Orizaba, Secretar?a de Cultura de San Luis Potos? ', 'Estand en la Feria Iberoamericana del Libro Orizaba', '12/05/2018', 'S/D', 5, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 0, 0, 0, 2, 4, 1, 0, 0, 0, 0, 0, 4),
(217, 'Librer?a Biul?', '2', 'S/D', 'Librer?a del Ermita?o', 'S/D', 'Premio Nacional de Librer?a', '16/10/2018', '1', 10, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 7, 0, 0, 0, 0, 4, 1, 0, 0, 0, 0, 1, 5),
(218, 'Librer?a Biul?', '2', 'S/D', 'S/D', 'S/D', 'Escribir Es-jugar. Taller de escritura creativa para ni?os', '19/05/2018', '8', 5, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 1, 0, 0, 0, 2, 4, 0, 1, 0, 0, 0, 0, 1),
(219, 'Librer?a Biul?', '2', 'S/D', 'S/D', 'Programa Nacional de Salas de Lectura', 'Club de Lectura para ni?os', '11/12/2018', 'S/D', 12, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 1, 0, 0, 0, 2, 4, 0, 1, 0, 0, 0, 0, 1),
(220, 'Librer?a Biul?', '2', 'S/D', 'S/D', 'S/D', 'Taller de dibujo', '22/11/2018', 'S/D', 11, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 1, 0, 0, 0, 2, 1, 0, 1, 0, 0, 0, 0, 1),
(221, 'Librer?a Biul?', '2', 'S/D', 'S/D', 'Programa Nacional de Salas de Lectura', 'Club de Lectura para ni?os', '04/09/2018', 'S/D', 9, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 1, 0, 0, 0, 2, 4, 0, 1, 0, 0, 0, 0, 1),
(222, 'Librer?a Biul?', '2', 'S/D', 'S/D', 'Programa Nacional de Salas de Lectura', 'Talleres de Lectura', '22/01/2018', '6', 1, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 1, 0, 0, 0, 2, 4, 0, 1, 0, 0, 0, 0, 1),
(223, 'Empoderando Mujeres AC', '1', 'S/D', 'S/D', 'S/D', 'Mil voces contra el ACOSO', '05/05/2018', '1', 5, 2018, 'C?rdoba', 'C?rdoba', 'Veracruz', '300440001', '18.9239713', '-96.9278109', 3, 0, 0, 0, 2, 0, 1, 1, 0, 0, 0, 0, 7),
(224, 'Empoderando Mujeres AC', '1', 'S/D', 'S/D', 'S/D', 'C?rculo De Autoayuda: LO MEJOR DE NOSOTRAS', '11/08/2018', 'S/D', 8, 2018, 'C?rdoba', 'C?rdoba', 'Veracruz', '300440001', '18.9239713', '-96.9278109', 3, 0, 0, 0, 2, 0, 1, 1, 0, 0, 0, 0, 7),
(225, 'Empoderando Mujeres AC', '1', 'S/D', 'S/D', 'S/D', 'Cerrando ciclos', '28/12/2018', '1', 12, 2018, 'C?rdoba', 'C?rdoba', 'Veracruz', '300440001', '18.9239713', '-96.9278109', 3, 0, 0, 0, 2, 0, 1, 1, 0, 0, 0, 0, 7),
(226, 'Empoderando Mujeres AC', '1', 'S/D', 'S/D', 'S/D', 'Taller De Autodefensa Personal Para Mujeres', '17/08/2018', 'S/D', 8, 2018, 'C?rdoba', 'C?rdoba', 'Veracruz', '300440001', '18.9239713', '-96.9278109', 3, 0, 0, 0, 2, 0, 1, 1, 0, 0, 0, 0, 1),
(227, 'Empoderando Mujeres AC', '1', 'S/D', 'S/D', 'S/D', 'Conferencia: "Relaciones Destructivas"', '25/11/2018', '1', 11, 2018, 'C?rdoba', 'C?rdoba', 'Veracruz', '300440001', '18.9239713', '-96.9278109', 3, 0, 0, 0, 2, 0, 1, 1, 0, 0, 0, 0, 4),
(228, 'Empoderando Mujeres AC', '1', 'S/D', 'S/D', 'Comisi?n Estatal de Derechos Humanos Veracruz', 'Curso Derechos Humanos en M?xico', '07/11/2018', 'S/D', 11, 2018, 'C?rdoba', 'C?rdoba', 'Veracruz', '300440001', '18.9239713', '-96.9278109', 7, 0, 0, 0, 2, 0, 0, 1, 0, 0, 0, 0, 1),
(229, 'Empoderando Mujeres AC', '1', 'S/D', 'Ambulante A.C., ATENTO', 'Eficine Distribuci?n y Producci?n 189, Secretar?a de Cultura', 'Justicia en tu comunidad', '18/10/2018', '1', 10, 2018, 'C?rdoba', 'C?rdoba', 'Veracruz', '300440001', '18.9239713', '-96.9278109', 6, 0, 0, 0, 2, 2, 1, 0, 1, 0, 0, 0, 4),
(230, 'Audio Sahc Records', '4', 'S/D', 'Festival Internacional de Jazz en C?rdoba, Centro Cultural Casa Baltazar, CD Baby en Espa?ol', 'Ayuntamiento de C?rdoba', 'Distribuci?n Digital y estrategias para m?sicos independientes', '28/07/2018', '1', 7, 2018, 'C?rdoba', 'C?rdoba', 'Veracruz', '300440001', '18.9239713', '-96.9278109', 7, 0, 0, 0, 0, 7, 0, 1, 0, 1, 0, 0, 5),
(231, 'Audio Sahc Records', '4', 'S/D', 'S/D', 'S/D', 'Sesi?n de grabaci?n con Academia Musical Johann', '28/11/2018', 'S/D', 11, 2018, 'C?rdoba', 'C?rdoba', 'Veracruz', '300440001', '18.9239713', '-96.9278109', 7, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 1, 5),
(232, 'Audio Sahc Records', '4', 'S/D', 'Casa Baltazar, 12 Encuentro de Son Jarocho, Son huasteco y Huapango', 'Ayuntamiento de C?rdoba', 'Conferencia "Pol?tica cultural, econom?a creativa y Marketing Cultural"', '13/10/2018', '1', 10, 2018, 'C?rdoba', 'C?rdoba', 'Veracruz', '300440001', '18.9239713', '-96.9278109', 7, 0, 0, 0, 0, 5, 1, 0, 0, 0, 0, 0, 5),
(233, 'Audio Sahc Records', '4', 'S/D', 'S/D', 'S/D', 'Sesi?n de grabaci?n del disco del 12vo Encuentro de Son en C?rdoba', '11/10/2018', 'S/D', 10, 2018, 'C?rdoba', 'C?rdoba', 'Veracruz', '300440001', '18.9239713', '-96.9278109', 7, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 1, 5),
(234, 'Audio Sahc Records', '4', 'S/D', 'S/D', 'S/D', 'Sesi?n de grabaci?n Sara Rub?', '29/09/2018', 'S/D', 9, 2018, 'C?rdoba', 'C?rdoba', 'Veracruz', '300440001', '18.9239713', '-96.9278109', 7, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 1, 5),
(235, 'Audio Sahc Records', '4', 'S/D', 'CDBaby M?xico', 'S/D', 'Entrevista a Mario S?nchez sobre nuevas herramientas de marketing digital para m?sicos independientes', '03/08/2018', '1', 8, 2018, 'C?rdoba', 'C?rdoba', 'Veracruz', '300440001', '18.9239713', '-96.9278109', 7, 0, 0, 0, 0, 5, 0, 1, 0, 1, 0, 0, 5),
(236, 'Audio Sahc Records', '4', 'S/D', 'S/D', 'S/D', 'Sesi?n de grabaci?n Hang Drum con Ferm?n Cortina', '20/07/2018', 'S/D', 7, 2018, 'C?rdoba', 'C?rdoba', 'Veracruz', '300440001', '18.9239713', '-96.9278109', 7, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 1, 5),
(237, 'Audio Sahc Records', '4', 'S/D', 'S/D', 'S/D', 'Charla/Taller sobre grabaci?n y producci?n con Academia de M?sica "Mangor?"', '26/06/2018', '1', 6, 2018, 'C?rdoba', 'C?rdoba', 'Veracruz', '300440001', '18.9239713', '-96.9278109', 7, 0, 0, 0, 1, 5, 0, 1, 0, 0, 0, 1, 1),
(238, 'Audio Sahc Records', '4', 'S/D', 'Ser Arte TV', 'S/D', '2do Aniversario Programa Ser Arte TV', '22/06/2018', '1', 6, 2018, 'C?rdoba', 'C?rdoba', 'Veracruz', '300440001', '18.9239713', '-96.9278109', 6, 3, 0, 0, 0, 7, 1, 1, 0, 0, 0, 0, 4),
(239, 'Silvia Mart?nez Saavedra', '1', 'Luis Alfonso Gonz?lez Valdivia', 'Colectivo Cultural Xinachtli', 'S/D', 'Tendedero Po?tico', '25/11/2018', '4', 11, 2018, 'Coscomatepec de Bravo', 'Coscomatepec', 'Veracruz', '300470001', '19.071667', '-97.046111', 6, 0, 0, 0, 0, 4, 1, 0, 0, 0, 0, 0, 4),
(240, 'Greco Montano', '3', 'S/D', 'Pobladores de Rafael Delgado', 'S/D', 'Mural de Arte Urbano', '27/09/2018', 'S/D', 9, 2018, 'Rafael Delgado', 'Rafael Delgado', 'Veracruz', '301350001', '18.8092313', '-97.0715457', 6, 6, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 7),
(241, 'Zontradici?n del ITSZ', '4', 'S/D', 'Orquesta Juvenil de R?o Blanco', 'Ayuntamiento de Nogales', 'Festival art?stico y cultural Laguna Navide?a 2018 Estado de Veracruz "Huateque y Jaqueton"', '20/12/2018', '1', 12, 2018, 'Nogales', 'Nogales', 'Veracruz', '301150001', '18.8222858', '-97.1647214', 6, 3, 0, 0, 0, 3, 1, 0, 1, 0, 0, 0, 4),
(242, 'Zontradici?n del ITSZ', '4', 'S/D', 'Ballet Folkl?rico Tequilatlakpak, Ballet Folkl?rico Yoloxochilt, Son Huasteco Navegantes del Son, 1er Productores de caf? artesanal', 'S/D', '1er Encuentro de productores de caf? artesanal', '16/12/2018', '1', 12, 2018, 'Tequila', 'Tequila', 'Veracruz', '301680001', '18.729444', '-97.070278', 6, 3, 0, 0, 0, 3, 1, 0, 1, 0, 0, 0, 4),
(243, 'Zontradici?n del ITSZ', '4', 'S/D', 'Festival Internacional Cervantino', 'Ayuntamiento de Orizaba, Teatro Ignacio de la Llave', 'Festival Internacional Cervantino Orizaba', '02/12/2018', '5', 12, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 3, 0, 0, 0, 3, 1, 0, 0, 0, 0, 0, 4),
(244, 'Zontradici?n del ITSZ', '4', 'S/D', 'S/D', 'S/D', 'Zondance del ITSZ', '26/11/2018', '1', 11, 2018, 'Tequila', 'Tequila', 'Veracruz', '301680001', '18.729444', '-97.070278', 6, 3, 0, 0, 0, 3, 1, 0, 1, 0, 0, 0, 4),
(245, 'Zontradici?n del ITSZ', '4', 'S/D', 'S/D', 'Ayuntamiento de Orizaba, Pante?n Municipal', 'Tercera Noche de Muertos', '02/11/2018', '1', 11, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 3, 0, 0, 0, 3, 1, 0, 1, 0, 0, 0, 4),
(246, 'Zontradici?n del ITSZ', '4', 'S/D', 'S/D', 'Ayuntamiento de Tequila', 'Festival Tlasohkamati', '27/10/2018', '1', 10, 2018, 'Tequila', 'Tequila', 'Veracruz', '301680001', '18.729444', '-97.070278', 6, 3, 0, 0, 0, 3, 1, 0, 1, 0, 0, 0, 4),
(247, 'Zontradici?n del ITSZ', '4', 'S/D', 'S/D', 'Ayuntamiento de Orizaba, Teatro Ignacio de la Llave', 'Danzoneros en el Llave', '14/10/2018', '1', 10, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 3, 0, 0, 0, 3, 1, 0, 1, 0, 0, 0, 4),
(248, 'Zontradici?n del ITSZ', '4', 'S/D', 'S/D', 'Ayuntamiento de Huiloapan de Cuauht?moc', 'Fiestas Patrias 2018', '12/09/2018', '1', 9, 2018, 'Huiloapan de Cuauht?moc', 'Huiloapan de Cuauht?moc', 'Veracruz', '300740001', '18.8166304', '-97.1522807', 6, 3, 0, 0, 0, 3, 1, 0, 1, 0, 0, 0, 4),
(249, 'Zontradici?n del ITSZ', '4', 'S/D', 'S/D', 'Ayuntamiento de R?o Blanco', '15 de septiembre. Semana Cultural', '15/09/2018', '1', 9, 2018, 'R?o Blanco', 'R?o Blanco', 'Veracruz', '301380001', '18.8369927', '-97.1216906', 6, 3, 0, 0, 0, 3, 1, 0, 1, 0, 0, 0, 4),
(250, 'Zontradici?n del ITSZ', '4', 'S/D', 'Orquesta Juvenil de R?o Blanco', 'Ayuntamiento de Orizaba, Teatro Ignacio de la Llave', '2? Aniversario Compa??a de Danza Folkl?rica Zontradici?n', '26/08/2018', '1', 8, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 3, 0, 0, 0, 3, 1, 0, 1, 0, 0, 0, 4),
(251, 'Zontradici?n del ITSZ', '4', 'S/D', 'S/D', 'Ayuntamiento de Orizaba, Teatro Ignacio de la Llave', '5o Festival Internacional de Folclore Orizaba', '26/07/2018', '1', 7, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 3, 0, 0, 0, 3, 1, 0, 1, 0, 0, 0, 4),
(252, 'Zontradici?n del ITSZ', '4', 'S/D', 'Expori', 'Ayuntamiento de C?rdoba', 'Feria de C?rdoba 400 a?os', '20/05/2018', '1', 5, 2018, 'C?rdoba', 'C?rdoba', 'Veracruz', '300440001', '18.9239713', '-96.9278109', 6, 3, 0, 0, 0, 3, 1, 0, 1, 0, 0, 0, 4),
(253, 'Zontradici?n del ITSZ', '4', 'S/D', 'S/D', 'Ayuntamiento de Orizaba, Feria Iberoamericana del Libro Orizaba', 'Feria Iberoamericana del Libro Orizaba', '13/05/2018', '1', 5, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 6, 3, 0, 0, 0, 3, 1, 0, 1, 0, 0, 0, 4),
(254, 'Miguel ?ngel Zamudio', '1', 'S/D', 'Varios artistas que colaboran en la multiplataforma de fondeo', 'S/D', 'Fondearte', '01/01/2018', '365', 1, 2018, 'Veracruz', 'Veracruz', 'Veracruz', '301930001', '19.1798858', '-96.135648', 7, 0, 0, 0, 0, 7, 0, 0, 0, 1, 0, 0, 5),
(255, 'Alejandro Escudero', '3', 'S/D', 'Caf? Teatro Tierra Luna, Colectivo Voces Xallahuac', 'S/D', 'Cantata Sotaventina. Obra coral polif?nica de Alejandro Escudero', '23/11/2018', '1', 11, 2018, 'Xalapa-Enr?quez', 'Xalapa', 'Veracruz', '300870001', '19.5408338', '-96.9146374', 6, 0, 0, 0, 0, 5, 0, 0, 1, 0, 0, 0, 4),
(256, 'Alejandro Escudero', '3', 'S/D', 'La Casa de Nadie Colectivo Voces Xallahuac', 'Ayuntamiento de Xalapa Centro Recreativo Xalape?o', 'Audiciones. Cantata Sotaventina. Obra coral polif?nica de Alejandro Escudero', '30/08/2018', '8', 8, 2018, 'Xalapa-Enr?quez', 'Xalapa', 'Veracruz', '300870001', '19.5408338', '-96.9146374', 6, 0, 0, 0, 0, 5, 0, 0, 1, 0, 0, 0, 4),
(257, 'Alejandro Escudero', '3', 'S/D', 'Voces Xallahuac. Canto Coral Comunitario', 'S/D', 'Repertorio Trovante', '08/06/2018', '1', 6, 2018, 'Xalapa-Enr?quez', 'Xalapa', 'Veracruz', '300870001', '19.5408338', '-96.9146374', 6, 0, 0, 0, 0, 5, 1, 0, 1, 0, 0, 0, 4),
(258, 'Alejandro Escudero', '3', 'S/D', 'Voces Xallahuac. Canto Coral Comunitario', 'S/D', 'Invitaci?n a formar el Colectivo Voces Xallahuac. Canto Coral Comunitario.', '17/02/2018', 'S/D', 2, 2018, 'Xalapa-Enr?quez', 'Xalapa', 'Veracruz', '300870001', '19.5408338', '-96.9146374', 7, 0, 0, 0, 1, 4, 0, 1, 0, 0, 0, 0, 7),
(259, 'La elipse de Viento', '3', 'S/D', 'Cafenatlan Buen Caf? Buen vivir, Nitlakowateh mercadito comercio justo', 'S/D', 'Taller de escritura. ', '15/07/2018', '1', 7, 2018, 'Orizaba', 'Orizaba', 'Veracruz', '301180001', '18.849722', '-97.103611', 2, 0, 0, 0, 2, 4, 0, 1, 0, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `graficas`
--

CREATE TABLE `graficas` (
  `id` int(11) NOT NULL,
  `estado` varchar(50) DEFAULT NULL,
  `eRegistradas` int(50) DEFAULT NULL,
  `creador` int(50) DEFAULT NULL,
  `emprendedor` int(50) DEFAULT NULL,
  `gestor` int(50) DEFAULT NULL,
  `promotor` int(50) DEFAULT NULL,
  `objMujeres` int(50) DEFAULT NULL,
  `objJovenes` int(50) DEFAULT NULL,
  `objInfantes` int(50) DEFAULT NULL,
  `objMayores` int(50) DEFAULT NULL,
  `objGeneral` int(50) DEFAULT NULL,
  `objGestores` int(50) DEFAULT NULL,
  `objCalle` int(50) DEFAULT NULL,
  `objOtro` int(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `graficas`
--

INSERT INTO `graficas` (`id`, `estado`, `eRegistradas`, `creador`, `emprendedor`, `gestor`, `promotor`, `objMujeres`, `objJovenes`, `objInfantes`, `objMayores`, `objGeneral`, `objGestores`, `objCalle`, `objOtro`) VALUES
(1, 'Veracruz', 42, 19, 6, 14, 3, 13, 23, 17, 12, 14, 3, 4, 0),
(2, 'Queretaro', 2, 1, 1, NULL, NULL, NULL, 1, NULL, NULL, 1, NULL, NULL, NULL),
(3, 'Tlaxcala', 1, NULL, NULL, 1, NULL, 1, 1, 1, 1, NULL, NULL, NULL, NULL),
(4, 'Puebla', 1, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(5, 'Hidalgo', 2, NULL, 1, 1, NULL, 2, 2, 1, 1, NULL, NULL, 1, NULL),
(6, 'Nuevo Leon', 2, NULL, 2, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipios_veracruz`
--

CREATE TABLE `municipios_veracruz` (
  `id` int(11) NOT NULL,
  `estado` varchar(100) NOT NULL,
  `municipio` varchar(200) DEFAULT NULL,
  `miembros` int(11) DEFAULT NULL,
  `creador` int(11) NOT NULL,
  `emprendedor` int(11) NOT NULL,
  `gestor` int(11) NOT NULL,
  `promotor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `municipios_veracruz`
--

INSERT INTO `municipios_veracruz` (`id`, `estado`, `municipio`, `miembros`, `creador`, `emprendedor`, `gestor`, `promotor`) VALUES
(1, 'Veracruz', 'Rafael Delgado', 4, 2, 1, 0, 1),
(2, 'Veracruz', 'Cordoba', 3, 1, 1, 1, 0),
(3, 'Veracruz', 'Coscomatepec', 1, 0, 0, 1, 0),
(4, 'Veracruz', 'Mariano Escobedo', 1, 0, 0, 1, 0),
(5, 'Veracruz', 'Camerino Z. Mendoza', 4, 2, 0, 2, 0),
(6, 'Veracruz', 'Ixhuatlancillo', 1, 1, 0, 0, 0),
(7, 'Veracruz', 'Orizaba', 16, 6, 3, 1, 6),
(8, 'Veracruz', 'Xalapa', 4, 1, 0, 0, 0),
(9, 'Veracruz', 'Veracruz', 1, 0, 0, 1, 0),
(10, 'Veracruz', 'Zongolica', 4, 1, 1, 0, 1),
(11, 'Veracruz', 'Tlilapan', 1, 0, 0, 0, 1),
(12, 'Veracruz', 'Soledad Atzompa	', 1, 1, 0, 0, 0),
(13, 'Veracruz', 'Río Blanco', 1, 0, 0, 0, 1),
(14, 'Queretaro', 'Amealco de Bonfil', 1, 0, 1, 0, 0),
(15, 'Queretaro', 'Querétaro', 1, 1, 0, 0, 0),
(16, 'Tlaxcala', 'Chiautempan', 1, 0, 0, 1, 0),
(17, 'Hidalgo', 'Pachuca de Soto', 1, 0, 1, 1, 0),
(18, 'Nuevo Leon', 'Monterrey', 2, 0, 2, 0, 0),
(19, 'Puebla', 'Puebla', 1, 0, 0, 1, 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `boletin1`
--
ALTER TABLE `boletin1`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `graficas`
--
ALTER TABLE `graficas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `municipios_veracruz`
--
ALTER TABLE `municipios_veracruz`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `boletin1`
--
ALTER TABLE `boletin1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=260;
--
-- AUTO_INCREMENT de la tabla `graficas`
--
ALTER TABLE `graficas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `municipios_veracruz`
--
ALTER TABLE `municipios_veracruz`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
