create table miembros(
	id int not null primary key,
	tipo varchar(100),
	nombre varchar(100),
	representante varchar(100),
	estado varchar(100), 
	municipio varchar(100),
	localidad varchar(100),
	cve_geo int(10),
	correo varchar(100),
	blog varchar(100),
	redes varchar(150),
	twitter varchar(100),
	mision text,
	latitud varchar(100),
	longitud varchar(100)
);