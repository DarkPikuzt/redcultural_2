 <?php 
 //aqui se incluye las consultas de la base de datos para los miembros de la Red cultural
    include ("php/marker_miembros.php");
 //Aui se incluye las consultas de la base de datos para cada categoria
    include ("php/marcadores.php");
 //Aui se incluyen las consultas para las graficas de cada estado   
    include ("php/consultas.php");
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Mapa de los estados asociados</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <!--script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script-->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="estilo.css">
  <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
  <!--p> templete a utiliza  -- https://www.w3schools.com/bootstrap/tryit.asp?filename=trybs_temp_webpage&stacked=h</p-->
  <!--script type="text/javascript" src="js/cargarmapa.js"></script-->

  <script src="https://cdnjs.cloudflare.com/ajax/libs/OverlappingMarkerSpiderfier/1.0.3/oms.min.js"></script>
  <!--link de los archivos de los marcadores-->
  <script id="marc" type="text/javascript" src="js/marcadores.js"></script>
  

  <script src="js/markerclusterer.min.js"></script>
  <script type="text/javascript" src="js/evento_publico.js" ></script>
  <!--link para los marcadores de los miembros recultivar-->
  <script type="text/javascript" src="js/markers_miembros.js"></script>
  <script type="text/javascript" src="js/mostrar_ocultar_markers_miembros.js"></script>
  
   <!--script type="text/javascript" src="js/graficas.js"></script-->
   <!--libreria para el sobreposicionamiento-->
  <!--script  id="oms" src="https://jawj.github.io/OverlappingMarkerSpiderfier/bin/oms.min.js"></script-->
  
</head>
<body>
    
    <header style="border: 1px solid #EDEFEF;">
      <div>
          <nav>
            <center>
              <ul>
                 <li style="margin-left: -30px;"><p><a href="#" id=""><img src="img/Luv.jpg" height="78" width="88"></a></p></li>
                 <!--li ><div><p style="border: 1px solid;" height="90"><h5>Cartografía de la gestión cultural.</h5>México, Red de Cultura Viva Comunitaria</p></div></li-->
                 <li><div><div height="90" width="280"><p><h3><strong>Cartografía de la gestión cultural. México, Red de Cultura Viva Comunitaria.</strong></h3></p></div></div></li>
                 <li><p>&nbsp;&nbsp;&nbsp;<a href="https://www.uv.mx/opc" title="ir a la página"><img src="img/opc.jpg" height="60" width="78"></a></p></li>
                 <li><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://www.uv.mx/apps/cuo/opc/recultivar_mexico/integrantesAsociados.php" title="ir a la página"><img src="img/recultivar.png" height="89" width="64"></a></p></li>
                 <li><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://www.uv.mx/cuo/" title="ir a la página"><img src="img/LOGO.png" height="67" width="85"></a></p></li>
              </ul> 
            </center>
          </nav>
      </div>
    </header><br>



    <!-- Modal  de cargador-->



    <script>




    </script>




   
    <div class="container col-md-12" id="m_2018">
      <div id=""></div>
    <div class="row" style="width: 100%;" id="">
        <div id="comunicacion"></div>
         <div id="div_008"></div>
        <div class="col-sm-3 col-md-3" style="background-color:" id="menu2018">
            <!--select name="cambioanio" id="cambioanio">
              <option>2018</option>
              <option>2019</option>
            </select-->


            <style>
                .per{
                    color: #ffffff;
                    size: 1rem;
                    padding: 1rem;
                }
            </style>
            <div class="panel-group" id="accordion">
                <div class="text-center">
                    <strong class="per" > Periodo</strong>
                        <select name="cambioanio" id="cambioanio" class="form">
                            <option value="2020">2020</option>
                            <option value="2019">2019</option>
                            <option value="2018">2018 </option>
                        </select>
                </div>
                <style>
                    .muestra{
                        position: absolute;
                        left: 37%;
                        top: 300px;
                    }
                </style>
                <!-- Modal -->
                <div id="myModal2" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-body muestra text-center">
                                <img src="img/loading-25.gif" style="width: 100px; height: 100px" class="img-resposive">
                            </div>

                        </div>

                    </div>
                </div>

                <script>

                    $( "#cambioanio" ).change(function() {
                        $("#myModal2").modal("show")
                        window.location = 'act_2019/mapa2019.php?valor=' + document.getElementById('cambioanio').value;return false;




                    });






                </script>
                
                <!--p class="text-center" style="font-size: 11pt;color: #F5FFF6;"><strong>Eventos de los miembros de la Red 2018</strong></p-->
                <div class="panel panel-default">
                    <div class="panel-heading" id="bd">
                        <h4 class="panel-title">
                            <a data-toggle="modal" href="#" data-target="#graf" id="dom"><span class="glyphicon glyphicon-list-alt" style="color: #088D4A;">
                            </span><strong>Instrucciones</strong></a>
                        </h4>
                    </div>
                    <div id="collapse6" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="table">
                              
                            </table>
                        </div>
                    </div>
                </div>
               
                <div class="panel panel-default">
                    <div class="panel-heading" id="bd">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" id="dom"><span class="glyphicon glyphicon-th" style="color: #088D4A;">
                            </span><strong>Dominio Cultural del evento</strong></a>&nbsp;<!--i class="fa fa-question-circle-o" data-toggle="tooltip" title="Tipo" id="tooltip" style="  font-size: 20px; cursor: pointer;"></i-->
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="table">
                                <tr id="" class="">
                                    <td>
                                      <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="intangible" name="check"> <span class="label-text">Patrimonio Intangible</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="g" class="fa fa-question-circle-o tro too"><span class="tool">Todo aquel patrimonio que debe salvaguardarse y consiste en el reconocimiento de los usos, representaciones, expresiones, conocimientos y técnicas transmitidos de generación en generación y que infunden a las comunidades y a los grupos un sentimiento de identidad y continuidad, contribuyendo así a promover el respeto a la diversidad cultural y la creatividad humana.  UNESCO. Convención para la salvaguardia del patrimonio cultural inmaterial 2003.</span></i>
                                        </label>
                                      </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                      <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="natural" name="check"><span class="label-text">Patrimonio Natural</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="n" class="fa fa-question-circle-o n"><span class="np">Acciones de las comunidades humanas para la salvaguarda, goce y divulgación de los servicios ecosistémicos.</span></i>
                                        </label>
                                      </div>
                                    </td>
                                </tr>
                                <!--tr>
                                    <td>
                                      <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="tangible" name="check"> <span class="label-text">Patrimonio Tangible</span>
                                        </label>
                                      </div> 
                                    </td>
                                </tr-->
                                <tr>
                                    <td>
                                      <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="educar" name="check"> <span class="label-text">Educación</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="e" class="fa fa-question-circle-o e"><span class="ed">Acciones encaminadas a la transformación permanente de la conducta y actitudes humanas, mediante el conocimiento de diversas realidades (natural, social, cultural).</span></i>
                                        </label>
                                      </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                      <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="artess" name="check"> <span class="label-text">Artes</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="a" class="fa fa-question-circle-o a"><span class="aa">Expresiones humanas de carácter estético.</span></i>
                                        </label>
                                      </div> 
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" id="bd">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" id="dom"><span class="glyphicon glyphicon-th" style="color: #088D4A;">
                            </span><strong>Público objetivo de los eventos</strong></a>
                        </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="table">
                                <!--tr>
                                    <td>
                                      <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="object" name="check"> <span class="label-text">Público objetivo </span>
                                        </label>
                                      </div> 
                                    </td>
                                </tr-->
                                <tr>
                                  <td>
                                    <label>
                                      <input type="checkbox" id="infan" name="check"> <span class="label-text">Infantes</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="i"  class="fa fa-question-circle-o i"><span class="ii">Personas de hasta 12 años.</span></i>
                                    </label>
                                  </td>
                                </tr>
                                 <tr>
                                  <td>
                                    <label>
                                      <input type="checkbox" id="jven" name="check"> <span class="label-text">Jóvenes</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="j" class="fa fa-question-circle-o j"><span class="jj">Personas a partir de 13 años.</span></i>
                                    </label>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <label>
                                      <input type="checkbox" id="mjer" name="check"> <span class="label-text">Mujeres</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="m" class="fa fa-question-circle-o m"><span class="mm">Género femenino.</span></i>
                                    </label>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <label>
                                      <input type="checkbox" id="may" name="check"> <span class="label-text">Adultos Mayores</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="am" class="fa fa-question-circle-o am"><span class="amy">Personas a partir de 60 años.</span></i>
                                    </label>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <label>
                                      <input type="checkbox" id="pgral" name="check"> <span class="label-text">Público en general</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="pg" class="fa fa-question-circle-o pg"><span class="pgp">No se especifica un público y se asume que puede asistir cualquier persona interesada.</span></i>
                                    </label>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <label>
                                      <input type="checkbox" id="potra" name="check"> <span class="label-text">Otra</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="o" class="fa fa-question-circle-o o"><span class="ol">Especializado como gestores, creadores, académicos, sólo adultos.</span></i>
                                    </label>
                                  </td>
                                </tr>
                              </table>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" id="bd">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" id="dom"><span class="glyphicon glyphicon-th" style="color: #088D4A;">
                            </span><strong>Intención Principal del gestor</strong></a>
                        </h4>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="table">
                                <tr>
                                    <td>
                                      <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="difundir" name="check"><span class="label-text">Difundir</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="d" class="fa fa-question-circle-o d"><span class="df">Difundir y Divulgar.</span></i>
                                        </label>
                                      </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                      <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="educacion" name="check"><span class="label-text">Educación</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="ed" class="fa fa-question-circle-o edt"><span class="edn">Educación y/o formación de públicos.</span></i>
                                        </label>
                                      </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                      <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="expresion" name="check"> <span class="label-text">Expresion creativa</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="ex" class="fa fa-question-circle-o ex"><span class="ec">Disfrute y expresión recreativa.</span></i>
                                        </label>
                                      </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="linea" name="check"> <span class="label-text">Contenido en linea</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="cl" class="fa fa-question-circle-o cl"><span class="clc">Producción de contenidos en línea.</span></i>
                                        </label>
                                      </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                      <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="preservacion" name="check"> <span class="label-text">Preservacion</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="pr"  class="fa fa-question-circle-o pr"><span class="pq">Preservación yRestauración.</span></i>
                                        </label>
                                      </div> 

                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                      <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="otro" name="check"> <span class="label-text">Otro</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="ot" class="fa fa-question-circle-o ot"><span class="oto">Entrega de premio nacional de librería, grabación de disco, taller de sonido y grabación, multiplataforma de fondeo cultural.</span></i>
                                        </label>
                                      </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default" >
                    <div class="panel-heading" id="bd">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse5" id="dom"><span class="glyphicon glyphicon-th" style="color: #088D4A;">
                            </span><strong>Modalidades de acceso y participación en la vida cultural</strong></a>
                        </h4>
                    </div>
                    <div id="collapse5" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="table">
                                <tr>
                                    <td>
                                      <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="taller" name="check"> <span class="label-text">Cursos y talleres</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="ct" class="fa fa-question-circle-o ct"><span class="cte">Cursos y talleres.</span></i>
                                        </label>
                                      </div> 
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                      <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="festival" name="check"> <span class="label-text">Festivales</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="fes" data-placement="right" class="fa fa-question-circle-o  fes"><span class="fess">Festivales.</span></i>
                                        </label>
                                      </div> 
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                      <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="foro" name="check"> <span class="label-text">Foros</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="for" class="fa fa-question-circle-o for"><span class="fos">Foros, seminarios, coloquios y conferencias.</span></i>
                                        </label>
                                      </div> 
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                      <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="prestacion" name="check"> <span class="label-text">Presentaciones</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="pre" data-placement="right" class="fa fa-question-circle-o pre"><span class="prp">Presentaciones, exposiciones, conciertos y proyecciones de audiovisuales.</span></i>
                                        </label>
                                      </div> 
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                      <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="artistica" name="check"> <span class="label-text">Producción artistica</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="prod" class="fa fa-question-circle-o prod"><span class="produ">Logística de producción artística y Creación de servicios culturales.</span></i>
                                        </label>
                                      </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                      <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="restauracion" name="check"> <span class="label-text">Restauración</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="rcion" data-placement="right" class="fa fa-question-circle-o rcion"><span class="rciono">Curaduría y restauración.</span></i>
                                        </label>
                                      </div>
                                    </td>
                                </tr>
                                <tr>
                                  <td>
                                      <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="otrofest" name="check"> <span class="label-text">Otro</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="tt" class="fa fa-question-circle-o tro"><span class="toot">cinedebate, jugar, bailar, elaborar algún altares, caminatas y recorridos, improvisación artística o participación activa de los participantes y audiencia que pueden combinar dos modalidades de las anteriores.</span></i>
                                        
                                        </label>
                                      </div>
                                  </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                 <div class="panel panel-default" >
                    <div class="panel-heading" id="bd">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapsesix" id="dom"><span class="glyphicon glyphicon-th" style="color: #088D4A;">
                            </span><strong>Miembros Recultivar</strong></a>
                        </h4>
                    </div>
                    <div id="collapsesix" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="table">
                                <tr>
                                    <td>
                                      <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="cread" name="check"> <span class="label-text">Creador</span><!--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="ct" class="fa fa-question-circle-o ct"><span class="cte">Cursos y talleres.</span></i>-->
                                        </label>
                                      </div> 
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                      <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="emprend" name="check"> <span class="label-text">Emprendedor cultural</span><!--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="fes" data-placement="right" class="fa fa-question-circle-o  fes"><span class="fess">Festivales.</span></i>-->
                                        </label>
                                      </div> 
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                      <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="gest" name="check"> <span class="label-text">Gestor</span><!--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="for" class="fa fa-question-circle-o for"><span class="fos">Foros, seminarios, coloquios y conferencias.</span></i>-->
                                        </label>
                                      </div> 
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                      <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="prom" name="check"> <span class="label-text">Promotor/animador cultural</span><!--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="pre" data-placement="right" class="fa fa-question-circle-o pre"><span class="prp">Presentaciones, exposiciones, conciertos y proyecciones de audiovisuales.</span></i>-->
                                        </label>
                                      </div> 
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                
                <!--Para cada panel collapse aqui termina-->
            </div>
        </div>
        <!--Aqui esta el html donde se ejecutan las ventanas modales con las graficas de cada uno de los estados-->
        <div class="col-sm-9 col-md-9" id="contenido">
          <div id="map"></div> 
          <div id="info-box" style="visibility: hidden;">?</div>
          <button style="visibility: hidden;">aceptar</button>
          <button id="ver" style=" visibility: hidden;">ver</button>
          <button id="ventveracruz" type="button" class="btn" data-toggle="modal" data-target="#myModal" hidden="true" style="visibility: hidden;">Veracruz</button>
          <button id="que" style="visibility: hidden;">quer</button>
          <button id="ventqueretaro" type="button" class="btn" data-toggle="modal" data-target="#myModal" hidden="true" style="visibility: hidden;">Queretaro</button>
          <button id="tlax"  style="visibility: hidden;">tlax</button>
          <button id="ventlax" type="button" class="btn" data-toggle="modal" data-target="#myModal" hidden="true" style="visibility: hidden;">tlaxcala</button>
          <button id="hidal" style="visibility: hidden;">hidal</button>
          <button id="venhidal" type="button" class="btn" data-toggle="modal" data-target="#myModal" hidden="true" style="visibility: hidden;">hidalgo</button>
          <button id="leon" style="visibility: hidden;"></button>
          <button id="venleon" type="button" class="btn" data-toggle="modal" data-target="#myModal" hidden="true" style="visibility: hidden;">nuevo Leon</button>
          <button id="pueb" style="visibility: hidden;">pueb</button>
          <button id="ventpuebla" type="button" class="btn" data-toggle="modal" data-target="#myModal" hidden="true" style="visibility: hidden;">puebla</button> 

        </div>
    </div>
</div>
<!--Este es un modal para mostrar las graficas de recultivar mexico-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" id="tamano">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><strong>Graficas de los estados de la red Recultivar México</strong></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
           
        <div class="row">
          <div class="col-sm-6" style="">
            <div style="" id="divcont" class="">
              <p class="text-center"><strong>Miembros de la red Recultivar México</strong> <i data-toggle="tooltip"  data-placement="right" title="Tipo de gestión realizada por cada Gestor, de acuerdo con su propia autoadscripción (Reconocimiento que hace la población de pertenecer a una forma de hacer gestión, con base en sus concepciones." class="fa fa-question-circle-o f" style="  font-size: 20px; cursor: pointer;"></i></p>
              <!--div id="chartContainer" style="height: 450px; width: 90%;"></div-->
              <div id="chartContainer" style="height: 400px; width: 100%;"></div>
            </div>
          </div>
          <div class="col-sm-6" style="">
            <div id="divcont">
             <p class="text-center"><strong>Tipo de población atendida</strong> <i data-toggle="tooltip" data-placement="right" title="Principales grupos de población atendida por cada gestor de la red." class="fa fa-question-circle-o f" style="  font-size: 20px; cursor: pointer;" ></i> </p>
             <!--div id="chartContainer2" style="height: 450px; width: 90%;"></div-->
             <div id="chartContainer2" style="height: 400px; width: 100%;"></div>
            </div> 
          </div>
          
         
        </div><br>
        <div class="row">
           <div class="col-sm-4">
             <div id="municip" style="height: 400px; width: 90%;"></div>
           </div>
          <div class="col-sm-8">
            <div id="divcont3">
              <p class="text-center"><strong>Tipos de miembros por municipio</strong> <i class="fa fa-question-circle-o" data-toggle="tooltip" title="Número de gestores de acuerdo al tipo de gestión que realizan por cada municipio." id="tooltip" style="  font-size: 20px; cursor: pointer;"></i></p>
              <div><div id="chartContainer3" style="height: 405px; width: 100%;"></div></div>
            </div> 
          </div>
        </div>
                              
                    
                    
                  
            
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <!--button type="button" class="btn btn-primary">Send message</button-->
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="graf" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" id="modalinstrucciones">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><h4><strong>Guía de uso del sistema de cartografía de la gestión cultural</strong></h4></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <p align="justify" style="font-size: 15;">
            <strong>1. Consultar los eventos de los miembros de Red en los Estados de la República.</strong><br>
           Si desea visualizar los eventos de la Red, seleccione la categoría correspondiente en el menú de la Izquierda.<br><br>
           <strong>2. Consultar los miembros de la red y su tipo de organización por Estados de la República.</strong><br>
            Coloque el puntero del mouse sobre el estado de su interés y de un click como se muestra en la figura siguiente para consultar la información asociada.<br><br>
            <img src="img/grafica.png" width="" height="">
          </p>   
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">Cerrar</button>
        <!--button type="button" class="btn btn-primary">Send message</button-->
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="msjegrafica" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" id="">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><h3><strong>Presentación</strong></h3></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <p align="justify" style="font-size: 15;">
            Si desea conocer la informacion de los miembros de la Red Recultivar Mexico, Primero, seleccione uno de los Estados de la República Mexicana mostrados en el mapa. 
          </p>   
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">Cerrar</button>
        <!--button type="button" class="btn btn-primary">Send message</button-->
      </div>
    </div>
  </div>
</div>
<div id="">
  <div id="cargar_2019"></div>
</div>

<div class="footer">
  <div class="footer-copyright text-center py-3"><br>
    <strong>© 2019 Universidad Veracruzana. Todos los derechos reservados.</strong><br>
      <a id="foo"><strong></strong></a>
  </div>
</div>
<!--script type="text/javascript">
  $(document).ready(function(){
    $("#cargar_2019").load('mapa.php');
  });
</script-->


<!--Este script se obtienen los marcadores en json desde php-->
 <script type="text/javascript">
    var marcador = <?php echo json_encode($marcadorint);?>;
    //console.log(marcador);
    var markernat = <?php echo json_encode($natural);?>;
    //console.log(markernat);
    var markeredu = <?php echo json_encode($educar);?>;
    //console.log(markeredu);
    var markerarte = <?php echo json_encode($artes);?>;
    //console.log(markerarte);
    var event1 = <?php echo json_encode($aobj_event1);?>;
    //console.log(event1);
    var event2 = <?php echo json_encode($aobj_event2);?>;
    //console.log(event2);
    var event3 = <?php echo json_encode($aobj_event3);?>;
    //console.log(event3);
    var event4 = <?php echo json_encode($aobj_event4);?>;
    //console.log(event4);
    var event5 = <?php echo json_encode($aobj_event5);?>;
    //console.log(event5);
    var event6 = <?php echo json_encode($aobj_event6);?>;
    //console.log(event6);
    var cursos = <?php echo json_encode($prins1);?>;
    //console.log(cursos);
    var festival = <?php echo json_encode($prins2);?>;
    //console.log(festival);
    var foro = <?php echo json_encode($prins3);?>;
    //console.log(foro);
    var presentaciones = <?php echo json_encode($prins4);?>;
    //console.log(presentaciones);
    var artistica = <?php echo json_encode($prins5);?>;
    //console.log(artistica);
    var restauracion = <?php echo json_encode($prins6);?>;
    //console.log(restauracion);
    var otrofestival = <?php echo json_encode($prins7);?>;
    //console.log(otrofestival);
    var marker = <?php echo json_encode($marcador);?>;
    //console.log(marker);
    var markerinfantes = <?php echo json_encode($p_infantes);?>;
    //console.log(markerinfantes);
    var markerjovenes = <?php echo json_encode($p_jovenes);?>;
    //console.log(markerjovenes); 
    var markermujeres = <?php echo json_encode($p_mujeres);?>;
    //console.log(markermujeres);
    var markermayores = <?php echo json_encode($p_mayores);?>;
    //console.log(markermayores);
    var markergeneral = <?php echo json_encode($p_general);?>;
    //console.log(markergeneral);
    var markerotra = <?php echo json_encode($p_otra);?>;
    //console.log(markerotra);
    var markeremp = <?php echo json_encode($emprend);?>;
    //console.log(markeremp);


    var markercread = <?php echo json_encode($cread);?>; 
    console.log(markercread);
    var markergest = <?php echo json_encode($gest);?>;
    console.log(markergest);
    var markerpromo = <?php echo json_encode($promo);?>;
    console.log(markerpromo);

    var mar = [];
    var intang = [];
    var edu = [];
    var art = [];
    var pub = [];
    var even1 = [];
    var even2 = [];
    var even3 = [];
    var even4 = [];
    var even5 = [];
    var even6 = [];
    var curs = [];
    var fest = [];
    var conf = [];
    var pres = [];
    var arti = [];
    var rest = [];
    var infante = [];
    var joven = [];
    var muj = [];
    var mayores = [];
    var pgeneral = [];
    var otrobj = [];
    var otrofest = [];
    //variables para los marcadores de los miembros de la red.
    var emprend = [];
    var creadr = [];
    var gesto = [];
    var animado = [];
    //////////////////////////////////
    var map;
    var infowindow;
    function initMap() {
    //marcadores();
        //var gm = google.maps;
         var markerClusterer;
        var clusterMarker = [];
        map = new google.maps.Map(document.getElementById('map'), {
         zoom: 6,
         ///center: {lat: 23.0000000, lng: -102.0000000},
         //center: {lat: 21.9683933, lng: -100.9611975},
         //center: {lat: 22.2122047, lng: -99.4920208},
         center: {lat: 21.8260013, lng: -98.9439123},
         maptype:"TERRAIN"

        });
        
        //marker_emprendedor();
        //marker_creador();
        //marker_gestor();
        //marker_promotor();

        //codigo de la libreria para mostrar los marcadores sobrepoblados
        //var iw = new gm.InfoWindow();
        var oms = new OverlappingMarkerSpiderfier(map,
        {markersWontMove: true, markersWontHide: true});

        /*window.map = map;
        window.oms = oms;*/
       var markers = [];
        ////////////////////////////////
        /*Aqui se mandan a llamar los estados asociados a la informacion para ser pintados*/
        '<div id="vesi">'
        var veracruz = document.createElement('script');
        veracruz.setAttribute(
        'src',
        'estados/veracruz.json');
        '</div>'
        var list = document.getElementsByTagName('head')[0].appendChild(veracruz);

        var queretaro = document.createElement('script');
        queretaro.setAttribute(
        'src',
        'estados/queretaro.json');

        document.getElementsByTagName('head')[0].appendChild(queretaro);

       var tlaxcala = document.createElement('script');
        tlaxcala.setAttribute(
        'src',
        'estados/tlaxcala.json');

        document.getElementsByTagName('head')[0].appendChild(tlaxcala);

        var hidalgo = document.createElement('script');
        hidalgo.setAttribute(
        'src',
        'estados/hidalgo.json');

        document.getElementsByTagName('head')[0].appendChild(hidalgo);

        var nuevo_leon = document.createElement('script');
        nuevo_leon.setAttribute(
        'src',
        'estados/nuevo_leon.json');

        document.getElementsByTagName('head')[0].appendChild(nuevo_leon);

        var puebla = document.createElement('script');
        puebla.setAttribute(
        'src',
        'estados/puebla.json');

        document.getElementsByTagName('head')[0].appendChild(puebla);
        /*Aqui termina el codigo de la carga de los estados*/
        color();
        //llamada de informacion para pintar los marcadores
       
        //aqui empieza las funciones para mostrar las capas del dominio cultural del evento
        mostrar_y_ocultar_domin_cult_evet(oms,markers);
        //aqui termoiman las functiones para mostrar
         //aqui empiezan las funciones para mostrar las capas de publico objetivo de los elementos.
        mostrar_y_ocultar_public_objet_event(oms,markers);
        //functiones para mostrar intencion pincipal del gestor
        mostrar_y_ocultar_int_prin_gest(oms,markers);
        //aqui terminan las funciones
        //aqui emiezan las funciones para mostrar modalidades de acceso y participación en la vida cultural.
        mostrar_y_ocultar_modalidades_de_acceso(oms,markers);
        //aqui se terminat las funciones de mostrar y ocultar las capas
        //funciones que muestran los puntos de los miembros de la red.
        mostrar_y_ocultar_emprendedor();
        mostrar_y_ocultar_creador();
        mostrar_y_ocultar_gestor();
        mostrar_y_ocultar_promo();
        //function que muestra las graficas
        mostrargraficas();


    }//<----- Aqui termina la function initMap 
    //codigo necesario para que se nuetren pintados los estados
    function eqfeed_callback(data) {
        map.data.addGeoJson(data);
    }
    ///////////////////
</script>
<!--Aqui empiezan las graficas de cada estado con la api de camvas-->
<script type="text/javascript">
 
$(document).ready(function(){
   $("#ventveracruz").click(function(){
         //alert("comprobar");
         var chart = new CanvasJS.Chart("chartContainer2", {
      title:{
          text: 'Estado de Veracruz'
        },
        animationEnabled: true,
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        axisY: {
          title: "Miembros registrados"
        },
        data: [{        
          type: "column", 
          showInLegend: true, 
          legendMarkerColor: "grey",
           legendText: "Total Registrados : 42",
          dataPoints: <?php echo json_encode($entidades, JSON_NUMERIC_CHECK); ?>
               
        }]
      });
      chart.render();
      //poblacion atendida
      var chart = new CanvasJS.Chart("chartContainer", {
      title:{
          text: 'Estado de Veracruz'
        },
        animationEnabled: true,
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        axisY: {
         //title: "Miembros registrados"
        },
        data: [{        
          type: "column", 
          showInLegend: true, 
          legendMarkerColor: "grey",
          legendText: "Total Registrados : 42",
          dataPoints: <?php echo json_encode($miembros, JSON_NUMERIC_CHECK); ?>
         
                
        }]
      });
      chart.render();
      //municipios establecidos
  var chart = new CanvasJS.Chart("chartContainer3", {
  axisX: {
      interval: 1
  },
  title: {
    //text: "Spending of Money Based on Household Composition"
  },
  theme: "light2",
  animationEnabled: true,
  toolTip:{
    shared: true,
    reversed: true
  },
  axisY: {
    title: "Miembros registrados"
  },
  data: [
    {
      type: "stackedColumn",
      name: "creador",
      showInLegend: true,
      //yValueFormatString: "$#,##0 K",
      dataPoints: <?php echo json_encode($dataPoints1, JSON_NUMERIC_CHECK); ?>
       
    },{
      type: "stackedColumn",
      name: "emprendedor",
      showInLegend: true,
      //yValueFormatString: "$#,##0 K",
      dataPoints: <?php echo json_encode($dataPoints2, JSON_NUMERIC_CHECK); ?>
      
    },{
      type: "stackedColumn",
      name: "gestor",
      showInLegend: true,
      //yValueFormatString: "$#,##0 K",
      dataPoints: <?php echo json_encode($dataPoints3, JSON_NUMERIC_CHECK); ?>
     
    },{
      type: "stackedColumn",
      name: "promotor",
      showInLegend: true,
     // yValueFormatString: "$#,##0 K",
      dataPoints: <?php echo json_encode($dataPoints4, JSON_NUMERIC_CHECK); ?>
       

    }
  ]
});
 
chart.render();

     });
    

});
</script>
<script type="text/javascript">
$(document).ready(function() {
  $("#ventqueretaro").click(function() {
    //alert("queretaro");
     var chart = new CanvasJS.Chart("chartContainer2", {
      title:{
          text: 'Estado de Querétaro'
        },
        animationEnabled: true,
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        axisY: {
          title: "Miembros registrados"
        },
        data: [{        
          type: "column", 
          showInLegend: true, 
          legendMarkerColor: "grey",
          legendText: "Total Registrados : 2",
          dataPoints: <?php echo json_encode($entidadesque, JSON_NUMERIC_CHECK); ?>
             
        }]
      });
      chart.render();
      //poblacion atendida
      var chart = new CanvasJS.Chart("chartContainer", {
      title:{
           text: 'Estado de Querétaro'
        },
        animationEnabled: true,
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        axisY: {
         //title: "Miembros registrados"
        },
        data: [{        
          type: "column", 
          showInLegend: true, 
          legendMarkerColor: "grey",
          legendText: "Total Registrados : 2",
          dataPoints: <?php echo json_encode($miembrosque, JSON_NUMERIC_CHECK); ?>
         
                
        }]
      });
      chart.render();
      //municipios establecidos
      var chart = new CanvasJS.Chart("chartContainer3", {
  title: {
    //text: "Spending of Money Based on Household Composition"
  },
  theme: "light2",
  animationEnabled: true,
  toolTip:{
    shared: true,
    reversed: true
  },
  axisY: {
    //suffix: "%"
  },
  data: [
    {
      type: "stackedColumn",
      name: "creador",
      showInLegend: true,
      //yValueFormatString: "$#,##0 K",
      dataPoints: <?php echo json_encode($que1, JSON_NUMERIC_CHECK); ?>
      
    },{
      type: "stackedColumn",
      name: "emprendedor",
      showInLegend: true,
      //yValueFormatString: "$#,##0 K",
      dataPoints: <?php echo json_encode($que2, JSON_NUMERIC_CHECK); ?>
      
    },{
      type: "stackedColumn",
      name: "gestor",
      showInLegend: true,
      //yValueFormatString: "$#,##0 K",
      dataPoints: <?php echo json_encode($que3, JSON_NUMERIC_CHECK); ?>
     
    },{
      type: "stackedColumn",
      name: "promotor",
      showInLegend: true,
     // yValueFormatString: "$#,##0 K",
     dataPoints: <?php echo json_encode($que4, JSON_NUMERIC_CHECK); ?>
       

    }
  ]
});
 
chart.render();



  });
});  
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $("#ventlax").click(function(event) {
     // alert("tlaxcala");
         var chart = new CanvasJS.Chart("chartContainer2", {
      title:{
          text: 'Estado de Tlaxcala'
        },
        animationEnabled: true,
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        axisY: {
          title: "Miembros registrados"
        },
        data: [{        
          type: "column", 
          showInLegend: true, 
          legendMarkerColor: "grey",
          legendText: "Total Registrados : 1",
          dataPoints: <?php echo json_encode($entidadestlaxcala, JSON_NUMERIC_CHECK); ?>
             
        }]
      });
      chart.render();
      //poblacion atendida
      var chart = new CanvasJS.Chart("chartContainer", {
      title:{
           text: 'Estado de Tlaxcala'
        },
        animationEnabled: true,
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        axisY: {
         //title: "Miembros registrados"
        },
        data: [{        
          type: "column", 
          showInLegend: true, 
          legendMarkerColor: "grey",
          legendText: "Total Registrados : 1",
         dataPoints: <?php echo json_encode($miembrostlax, JSON_NUMERIC_CHECK); ?>
          
                
        }]
      });
      chart.render();
      //municipios establecidos
       var chart = new CanvasJS.Chart("chartContainer3", {
  title: {
    //text: "Spending of Money Based on Household Composition"
  },
  theme: "light2",
  animationEnabled: true,
  toolTip:{
    shared: true,
    reversed: true
  },
  axisY: {
    //suffix: "%"
  },
  data: [
    {
      type: "stackedColumn",
      name: "creador",
      showInLegend: true,
      //yValueFormatString: "$#,##0 K",
      dataPoints: <?php echo json_encode($tlax1, JSON_NUMERIC_CHECK); ?>
      
    },{
      type: "stackedColumn",
      name: "emprendedor",
      showInLegend: true,
      //yValueFormatString: "$#,##0 K",
      dataPoints: <?php echo json_encode($tlax2, JSON_NUMERIC_CHECK); ?>
      
    },{
      type: "stackedColumn",
      name: "gestor",
      showInLegend: true,
      //yValueFormatString: "$#,##0 K",
      dataPoints: <?php echo json_encode($tlax3, JSON_NUMERIC_CHECK); ?>
     
    },{
      type: "stackedColumn",
      name: "promotor",
      showInLegend: true,
     // yValueFormatString: "$#,##0 K",
     dataPoints: <?php echo json_encode($tlax4, JSON_NUMERIC_CHECK); ?>
       

    }
  ]
});
 
chart.render();
  });
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $("#venhidal").click(function() {
      //alert("hidalgo");
       var chart = new CanvasJS.Chart("chartContainer2", {
      title:{
           text: 'Estado de Hidalgo'
        },
        animationEnabled: true,
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        axisY: {
          title: "Miembros registrados"
        },
        data: [{        
          type: "column", 
          showInLegend: true, 
          legendMarkerColor: "grey",
          legendText: "Total Registrados : 2",
          dataPoints: <?php echo json_encode($entidadeshidalgo, JSON_NUMERIC_CHECK); ?> 
            
        }]
      });
      chart.render();
      //poblacion atendida
      var chart = new CanvasJS.Chart("chartContainer", {
      title:{
           text: 'Estado de Hidalgo'
        },
        animationEnabled: true,
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        axisY: {
         //title: "Miembros registrados"
        },
        data: [{        
          type: "column", 
          showInLegend: true, 
          legendMarkerColor: "grey",
          legendText: "Total Registrados : 2",
          dataPoints: <?php echo json_encode($miembroshidal, JSON_NUMERIC_CHECK); ?>
          
                
        }]
      });
      chart.render();
      //municipios establecidos
      var chart = new CanvasJS.Chart("chartContainer3", {
  title: {
    //text: "Spending of Money Based on Household Composition"
  },
  theme: "light2",
  animationEnabled: true,
  toolTip:{
    shared: true,
    reversed: true
  },
  axisY: {
    //suffix: "%"
  },
  data: [
    {
      type: "stackedColumn",
      name: "creador",
      showInLegend: true,
      //yValueFormatString: "$#,##0 K",
      dataPoints: <?php echo json_encode($hidal1, JSON_NUMERIC_CHECK); ?>
    
    },{
      type: "stackedColumn",
      name: "emprendedor",
      showInLegend: true,
      //yValueFormatString: "$#,##0 K",
      dataPoints: <?php echo json_encode($hidal2, JSON_NUMERIC_CHECK); ?>
     
    },{
      type: "stackedColumn",
      name: "gestor",
      showInLegend: true,
      //yValueFormatString: "$#,##0 K",
      dataPoints: <?php echo json_encode($hidal3, JSON_NUMERIC_CHECK); ?>
  
    },{
      type: "stackedColumn",
      name: "promotor",
      showInLegend: true,
      // yValueFormatString: "$#,##0 K",
      dataPoints: <?php echo json_encode($hidal4, JSON_NUMERIC_CHECK); ?>
      

    }
  ]
});
 
chart.render();
 });  
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
   $("#venleon").click(function() {
     //alert("nuevo leon");
      var chart = new CanvasJS.Chart("chartContainer2", {
      title:{
          text: 'Estado de Nuevo León'
        },
        animationEnabled: true,
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        axisY: {
          title: "Miembros registrados"
        },
        data: [{        
          type: "column", 
          showInLegend: true, 
          legendMarkerColor: "grey",
          legendText: "Total Registrados : 2",
          dataPoints: <?php echo json_encode($entidadesleon, JSON_NUMERIC_CHECK); ?>
             
        }]
      });
      chart.render();
      //poblacion atendida
      var chart = new CanvasJS.Chart("chartContainer", {
      title:{
           text: 'Estado de Nuevo León'
        },
        animationEnabled: true,
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        axisY: {
         //title: "Miembros registrados"
        },
        data: [{        
          type: "column", 
          showInLegend: true, 
          legendMarkerColor: "grey",
          legendText: "Total Registrados : 2",
          dataPoints: <?php echo json_encode($miembrosleon, JSON_NUMERIC_CHECK); ?>
      
                
        }]
      });
      chart.render();
      //municipios establecidos
      var chart = new CanvasJS.Chart("chartContainer3", {
  title: {
    //text: "Spending of Money Based on Household Composition"
  },
  theme: "light2",
  animationEnabled: true,
  toolTip:{
    shared: true,
    reversed: true
  },
  axisY: {
    //suffix: "%"
  },
  data: [
    {
      type: "stackedColumn",
      name: "creador",
      showInLegend: true,
      //yValueFormatString: "$#,##0 K",
      dataPoints: <?php echo json_encode($leon1, JSON_NUMERIC_CHECK); ?>
      
    },{
      type: "stackedColumn",
      name: "emprendedor",
      showInLegend: true,
      //yValueFormatString: "$#,##0 K",
      dataPoints: <?php echo json_encode($leon2, JSON_NUMERIC_CHECK); ?>
       
    },{
      type: "stackedColumn",
      name: "gestor",
      showInLegend: true,
      //yValueFormatString: "$#,##0 K",
      dataPoints: <?php echo json_encode($leon3, JSON_NUMERIC_CHECK); ?>
      
    },{
      type: "stackedColumn",
      name: "promotor",
      showInLegend: true,
      // yValueFormatString: "$#,##0 K",
      dataPoints: <?php echo json_encode($leon4, JSON_NUMERIC_CHECK); ?>
       

    }
  ]
});
 
chart.render();
 });
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
   $("#ventpuebla").click(function() {
    // alert("puebla");
      var chart = new CanvasJS.Chart("chartContainer2", {
      title:{
           text: 'Estado de Puebla'
        },
        animationEnabled: true,
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        axisY: {
          title: "Miembros registrados"
        },
        data: [{        
          type: "column", 
          showInLegend: true, 
          legendMarkerColor: "grey",
          legendText: "Total Registrados : 1",
          dataPoints: <?php echo json_encode($entidadespuebla, JSON_NUMERIC_CHECK); ?>      
        }]
      });
      chart.render();
      //poblacion atendida
      var chart = new CanvasJS.Chart("chartContainer", {
      title:{
           text: 'Estado de Puebla'
        },
        animationEnabled: true,
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        axisY: {
         //title: "Miembros registrados"
        },
        data: [{        
          type: "column", 
          showInLegend: true, 
          legendMarkerColor: "grey",
          legendText: "Total Registrados : 2",
          dataPoints: <?php echo json_encode($miembrospuebla, JSON_NUMERIC_CHECK); ?>
                
        }]
      });
      chart.render();
      //municipios establecidos
      var chart = new CanvasJS.Chart("chartContainer3", {
  title: {
    //text: "Spending of Money Based on Household Composition"
  },
  theme: "light2",
  animationEnabled: true,
  toolTip:{
    shared: true,
    reversed: true
  },
  axisY: {
    //suffix: "%"
  },
  data: [
    {
      type: "stackedColumn",
      name: "creador",
      showInLegend: true,
      //yValueFormatString: "$#,##0 K",
      dataPoints: <?php echo json_encode($pueb1, JSON_NUMERIC_CHECK); ?>
    },{
      type: "stackedColumn",
      name: "emprendedor",
      showInLegend: true,
      //yValueFormatString: "$#,##0 K"
      dataPoints: <?php echo json_encode($pueb2, JSON_NUMERIC_CHECK); ?>
    },{
      type: "stackedColumn",
      name: "gestor",
      showInLegend: true,
      //yValueFormatString: "$#,##0 K",
      dataPoints: <?php echo json_encode($pueb3, JSON_NUMERIC_CHECK); ?>
    },{
      type: "stackedColumn",
      name: "promotor",
      showInLegend: true,
      // yValueFormatString: "$#,##0 K",
      dataPoints: <?php echo json_encode($pueb4, JSON_NUMERIC_CHECK); ?>

    }
  ]
});
 
chart.render();



   }); 
  });
</script>
<script type="text/javascript">
  window.onload = function() {
    $("#dominio").hide();
    $("#publico_objetivo").hide();
    $("#event").hide();
    $("#modalidades").hide();
    $("#veracruz_graficas").hide();
    $("#queretaro_graficas").hide();
    ///$("#menu2018").hide();
    //limpiar todos los puntos al inicio de la carga
    clearMarkers();
    Markers();
    educaMarkers();
    arteMarkers();
    clearpub();
    clearevent1();
    clearevent2();
    clearevent3();
    clearevent4();
    clearevent5();
    clearevent6();
    clearcurso();
    clearfesti();
    clearforo();
    clearprest();
    clearartist();
    clearestauracion();
    clearfestival();
    clearinfantes();
    clearjoven();
    clearmujer();
    clearmayores();
    cleargeneral();
    clearobj();
    clearnat(); 
    //$('#msjegrafica').modal('show'); 
  }
</script>
  
<script id="lib" async defer
   src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKrV88GHRaVGDBAmgj2_KmGnFnFOl4zvs&callback=initMap">>
</script>
</body>
</html>


