<?php

include ("conexion.php");

$sql = " SELECT * FROM brm_graficas where estado ='Veracruz'";
$resultado = mysqli_query($conectar,$sql);

$sql1 = " SELECT creador,emprendedor,gestor,promotor FROM brm_graficas where estado ='Veracruz'";
$resultado1 = mysqli_query($conectar,$sql);


$sql2 = " SELECT * FROM brm_municipios";
$resultado2 = mysqli_query($conectar,$sql2);


$sql3 = "SELECT * FROM brm_municipios";
$resultado3 = mysqli_query($conectar,$sql3);

while ($mostrar = mysqli_fetch_array($resultado)) {
		$id = $mostrar ["id"];
		$estado = $mostrar["estado"];
		$creador = $mostrar ["creador"];
		$emprendedor = $mostrar["emprendedor"];
		$gestor = $mostrar["gestor"];
		$promotor = $mostrar["promotor"];
		$mujeres = $mostrar["objMujeres"];
		$jovenes = $mostrar["objJovenes"];
		$infantes = $mostrar["objInfantes"];
		$mayores = $mostrar["objMayores"];
		$general = $mostrar["objGeneral"];
		$gestores = $mostrar["objGestores"];
		$calle = $mostrar["objCalle"];
		$otro = $mostrar["objOtro"];

		$entidades = array( //'id' => $id, 
							 //'estado' => $estado,
	  						 //array('y' => $creador, 'label' => 'creador'),
							 //array('y' => $emprendedor, 'label' => 'emprendedor'),
							 //array('y' => $gestor, 'label' => 'gestor'),
							 //array('y' => $promotor, 'label' => 'promotor'),
							 array('y' => $mujeres, 'label' => 'Mujeres'),
							 array('y' => $jovenes, 'label' => 'Jovenes'),
							 array('y' => $infantes, 'label' => 'Infantes'),
							 array('y' => $mayores, 'label' => 'Adultos Mayores'),
							 array('y' => $general, 'label' => 'Publico en general'),
							 array('y' => $gestores, 'label' => 'Gestores culturales'),
							 array('y' => $calle, 'label' => 'Poblacion en situacion de calle'),
							 //array('y' => $otro, 'label' => 'otro'),
							);

}

while ($mostrar = mysqli_fetch_array($resultado1)) {
		
		$creador = $mostrar ["creador"];
		$emprendedor = $mostrar["emprendedor"];
		$gestor = $mostrar["gestor"];
		$promotor = $mostrar["promotor"];
		

		$miembros = array( //'id' => $id, 
							 //'estado' => $estado,
	  						 array('y' => $creador, 'label' => 'Creador'),
							 array('y' => $emprendedor, 'label' => 'Emprendedor Cultural'),
							 array('y' => $gestor, 'label' => 'Gestor Cultural'),
							 array('y' => $promotor, 'label' => 'promotor Cultural'),
					    );

}

while ($mostrar = mysqli_fetch_array($resultado2)) {
	$municipio = $mostrar["municipio"];
	$miembro = $mostrar["miembros"];


}


$dataPoints1 = array(
	array("label"=> "Rafael Delgado", "y"=> 2),
	array("label"=> "Cordoba", "y"=> 1),
	array("label"=> "Coscomatepec", "y"=> 0),
	array("label"=> "Mariano Escobedo", "y"=> 0),
	array("label"=> "Camerino Z. Mendoza", "y"=> 2),
	array("label"=> "Ixhuatlancillo", "y"=> 1),
	array("label"=> "Orizaba", "y"=> 6),
	array("label"=> "Xalapa", "y"=> 4),
	array("label"=> "Veracruz", "y"=> 0),
	array("label"=> "Zongolica", "y"=> 1),
	array("label"=> "Tlilapan", "y"=> 0),
	array("label"=> "Soledad Atzompa", "y"=> 1),
	array("label"=> "Rio Blanco", "y"=> 0),
);
 //emprendedor
$dataPoints2 = array(
	array("label"=> "Rafael Delgado", "y"=> 1),
	array("label"=> "Cordoba", "y"=> 1),
	array("label"=> "Coscomatepec", "y"=> 0),
	array("label"=> "Mariano Escobedo", "y"=> 0),
	array("label"=> "Camerino Z. Mendoza", "y"=> 0),
	array("label"=> "Ixhuatlancillo", "y"=> 0),
	array("label"=> "Orizaba", "y"=> 3),
	array("label"=> "Xalapa", "y"=> 0),
	array("label"=> "Veracruz", "y"=> 0),
	array("label"=> "Zongolica", "y"=> 1),
	array("label"=> "Tlilapan", "y"=> 0),
	array("label"=> "Soledad Atzompa", "y"=> 0),
	array("label"=> "Rio Blanco", "y"=> 0),
);
 //gestor
$dataPoints3 = array(
	array("label"=> "Rafael Delgado", "y"=> 0),
	array("label"=> "Cordoba", "y"=> 1),
	array("label"=> "Coscomatepec", "y"=> 1),
	array("label"=> "Mariano Escobedo", "y"=> 1),
	array("label"=> "Camerino Z. Mendoza", "y"=> 2),
	array("label"=> "Ixhuatlancillo", "y"=> 0),
	array("label"=> "Orizaba", "y"=> 1),
	array("label"=> "Xalapa", "y"=> 0),
	array("label"=> "Veracruz", "y"=> 1),
	array("label"=> "Zongolica", "y"=> 0),
	array("label"=> "Tlilapan", "y"=> 0),
	array("label"=> "Soledad Atzompa", "y"=> 0),
	array("label"=> "Rio Blanco", "y"=> 0),
);
//promotor 
$dataPoints4 = array(
	array("label"=> "Rafael Delgado", "y"=> 1),
	array("label"=> "Cordoba", "y"=> 0),
	array("label"=> "Coscomatepec", "y"=> 0),
	array("label"=> "Mariano Escobedo", "y"=> 0),
	array("label"=> "Camerino Z. Mendoza", "y"=> 0),
	array("label"=> "Ixhuatlancillo", "y"=> 0),
	array("label"=> "Orizaba", "y"=> 6),
	array("label"=> "Xalapa", "y"=> 0),
	array("label"=> "Veracruz", "y"=> 0),
	array("label"=> "Zongolica", "y"=> 1),
	array("label"=> "Tlilapan", "y"=> 1),
	array("label"=> "Soledad Atzompa", "y"=> 0),
	array("label"=> "Rio Blanco", "y"=> 1),
);
 
//Estado de Queretaro
$sqlqueretaro = " SELECT * FROM brm_graficas where estado = 'Queretaro'";
$resultadoque = mysqli_query($conectar,$sqlqueretaro);


$sqlque = " SELECT creador,emprendedor,gestor,promotor FROM brm_graficas where estado ='Queretaro'";
$resultadoque1 = mysqli_query($conectar,$sqlque);

while ($mostrar = mysqli_fetch_array($resultadoque)) {
	$id = $mostrar ["id"];
		$estado = $mostrar["estado"];
		$creador = $mostrar ["creador"];
		$emprendedor = $mostrar["emprendedor"];
		$gestor = $mostrar["gestor"];
		$promotor = $mostrar["promotor"];
		$mujeres = $mostrar["objMujeres"];
		$jovenes = $mostrar["objJovenes"];
		$infantes = $mostrar["objInfantes"];
		$mayores = $mostrar["objMayores"];
		$general = $mostrar["objGeneral"];
		$gestores = $mostrar["objGestores"];
		$calle = $mostrar["objCalle"];
		$otro = $mostrar["objOtro"];

		$entidadesque = array( 

							 array('y' => $mujeres, 'label' => 'Mujeres'),
							 array('y' => $jovenes, 'label' => 'Jovenes'),
							 array('y' => $infantes, 'label' => 'Infantes'),
							 array('y' => $mayores, 'label' => 'Adultos Mayores'),
							 array('y' => $general, 'label' => 'Publico en general'),
							 array('y' => $gestores, 'label' => 'Gestores culturales'),
							 array('y' => $calle, 'label' => 'Poblacion en situacion de calle'),
							 
							);

}

//miembros
while ($mostrar = mysqli_fetch_array($resultadoque1)) {
		
		$creador = $mostrar ["creador"];
		$emprendedor = $mostrar["emprendedor"];
		$gestor = $mostrar["gestor"];
		$promotor = $mostrar["promotor"];
		

		$miembrosque = array( //'id' => $id, 
							 //'estado' => $estado,
	  						 array('y' => $creador, 'label' => 'Creador'),
							 array('y' => $emprendedor, 'label' => 'Emprendedor Cultural'),
							 array('y' => $gestor, 'label' => 'Gestor Cultural'),
							 array('y' => $promotor, 'label' => 'promotor Cultural'),
					    );

}
//Tipo de miembros por municipio
//creador
$que1 = array(
	array("label"=> "Amealco de Bonfil", "y"=> 0),
	array("label"=> "Querétaro", "y"=> 1),
	
);
 //emprendedor
$que2 = array(
	array("label"=> "Amealco de Bonfil", "y"=> 1),
	array("label"=> "Querétaro", "y"=> 0),
	
);
 //gestor
$que3 = array(
	array("label"=> "Amealco de Bonfil", "y"=> 0),
	array("label"=> "Querétaro", "y"=> 0),
	
);
//promotor 
$que4 = array(
	array("label"=> "Amealco de Bonfil", "y"=> 0),
	array("label"=> "Querétaro", "y"=> 0),
	
);

//estado de tlaxcala
$sqltlaxcala = " SELECT * FROM brm_graficas where estado = 'Tlaxcala'";
$resultadotlaxcala = mysqli_query($conectar,$sqltlaxcala);

$sqltlax = " SELECT creador,emprendedor,gestor,promotor FROM brm_graficas where estado ='Tlaxcala'";
$resultadotlax1 = mysqli_query($conectar,$sqltlax);

while ($mostrar = mysqli_fetch_array($resultadotlaxcala)) {
	$id = $mostrar ["id"];
		$estado = $mostrar["estado"];
		$creador = $mostrar ["creador"];
		$emprendedor = $mostrar["emprendedor"];
		$gestor = $mostrar["gestor"];
		$promotor = $mostrar["promotor"];
		$mujeres = $mostrar["objMujeres"];
		$jovenes = $mostrar["objJovenes"];
		$infantes = $mostrar["objInfantes"];
		$mayores = $mostrar["objMayores"];
		$general = $mostrar["objGeneral"];
		$gestores = $mostrar["objGestores"];
		$calle = $mostrar["objCalle"];
		$otro = $mostrar["objOtro"];

		$entidadestlaxcala = array( 

							 array('y' => $mujeres, 'label' => 'Mujeres'),
							 array('y' => $jovenes, 'label' => 'Jovenes'),
							 array('y' => $infantes, 'label' => 'Infantes'),
							 array('y' => $mayores, 'label' => 'Adultos Mayores'),
							 array('y' => $general, 'label' => 'Publico en general'),
							 array('y' => $gestores, 'label' => 'Gestores culturales'),
							 array('y' => $calle, 'label' => 'Poblacion en situacion de calle'),
							 
							);

}

//miembros
while ($mostrar = mysqli_fetch_array($resultadotlax1)) {
		
		$creador = $mostrar ["creador"];
		$emprendedor = $mostrar["emprendedor"];
		$gestor = $mostrar["gestor"];
		$promotor = $mostrar["promotor"];
		

		$miembrostlax = array( //'id' => $id, 
							 //'estado' => $estado,
	  						 array('y' => $creador, 'label' => 'Creador'),
							 array('y' => $emprendedor, 'label' => 'Emprendedor Cultural'),
							 array('y' => $gestor, 'label' => 'Gestor Cultural'),
							 array('y' => $promotor, 'label' => 'promotor Cultural'),
					    );

}

//Tipo de miembros por municipio
//creador
$tlax1 = array(
	array("label"=> "Chiautempan", "y"=> 0),
	
	
);
 //emprendedor
$tlax2 = array(
	array("label"=> "Chiautempan", "y"=> 0),
	
);
 //gestor
$tlax3 = array(
	array("label"=> "Chiautempan", "y"=> 1),
	
	
);
//promotor 
$tlax4 = array(
	array("label"=> "Chiautempan", "y"=> 0),
	
	
);
//Estado de hidalgo
$sqlhidalgo= " SELECT * FROM brm_graficas where estado = 'Hidalgo'";
$resultadohidalgo = mysqli_query($conectar,$sqlhidalgo);

$sqlhid = " SELECT creador,emprendedor,gestor,promotor FROM brm_graficas where estado ='Hidalgo'";
$resultadohid1 = mysqli_query($conectar,$sqlhid);


while ($mostrar = mysqli_fetch_array($resultadohidalgo)) {
	$id = $mostrar ["id"];
		$estado = $mostrar["estado"];
		$creador = $mostrar ["creador"];
		$emprendedor = $mostrar["emprendedor"];
		$gestor = $mostrar["gestor"];
		$promotor = $mostrar["promotor"];
		$mujeres = $mostrar["objMujeres"];
		$jovenes = $mostrar["objJovenes"];
		$infantes = $mostrar["objInfantes"];
		$mayores = $mostrar["objMayores"];
		$general = $mostrar["objGeneral"];
		$gestores = $mostrar["objGestores"];
		$calle = $mostrar["objCalle"];
		$otro = $mostrar["objOtro"];

		$entidadeshidalgo = array( 

							 array('y' => $mujeres, 'label' => 'Mujeres'),
							 array('y' => $jovenes, 'label' => 'Jovenes'),
							 array('y' => $infantes, 'label' => 'Infantes'),
							 array('y' => $mayores, 'label' => 'Adultos Mayores'),
							 array('y' => $general, 'label' => 'Publico en general'),
							 array('y' => $gestores, 'label' => 'Gestores culturales'),
							 array('y' => $calle, 'label' => 'Poblacion en situacion de calle'),
							 
							);

}
//miembros
while ($mostrar = mysqli_fetch_array($resultadohid1)) {
		
		$creador = $mostrar ["creador"];
		$emprendedor = $mostrar["emprendedor"];
		$gestor = $mostrar["gestor"];
		$promotor = $mostrar["promotor"];
		

		$miembroshidal = array( //'id' => $id, 
							 //'estado' => $estado,
	  						 array('y' => $creador, 'label' => 'Creador'),
							 array('y' => $emprendedor, 'label' => 'Emprendedor Cultural'),
							 array('y' => $gestor, 'label' => 'Gestor Cultural'),
							 array('y' => $promotor, 'label' => 'promotor Cultural'),
					    );

}
//Tipo de miembros por municipio
//creador
$hidal1 = array(
	array("label"=> "Pachuca de Soto", "y"=> 0),
	
	
);
 //emprendedor
$hidal2 = array(
	array("label"=> "Pachuca de Soto", "y"=> 1),
	
);
 //gestor
$hidal3 = array(
	array("label"=> "Pachuca de Soto", "y"=> 1),
	
	
);
//promotor 
$hidal4 = array(
	array("label"=> "Pachuca de Soto", "y"=> 0),
	
	
);

//estado de nuevo leon
$sqleon= " SELECT * FROM brm_graficas where estado = 'Nuevo Leon'";
$resultadoleon = mysqli_query($conectar,$sqleon);

$sqlnuevo = " SELECT creador,emprendedor,gestor,promotor FROM brm_graficas where estado ='Nuevo Leon'";
$resultadoleon1 = mysqli_query($conectar,$sqlnuevo);


while ($mostrar = mysqli_fetch_array($resultadoleon)) {
		$id = $mostrar ["id"];
		$estado = $mostrar["estado"];
		$creador = $mostrar ["creador"];
		$emprendedor = $mostrar["emprendedor"];
		$gestor = $mostrar["gestor"];
		$promotor = $mostrar["promotor"];
		$mujeres = $mostrar["objMujeres"];
		$jovenes = $mostrar["objJovenes"];
		$infantes = $mostrar["objInfantes"];
		$mayores = $mostrar["objMayores"];
		$general = $mostrar["objGeneral"];
		$gestores = $mostrar["objGestores"];
		$calle = $mostrar["objCalle"];
		$otro = $mostrar["objOtro"];

		$entidadesleon = array( 

							 array('y' => $mujeres, 'label' => 'Mujeres'),
							 array('y' => $jovenes, 'label' => 'Jovenes'),
							 array('y' => $infantes, 'label' => 'Infantes'),
							 array('y' => $mayores, 'label' => 'Adultos Mayores'),
							 array('y' => $general, 'label' => 'Publico en general'),
							 array('y' => $gestores, 'label' => 'Gestores culturales'),
							 array('y' => $calle, 'label' => 'Poblacion en situacion de calle'),
							 
							);

}
//miembros
while ($mostrar = mysqli_fetch_array($resultadoleon1)) {
		
		$creador = $mostrar ["creador"];
		$emprendedor = $mostrar["emprendedor"];
		$gestor = $mostrar["gestor"];
		$promotor = $mostrar["promotor"];
		

		$miembrosleon = array( //'id' => $id, 
							 //'estado' => $estado,
	  						 array('y' => $creador, 'label' => 'Creador'),
							 array('y' => $emprendedor, 'label' => 'Emprendedor Cultural'),
							 array('y' => $gestor, 'label' => 'Gestor Cultural'),
							 array('y' => $promotor, 'label' => 'promotor Cultural'),
					    );

}
//Tipo de miembros por municipio
//creador
$leon1 = array(
	array("label"=> "Monterrey", "y"=> 0),
	
	
);
 //emprendedor
$leon2 = array(
	array("label"=> "Monterrey", "y"=> 2),
	
);
 //gestor
$leon3 = array(
	array("label"=> "Monterrey", "y"=> 0),
	
	
);
//promotor 
$leon4 = array(
	array("label"=> "Monterrey", "y"=> 0),
	
	
);

//estado de puebla
$sqlpuebla= " SELECT * FROM brm_graficas where estado = 'Puebla'";
$resultadopuebla = mysqli_query($conectar,$sqlpuebla);

$sqlpueb = " SELECT creador,emprendedor,gestor,promotor FROM brm_graficas where estado ='Puebla'";
$resultadopueb1 = mysqli_query($conectar,$sqlpueb);

while ($mostrar = mysqli_fetch_array($resultadopuebla)) {
		$id = $mostrar ["id"];
		$estado = $mostrar["estado"];
		$creador = $mostrar ["creador"];
		$emprendedor = $mostrar["emprendedor"];
		$gestor = $mostrar["gestor"];
		$promotor = $mostrar["promotor"];
		$mujeres = $mostrar["objMujeres"];
		$jovenes = $mostrar["objJovenes"];
		$infantes = $mostrar["objInfantes"];
		$mayores = $mostrar["objMayores"];
		$general = $mostrar["objGeneral"];
		$gestores = $mostrar["objGestores"];
		$calle = $mostrar["objCalle"];
		$otro = $mostrar["objOtro"];

		$entidadespuebla = array( 

							 array('y' => $mujeres, 'label' => 'Mujeres'),
							 array('y' => $jovenes, 'label' => 'Jovenes'),
							 array('y' => $infantes, 'label' => 'Infantes'),
							 array('y' => $mayores, 'label' => 'Adultos Mayores'),
							 array('y' => $general, 'label' => 'Publico en general'),
							 array('y' => $gestores, 'label' => 'Gestores culturales'),
							 array('y' => $calle, 'label' => 'Poblacion en situacion de calle'),
							 
							);

}
//miembros
while ($mostrar = mysqli_fetch_array($resultadopueb1)) {
		
		$creador = $mostrar ["creador"];
		$emprendedor = $mostrar["emprendedor"];
		$gestor = $mostrar["gestor"];
		$promotor = $mostrar["promotor"];
		

		$miembrospuebla = array( //'id' => $id, 
							 //'estado' => $estado,
	  						 array('y' => $creador, 'label' => 'Creador'),
							 array('y' => $emprendedor, 'label' => 'Emprendedor Cultural'),
							 array('y' => $gestor, 'label' => 'Gestor Cultural'),
							 array('y' => $promotor, 'label' => 'promotor Cultural'),
					    );

}
//Tipo de miembros por municipio
//creador
$pueb1 = array(
	array("label"=> "Puebla", "y"=> 0),
	
	
);
 //emprendedor
$pueb2 = array(
	array("label"=> "Puebla", "y"=> 0),
	
);
 //gestor
$pueb3 = array(
	array("label"=> "Puebla", "y"=> 1),
	
	
);
//promotor 
$pueb4 = array(
	array("label"=> "Puebla", "y"=> 0),
	
	
);


?>
