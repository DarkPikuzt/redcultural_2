<div class="col-sm-3 col-md-3" style="background-color: #4CF2C5" id="menu2018">
<select name="cambioanio" id="cambioanio">
              <option>2018</option>
              <option>2019</option>
            </select>
            
            <div class="panel-group" id="accordion">
                
                <p class="text-center" style="font-size: 11pt;color: #F5FFF6;"><strong>Eventos de los miembros de la Red 2019</strong></p>
                <div class="panel panel-default" >
                    <div class="panel-heading" id="bd">
                        <h4 class="panel-title">
                            <a data-toggle="modal" href="#" data-target="#graf" id="dom"><span class="glyphicon glyphicon-list-alt" style="color: #088D4A;">
                            </span><strong>Instrucciones</strong></a>
                        </h4>
                    </div>
                    <div id="collapse6" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="table">
                              
                            </table>
                        </div>
                    </div>
                </div>
               
                <div class="panel panel-default">
                    <div class="panel-heading" id="bd">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" id="dom"><span class="glyphicon glyphicon-th" style="color: #088D4A;">
                            </span><strong>Dominio Cultural del evento</strong></a>&nbsp;<!--i class="fa fa-question-circle-o" data-toggle="tooltip" title="Tipo" id="tooltip" style="  font-size: 20px; cursor: pointer;"></i-->
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="table">
                                <tr id="" class="">
                                    <td>
                                      <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="intangible" name="check"> <span class="label-text">Patrimonio Intangible</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="g" data-placement="right" data-toggle="tooltip" class="fa fa-question-circle-o g"></i>
                                        </label>
                                      </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                      <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="natural" name="check"><span class="label-text">Patrimonio Natural</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="n" data-placement="right" class="fa fa-question-circle-o n"></i>
                                        </label>
                                      </div>  

                                    </td>
                                </tr>
                                <!--tr>
                                    <td>
                                      <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="tangible" name="check"> <span class="label-text">Patrimonio Tangible</span>
                                        </label>
                                      </div> 
                                    </td>
                                </tr-->
                                <tr>
                                    <td>
                                      <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="educar" name="check"> <span class="label-text">Educación</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="e" data-placement="right" class="fa fa-question-circle-o e"></i>
                                        </label>
                                      </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                      <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="artess" name="check"> <span class="label-text">Artes</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="a" data-placement="right" class="fa fa-question-circle-o a"></i>
                                        </label>
                                      </div> 
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" id="bd">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" id="dom"><span class="glyphicon glyphicon-th" style="color: #088D4A;">
                            </span><strong>Público objetivo de los eventos</strong></a>
                        </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="table">
                                <!--tr>
                                    <td>
                                      <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="object" name="check"> <span class="label-text">Público objetivo </span>
                                        </label>
                                      </div> 
                                    </td>
                                </tr-->
                                <tr>
                                  <td>
                                    <label>
                                      <input type="checkbox" id="infan" name="check"> <span class="label-text">Infantes</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="i" data-placement="right" class="fa fa-question-circle-o i"></i>
                                    </label>
                                  </td>
                                </tr>
                                 <tr>
                                  <td>
                                    <label>
                                      <input type="checkbox" id="jven" name="check"> <span class="label-text">Jóvenes</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="j" data-placement="right" class="fa fa-question-circle-o j"></i>
                                    </label>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <label>
                                      <input type="checkbox" id="mjer" name="check"> <span class="label-text">Mujeres</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="m" data-placement="right" class="fa fa-question-circle-o m"></i>
                                    </label>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <label>
                                      <input type="checkbox" id="may" name="check"> <span class="label-text">Adultos Mayores</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="am" data-placement="right" class="fa fa-question-circle-o am"></i>
                                    </label>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <label>
                                      <input type="checkbox" id="pgral" name="check"> <span class="label-text">Público en general</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="pg" data-placement="right" class="fa fa-question-circle-o pg"></i>
                                    </label>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <label>
                                      <input type="checkbox" id="potra" name="check"> <span class="label-text">Otra</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="o" data-placement="right" class="fa fa-question-circle-o o"></i>
                                    </label>
                                  </td>
                                </tr>
                              </table>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading" id="bd">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" id="dom"><span class="glyphicon glyphicon-th" style="color: #088D4A;">
                            </span><strong>Intención Principal del gestor</strong></a>
                        </h4>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="table">
                                <tr>
                                    <td>
                                      <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="difundir" name="check"> <span class="label-text">Difundir</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="d" data-placement="right" class="fa fa-question-circle-o d"></i>
                                        </label>
                                      </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                      <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="educacion" name="check"><span class="label-text">Educación</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="ed" data-placement="right" class="fa fa-question-circle-o ed"></i>
                                        </label>
                                      </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                      <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="expresion" name="check"> <span class="label-text">Expresion creativa</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="ex" data-placement="right" class="fa fa-question-circle-o ex"></i>
                                        </label>
                                      </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="linea" name="check"> <span class="label-text">Contenido en linea</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="cl" data-placement="right" class="fa fa-question-circle-o cl"></i>
                                        </label>
                                      </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                      <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="preservacion" name="check"> <span class="label-text">Preservacion</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="pr" data-placement="right" class="fa fa-question-circle-o pr"></i>
                                        </label>
                                      </div> 

                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                      <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="otro" name="check"> <span class="label-text">Otro</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="ot" data-placement="right" class="fa fa-question-circle-o ot"></i>
                                        </label>
                                      </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                 <div class="panel panel-default" >
                    <div class="panel-heading" id="bd">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse5" id="dom"><span class="glyphicon glyphicon-th" style="color: #088D4A;">
                            </span><strong>Modalidades de acceso y participación en la vida cultural</strong></a>
                        </h4>
                    </div>
                    <div id="collapse5" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="table">
                                <tr>
                                    <td>
                                      <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="taller" name="check"> <span class="label-text">Cursos y talleres</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="ct" data-placement="right" class="fa fa-question-circle-o ct"></i>
                                        </label>
                                      </div> 
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                      <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="festival" name="check"> <span class="label-text">Festivales</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="fes" data-placement="right" class="fa fa-question-circle-o  fes"></i>
                                        </label>
                                      </div> 
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                      <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="foro" name="check"> <span class="label-text">Foros</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="for" data-placement="right" class="fa fa-question-circle-o for"></i>
                                        </label>
                                      </div> 
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                      <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="prestacion" name="check"> <span class="label-text">Presentaciones</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="pre" data-placement="right" class="fa fa-question-circle-o pre"></i>
                                        </label>
                                      </div> 
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                      <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="artistica" name="check"> <span class="label-text">Producción artistica</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="prod" data-placement="right" class="fa fa-question-circle-o prod"></i>
                                        </label>
                                      </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                      <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="restauracion" name="check"> <span class="label-text">Restauración</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="rcion" data-placement="right" class="fa fa-question-circle-o rcion"></i>
                                        </label>
                                      </div>
                                    </td>
                                </tr>
                                <tr>
                                  <td>
                                      <div class="form-check">
                                        <label>
                                          <input type="checkbox" id="otrofest" name="check"> <span class="label-text">Otro</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i id="tro" data-placement="right" class="fa fa-question-circle-o tro"></i>
                                        </label>
                                      </div>
                                  </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>