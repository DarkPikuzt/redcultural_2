<?php 

	include ("conexion.php");
	/*Aqui empiezan las sentencias para los miembros de la Red cultural*/
	$sqlempre = "SELECT * FROM miembros where tipo = 'Emprendedor cultural'";
	$resultempre = mysqli_query($conectar,$sqlempre);
	while ($mosempre = mysqli_fetch_array($resultempre)) {
		$id = $mosempre['id'];
		$tipo = $mosempre['tipo'];
		$nombre = $mosempre['nombre'];
		$representante = $mosempre['representante'];
		$estado = $mosempre['estado'];
		$mun = $mosempre['municipio'];
		$loc = $mosempre['localidad'];
		$correo = $mosempre['correo'];
		$lat = $mosempre['latitud'];
		$long = $mosempre['longitud'];

		$emprend[] = array($id,
							   $tipo,
							   $nombre,
							   $representante,
							   $estado,
							   $mun,
							   $loc,
							   $correo,
							   $lat,
							   $long,		
							);
	}


	$slqcreador = "SELECT * FROM miembros where tipo = 'Creador'";
	$resultcreador = mysqli_query($conectar,$slqcreador);
	while ($moscreador = mysqli_fetch_array($resultcreador)) {
		$id = $moscreador['id'];
		$tipo = $moscreador['tipo'];
		$nombre = $moscreador['nombre'];
		$representante = $moscreador['representante'];
		$estado = $moscreador['estado'];
		$mun = $moscreador['municipio'];
		$loc = $moscreador['localidad'];
		$correo = $moscreador['correo'];
		$lat = $moscreador['latitud'];
		$long = $moscreador['longitud'];

		$cread[] = array($id,
						   $tipo,
						   $nombre,
						   $representante,
						   $estado,
						   $mun,
						   $loc,
						   $correo,
						   $lat,
						   $long,		
						);

	}

	$sqlgestor = "SELECT * FROM miembros WHERE tipo = 'Gestor'";
	$resultges = mysqli_query($conectar,$sqlgestor);
	while ($mosgestor = mysqli_fetch_array($resultges)) {
		$id = $mosgestor['id'];
		$tipo = $mosgestor['tipo'];
		$nombre = $mosgestor['nombre'];
		$representante = $mosgestor['representante'];
		$estado = $mosgestor['estado'];
		$mun = $mosgestor['municipio'];
		$loc = $mosgestor['localidad'];
		$correo = $mosgestor['correo'];
		$lat = $mosgestor['latitud'];
		$long = $mosgestor['longitud'];

		$gest[] = array($id,
						   $tipo,
						   $nombre,
						   $representante,
						   $estado,
						   $mun,
						   $loc,
						   $correo,
						   $lat,
						   $long,		
						);
	}

	$sqlpromo = "SELECT * from miembros WHERE  tipo = 'Promotor/animador cultural'";
	$resultp = mysqli_query($conectar,$sqlpromo);

	while ($resultpromo = mysqli_fetch_array($resultp)) {
		$id = $resultpromo['id'];
		$tipo = $resultpromo['tipo'];
		$nombre = $resultpromo['nombre'];
		$representante = $resultpromo['representante'];
		$estado = $resultpromo['estado'];
		$mun = $resultpromo['municipio'];
		$loc = $resultpromo['localidad'];
		$correo = $resultpromo['correo'];
		$lat = $resultpromo['latitud'];
		$long = $resultpromo['longitud'];

		$promo[] = array($id,
						   $tipo,
						   $nombre,
						   $representante,
						   $estado,
						   $mun,
						   $loc,
						   $correo,
						   $lat,
						   $long,		
						);

	}

	//echo json_encode($cread);
?>