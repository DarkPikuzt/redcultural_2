 <!-- Mensajes de las Hipotesis G Modal -->
	<div class="modal fade" id="miModal" role="dialog">
	    <div class="modal-dialog" id="tamano">
	    <!-- Modal content-->
	      <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title">Hipotesis G</h4>
	        </div>
	        <div class="modal-body">
	          <div class="col-lg-12  col-md-12  col-sm-12  col-xs-12">
  <div class="row">
  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
   <div class="card" id="panelInfo">
      <div class="card-header">
        <strong> Miembros de la red Recultivar México </strong>  <i class="fa fa-question-circle-o" data-toggle="tooltip" title="Tipo de gestión realizada por cada Gestor, de acuerdo con su propia autoadscripción (Reconocimiento que hace la población de pertenecer a una forma de hacer gestión, con base en sus concepciones)." id="tooltip" style="  font-size: 20px; cursor: pointer;"></i>
      </div>
      <div class="card-body" >
        <div id="chartContainer" style ="width : 100%; height: 100%"></div>
      </div>
   </div>
  </div>
<!-- Inicio Panel de poblacion objetivo -->
  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    <div class="card" id="panelInfo">
      <div class="card-header">
        <strong> Tipo de población atendida </strong> <i class="fa fa-question-circle-o" data-toggle="tooltip" title="Principales grupos de población atendida por cada gestor de la red." id="tooltip" style="  font-size: 20px; cursor: pointer;"></i>
      </div>
      <div class="card-body" id="pobObjetivo">
        <div id="chartContainer2" style ="width : 100%; height: 100%"></div>
      </div>
    </div>
  </div> 
          <!-- Fin Panel de poblacion objetivo -->
</div>
	        </div>
	        <div class="modal-footer">
	          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        </div>
	      </div>
	      
	    </div>
	 </div>