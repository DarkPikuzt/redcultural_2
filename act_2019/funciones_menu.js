//funciones para mostrar y ocultar los checkbox
$(document).ready(function(){
	 $("#evento").click(function(){
        $("#publico_objetivo").hide();
        $("#event").hide();
        $("#modalidades").hide();
        $("#dominio").show();
       return false;
     });

	 $("#object_event").click(function(){
	 	$("#dominio").hide();
	 	$("#event").hide();
	 	$("#modalidades").hide();
        $("#publico_objetivo").show();
        //alert("sdi");
       return false;
      });
	 $("#intencion").click(function(){
	 	$("#dominio").hide();
	 	$("#publico_objetivo").hide();
	 	$("#modalidades").hide();
	 	$("#event").show();
	 	return false;
	 });
	 $("#modalidad").click(function(){
	 	$("#dominio").hide();
	 	$("#publico_objetivo").hide();
	 	$("#event").hide();
	 	$("#modalidades").show();
	 	return false;
	 });


});
//aqui terminan las funciones para mostrar
//aqui empieza las funciones para mostrar las capas del dominio cultural del evento
function mostrar_y_ocultar_DCE(oms,markers){
	$("#intangible").on('change',function(){
            if ($(this).is(':checked') ) {
                /*Markers();
                showMarker();*/
                marker_int(oms,markers);
                //markerClusterer = new MarkerClusterer(map, clusterMarker, {imagePath: 'img/m', maxZoom: 14});
                markerClusterer = new MarkerClusterer(map, markers,
                {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m', maxZoom: 14});
                 var p =  $("#intangible").attr('id');
                console.log(p);
            }else{
                /*showMarker();*/
                Markers();
           
                markers = [];
                markerClusterer.clearMarkers();

            }
    });
    $("#natural").on( 'change', function() {
         if( $(this).is(':checked') ) {
          /*clearnat();
           shownat();*/
            marker_nat(oms,markers);
            markerClusterer = new MarkerClusterer(map, markers,
            {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m', maxZoom: 14});
         } else {
          shownat(); 
          clearnat();
          markers = [];
          markerClusterer.clearMarkers();       
         
         }
    });
    $("#educar").on('change', function(){
            if ($(this).is(':checked') ) {
              /*educaMarkers();
              showeduca();*/ 
               marker_edu(oms,markers);
               markerClusterer = new MarkerClusterer(map, markers,
               {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m', maxZoom: 14});
            }else{
              showeduca();
              educaMarkers();
              markers = [];
              markerClusterer.clearMarkers();  
            }
    });
    $("#artess").on('change', function(){
          if ($(this).is(':checked') ) {
                /*arteMarkers();
                showarte();*/
                 marker_artes(oms,markers);
                 markerClusterer = new MarkerClusterer(map, markers,
                 {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m', maxZoom: 14});

          }else{
               showarte();  
               arteMarkers();
               markers = [];
               markerClusterer.clearMarkers(); 
          }
    });


}

//aqui terminan las funciones para mostrar
//aqui empiezan las funciones para mostrar las capas de publico objetivo de los elementos. 
function mostrar_y_ocultar_POE(oms,markers){
	$("#infan").on('change', function(){
          if ( $(this).is(':checked')) {
              /*clearinfantes();
              showinfantes();*/ 
              mmarker_infantes(oms,markers);
              markerClusterer = new MarkerClusterer(map, markers,
              {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m', maxZoom: 14});
          }else{
              showinfantes();
              clearinfantes();
              markers = [];
              markerClusterer.clearMarkers();  
          }
    });
    $("#jven").on('change', function(){
          if ( $(this).is(':checked')) {
            /*clearjoven();
            showjoven();*/
            marker_jovenes(oms,markers);
            markerClusterer = new MarkerClusterer(map, markers,
            {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m', maxZoom: 14});
          }else{
            showjoven();
            clearjoven();
            markers = [];
            markerClusterer.clearMarkers();  
          }
    });
    $("#mjer").on('change', function(){
          if ( $(this).is(':checked')) {
            /*clearmujer();
            showmujer();*/
            marker_mujeres(oms,markers);
            markerClusterer = new MarkerClusterer(map, markers,
            {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m', maxZoom: 14});
          }else{
            /*howmujer();
            clearmujer();*/
            markers = [];
            markerClusterer.clearMarkers();    
          } 
    });
    $("#may").on('change', function(){
          if ( $(this).is(':checked')) {
              /*clearmayores();
              showmayores();*/
              marker_mayores(oms,markers);
              markerClusterer = new MarkerClusterer(map, markers,
              {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m', maxZoom: 14});
          }else{
              showmayores();
              clearmayores();
              markers = [];
              markerClusterer.clearMarkers();     
          } 
    });
    $("#pgral").on('change', function(){
          if ( $(this).is(':checked')) {
              /*cleargeneral();
              showgen();*/
               marker_general(oms,markers);
              markerClusterer = new MarkerClusterer(map, markers,
              {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m', maxZoom: 14});
          }else{
              showgen();
              cleargeneral();
              markers = [];
              markerClusterer.clearMarkers();   
          }  
    });
    $("#potra").on('change', function(){
          if ( $(this).is(':checked')) {
              /*clearobj();
              showobj();*/
              marker_event6(oms,markers);
              markerClusterer = new MarkerClusterer(map, markers,
              {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m', maxZoom: 14});
          }else{
              showobj();
              clearobj();
              markers = [];
              markerClusterer.clearMarkers();   

          }  
    });
}

//functiones para mostrar intencion pincipal del gestor
function mostrar_y_ocultar_IPG(oms,markers){
	$("#difundir").on('change', function(){
          if ( $(this).is(':checked')) {
            /*clearevent1();
            showevent1();*/
            marker_event1(oms,markers);
            markerClusterer = new MarkerClusterer(map, markers,
            {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m', maxZoom: 14});
          }else{
            showevent1();
            clearevent1();
            markers = [];
            markerClusterer.clearMarkers(); 
          }
    });
    $("#educacion").on('change', function(){
          if ( $(this).is(':checked')) {
            /*clearevent2();
            showevent2();*/
            marker_event2(oms,markers);
            markerClusterer = new MarkerClusterer(map, markers,
            {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m', maxZoom: 14});
          }else{
            showevent2();
            clearevent2();
            markers = [];
            markerClusterer.clearMarkers();  
          }
    });	
    $("#expresion").on('change', function(){
          if ( $(this).is(':checked')) {
            /*clearevent3();
            showevent3();*/
            marker_event3(oms,markers);
            markerClusterer = new MarkerClusterer(map, markers,
            {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m', maxZoom: 14}); 
          }else{
            showevent3();
            clearevent3();
            markers = [];
            markerClusterer.clearMarkers(); 
          }
    });
    $("#linea").on('change', function(){
          if ( $(this).is(':checked')) {
            /*clearevent4();
            showevent4();*/
            marker_event4(oms,markers);
            markerClusterer = new MarkerClusterer(map, markers,
            {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m', maxZoom: 14});  
          }else{
            showevent4();
            clearevent4();
            markers = [];
            markerClusterer.clearMarkers();  
          }
    });
    $("#preservacion").on('change', function(){
          if ( $(this).is(':checked')) {
            /*clearevent5();
            showevent5();*/
            marker_event5(oms,markers);
            markerClusterer = new MarkerClusterer(map, markers,
            {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m', maxZoom: 14}); 
          }else{
            showevent5();
            clearevent5();
            markers = [];
            markerClusterer.clearMarkers();   
          }
    });
    $("#otro").on('change', function(){
          if ( $(this).is(':checked')) {
            /*clearevent6();
            showevent6();*/
            marker_event6(oms,markers);
            markerClusterer = new MarkerClusterer(map, markers,
            {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m', maxZoom: 14});
          }else{
            showevent6();
            clearevent6();
            markers = [];
            markerClusterer.clearMarkers(); 
          }
    });
}

//aqui emiezan las funciones para mostrar modalidades de acceso y participación en la vida cultural.
function mostrar_y_ocultar_MAPVC(oms,markers){
	$("#taller").on('change', function(){
          if ( $(this).is(':checked')) {
            /*clearcurso();
            showcurso();*/
            marker_cursos(oms,markers);
            markerClusterer = new MarkerClusterer(map, markers,
            {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m', maxZoom: 14});
          }else{
            showcurso();
            clearcurso();
            markers = [];
            markerClusterer.clearMarkers(); 
          }
    });
    $("#festival").on('change', function(){
          if ( $(this).is(':checked')) {
            /*clearfesti();
            showfesti();*/
            marker_festival(oms,markers);
            markerClusterer = new MarkerClusterer(map, markers,
            {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m', maxZoom: 14});
          }else{
            showfesti();
            clearfesti();
            markers = [];
            markerClusterer.clearMarkers(); 
          }
    });
    $("#foro").on('change', function(){
          if ( $(this).is(':checked')) {
            /*clearforo();
            showforo();*/
            marker_foro(oms,markers);
            markerClusterer = new MarkerClusterer(map, markers,
            {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m', maxZoom: 14});
          }else{
            showforo();
            clearforo();
            markers = [];
            markerClusterer.clearMarkers(); 
          }
    });
    $("#prestacion").on('change', function(){
          if ( $(this).is(':checked')) {
            /*clearprest();
            showprest();*/
            marker_presentaciones(oms,markers);
            markerClusterer = new MarkerClusterer(map, markers,
            {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m', maxZoom: 14});
          }else{
            showprest();
            clearprest();
            markers = [];
            markerClusterer.clearMarkers(); 
          }
    });
    $("#artistica").on('change', function(){
          if ( $(this).is(':checked')) {
           /*clearartist();
            showartist();*/
            marker_artistica(oms,markers);
            markerClusterer = new MarkerClusterer(map, markers,
            {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m', maxZoom: 14});
          }else{
            showartist();
            clearartist();
            markers = [];
            markerClusterer.clearMarkers(); 
          }
    });
    $("#restauracion").on('change', function(){
          if ( $(this).is(':checked')) {
            /*clearestauracion();
            showrestauracion();*/
            marker_restauracion(oms,markers);
            markerClusterer = new MarkerClusterer(map, markers,
            {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m', maxZoom: 14});
          }else{
            showrestauracion();
            clearestauracion();
            markers = [];
            markerClusterer.clearMarkers(); 
          }
    });
    $("#otrofest").on('change', function(){
          if ( $(this).is(':checked')) {
            /*clearfestival();
            showfestival();*/
            marker_otrob(oms,markers);
            markerClusterer = new MarkerClusterer(map, markers,
            {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m', maxZoom: 14}); 
          }else{
            showfestival();
            clearfestival();
            markers = [];
            markerClusterer.clearMarkers(); 
          }
    });

}


//funciones para los municipios de cada estado
$(document).ready(function(){
	$("#ver").click(function(){
		var ve = 'Veracruz';
		//alert(ve);
		$.ajax({
			url: '../php/consultamunicipios.php',
			type: 'post',
			data: {ve:ve},
			success: function(datos){
				$('#municip').html(datos);
			}
		});

		 /*$("#veracruz_graficas").show();

	    $('html, body').animate({
	       scrollTop: $(document).height()
	    }, 'slow');*/
 
		
        return false;
	});

	$("#que").click(function(){
		var quer = 'Queretaro';
		
		$.ajax({
			url: '../php/queretaro.php',
			type: 'post',
			data: {quer:quer},
			success: function(datos){
				$('#municip').html(datos);
			}
		});
		/*$("#queretaro_graficas").show();
		$('html, body').animate({
	       scrollTop: $(document).height()
	    }, 'slow');*/
        return false;
	});

	$("#tlax").click(function(){
		var tla = 'Tlaxcala';
		
		$.ajax({
			url: '../php/tlaxcala.php',
			type: 'post',
			data: {tla:tla},
			success: function(datos){
				$('#municip').html(datos);
			}
		});
		
        return false;
	});


	$("#hidal").click(function(){
		var hid = 'Hidalgo';
		
		$.ajax({
			url: '../php/hidalgo.php',
			type: 'post',
			data: {hid:hid},
			success: function(datos){
				$('#municip').html(datos);
			}
		});
		
        return false;
	});

	$("#leon").click(function(){
		var leo = 'Nuevo Leon';
		
		$.ajax({
			url: '../php/leon.php',
			type: 'post',
			data: {leo:leo},
			success: function(datos){
				$('#municip').html(datos);
			}
		});
		
        return false;
	});

	$("#pueb").click(function(){
		var pu = 'puebla';
		
		$.ajax({
			url: '../php/puebla.php',
			type: 'post',
			data: {pu:pu},
			success: function(datos){
				$('#municip').html(datos);
			}
		});
		
        return false;
	});

});

//funciones para los mensajes de dialogo del menu
$(document).ready(function(){
	//$('#g').tooltip({title: "Todo aquel patrimonio que debe salvaguardarse y consiste en el reconocimiento de los usos, representaciones, expresiones, conocimientos y técnicas transmitidos de generación en generación y que infunden a las comunidades y a los grupos un sentimiento de identidad y continuidad, contribuyendo así a promover el respeto a la diversidad cultural y la creatividad humana.  UNESCO. Convención para la salvaguardia del patrimonio cultural inmaterial 2003.", container: ".g"});
	//$('#n').tooltip({title: "Acciones de las comunidades humanas para la salvaguarda, goce y divulgación de los servicios ecosistémicos.", container: ".n"});
	//$('#e').tooltip({title: "Acciones encaminadas a la transformación permanente de la conducta y actitudes humanas, mediante el conocimiento de diversas realidades (natural, social, cultural).", container: ".a"});
	//$('#a').tooltip({title: "Expresiones humanas de carácter estético.", container: ".a"});
	//$('#i').tooltip({title: "Personas de hasta 12 años.", container: ".i"});
	//$('#j').tooltip({title: "Personas a partir de 13 años.", container: ".j"});
	//$('#m').tooltip({title: "Género femenino.", container: ".m"});
	//$('#am').tooltip({title: "Personas a partir de 60 años", container: ".am"});
	//$('#pg').tooltip({title: "No se especifica un público y se asume que puede asistir cualquier persona interesada", container: ".pg"});
	//$('#o').tooltip({title: "Especializado como gestores, creadores, académicos, sólo adultos", container: ".o"});
	//$('#d').tooltip({title: "Difundir y Divulgar", container: ".d"});
	//$('#ed').tooltip({title: "Educación y/o formación de públicos", container: ".ed"});
	//$('#ex').tooltip({title: "Disfrute y expresión recreativa ", container: ".ex"});
	//$('#cl').tooltip({title: "Producción de contenidos en línea", container: ".cl"});
	//$('#pr').tooltip({title: "Preservación yRestauración", container: ".pr"});
	//$('#ot').tooltip({title: "Entrega de premio nacional de librería, grabación de disco, taller de sonido y grabación, multiplataforma de fondeo cultural", container: ".ot"});
	//$('#ct').tooltip({title: "Cursos y talleres", container: ".ct"});
	//$('#fes').tooltip({title: "Festivales", container: ".fes"});
	//$('#for').tooltip({title: "Foros, seminarios, coloquios y conferencias", container: ".for"});
	//$('#pre').tooltip({title: "Presentaciones, exposiciones, conciertos y proyecciones de audiovisuales", container: ".pre"});
	///$('#prod').tooltip({title: "Logística de producción artística y Creación de servicios culturales", container: ".prod"});
	//$('#rcion').tooltip({title: "Curaduría y restauración", container: ".rcion"});
	//$('#tro').tooltip({title: "cinedebate, jugar, bailar, elaborar algún altares, caminatas y recorridos, improvisación artística o participación activa de los participantes y audiencia que pueden combinar dos modalidades de las anteriores.", container: ".tro"});

});


$(document).ready(function(){
 /* $("select[name=cambioanio]").change(function(){
  	var anio = $('select[name=cambioanio]').val();
  	if ( anio == '2019') {
  	 	console.log(anio);
  	 	/*$("#intangible").removeAttr("id");
  	 	$("#intangible").attr("id","nuevo2018");*/
  	 	 /*var titulo = $("#intangible").attr('id');
          alert(titulo);
          $("#intangible").removeAttr("id");
          $("#intangible").attr("id","soy");
          tit = $("#soy").attr('id');
          alert(tit);*/
          //$("#intangible").removeAttr("#intangible");
          //document.getElementById("intangible").removeAttribute("id");
          //$("#intangible").removeAttr("id");
          //$("#intangible").attr("id","soy");
		 /* $('#intangible').removeAttr('id');

  	}
  	return false;	
  });*/
  //$("#comunicacion").load('php/menu2019.php');
  
    //$('[data-toggle="popover"]').popover();   

});
  
/*$(document).ready(function(){
  $("select[name=cambioanio]").change(function(){
  	var anio = $('select[name=cambioanio]').val();
  	if ( anio == '2019') {
  		//$("#map").removeAttr("id");
  		//$("#marc").removeAttr("src");
  	 	//console.log(anio);
  		 //$("#menu2018").hide();
  		 //$("#cargar_2019").load('act_2019/mapa2019.php');	
  		 //alert("No se cuenta por el momento con la información.");
  		 //$("#menu2018").show();
  		 $.ajax({
  		 	url: '../act_2019/obtener.php',
  		 	type: 'POST',
  		 	//dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
  		 	data: {anio: anio},
  		 })
  		 .done(function(datos) {
  		 	console.log("success");
  		 	$("#m_2018").html(datos);
  		 })
  		 .fail(function() {
  		 	console.log("error");
  		 })
  		 .always(function() {
  		 	console.log("complete");
  		 });
  		 
  		
  	}else{
  		//location.reload();
  	}
  	return false;	
  });
  

});*/

